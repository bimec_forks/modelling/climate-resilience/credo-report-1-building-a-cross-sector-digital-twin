pip install pre-commit

for HOOK in pre-commit pre-merge-commit pre-push post-commit post-checkout post-merge commit-msg
do
    pre-commit install --hook-type $HOOK > /dev/null
done
pre-commit autoupdate
