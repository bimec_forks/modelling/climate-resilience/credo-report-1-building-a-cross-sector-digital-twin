# CReDo: Building a cross-sector digital twin


This repository contains code written to create the Climate Resilience Demonstrator, and see [here](https://digitaltwinhub.co.uk/credo/) for more information about the project. 
For more details on the work contained in this repository, see 
[CReDo Technical Report 1: Building a cross-sector digital twin](https://doi.org/10.17863/CAM.81779).
  
This repository contains all of the components needed for building the information cascade model (named "CReDo workflow (information cascade model only)" on [DAFNI](https://dafni.ac.uk/)), and the majority of those needed to run the modular workflow (named "CReDo workflow (individual asset failure and system-wide impact models)" on DAFNI). For more information on the additional components for the latter workflow, see the [asset failure model](https://gitlab.developers.cam.ac.uk/cdbb/credo-group/credo-report-3-assessing-asset-failure) and the [systems impact model](https://gitlab.developers.cam.ac.uk/cdbb/credo-group/credo-report-4-modelling-system-impact), although use of these is optional. 


