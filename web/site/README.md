# Internal Website

This folder contains the source and Docker configuration files used to build the internal CReDo website. This website will hold information such as status updates, reports, and documentation; hosting it within a Docker container will allow project managers to view the content without having to checkout the GitLab repository.

## ???

The website has been created using [Grav](https://getgrav.org/), as website framework that uses a flat file structure, allowing the website to be stored and built from the GitLab repository.

## Updating content 

TODO 

## How to build

TODO 