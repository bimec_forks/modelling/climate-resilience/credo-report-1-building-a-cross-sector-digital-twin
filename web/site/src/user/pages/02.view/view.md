---
title: View File
visible: false
body_classes: 'title-center title-h1h2'
---

<div id="inner-body" class="three-quarter-width">
	<div id="name-container">Loading file, please wait...</div>
	<br/>
	<hr/>
	<div id="file-container"></div>
	<hr/>
</div>