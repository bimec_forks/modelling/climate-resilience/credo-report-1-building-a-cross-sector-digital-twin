---
title: Home
visible: false
body_classes: 'title-center title-h1h2'
---

<div id="inner-body">
	<p>This website has been produced by CMCL as part of the CReDo project and is intended to provide a single place to view internal documents, status reports, and READMEs for the ongoing technical work. It is envisioned that presenting this content via an internal website will allow non-technical users/client easier access to up-to-date information, rather than having to clone and regularly pull the GitLab repository. Note that at the time of writing, only TXT and MD files are viewable online (other file types are downloadable though).</p>
	
	<p>To add new files to this site, please contact the developer that has started the Docker container. They will need to pull the relevant branch of the GitLab repository. As the <code>/docs</code> directory of the repository is mounted as a volume, and files are discovered recursively via PHP, the webpage will automatically discover new files once the pull is made and the page is refreshed.</p>
	
	<br/>
	<hr/>	
	<h3>Available Files:</h3>
cadphp:p1:list-docs
	<br/>
	<hr/>
</div>


<script>
	function openFile(source) {
		let winURL = window.location;
        let baseURL = winURL.protocol + "//" + winURL.host + winURL.pathname;
		let fileName = source.dataset.file;
        
		if(fileName.endsWith(".txt") || fileName.endsWith(".md")) {
			// Open on view page
			var url = baseURL + "view?file=" + encodeURIComponent(fileName);
			window.open(url).focus();
			
		} else {
			// Direct download
			let fullfileURL = winURL.protocol + "//" + winURL.host + winURL.pathname;
			fullfileURL += "docs/" + fileName;
			window.open(fullfileURL);
		}
	}
</script>