The two containers required for the visulisation website are:
credo-vis:
	This container serves the website and most of the data.
geoserver:
	This container serves the geotiffs containing the flood data, the 'credo-vis' container acts as a proxy for this container so they can be accessed through the same http endpoint.

Before running anything you should modify the following variables in the .env file:
GEOSERVER_PORT:
	This port can have any value and only needs to be accessible from the 'credo-vis' container.
VIS_PORT:
	This port can have any value and needs to be accessible to be viewable through a browser.
	
GEOSERVER_HOST:
	The name of the host running the 'geoserver' container as seen from the 'credo-vis' container.
	The default value works with Docker, with Podman the name of the host running Podman seems to work.

The 'load_and_run.sh' script loads in the podman/docker images from the two .tar files and then starts the containers using 'podman-compose'.

Due to a quirk in its entrypoint script the 'geoserver' container might need to be restarted for it to correctly load in the flood data that it servers.
You can check whether this is required by going to the visualisation webpage and selecting timestep 43200 on the left, if you can see faint blue appear behind the assets then it is working.