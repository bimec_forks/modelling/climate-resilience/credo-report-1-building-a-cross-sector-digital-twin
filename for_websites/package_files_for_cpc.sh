#!/bin/sh

# Read in the "GEOSERVER_VERSION"
. ./.env

# Get the vis data out of the "docker_stacks_vis_data" volume.
# Only use if the volume contains all of the required data.
# Otherwise just copy the data into a directory called "data" manually.
#mkdir data/
#podman volume export docker_stacks_vis_data | tar -xC data/

# Remove previous copies and the export the container images
rm credo-vis.tar geoserver.tar
podman save docker.cmclinnovations.com/credo-vis:1.0.0-SNAPSHOT -o credo-vis.tar
podman save docker.cmclinnovations.com/geoserver:${GEOSERVER_VERSION} -o geoserver.tar

# Package files, excluding the "other building" data
tar -czvf credo_vis_for_cpc_website.tar.gz --exclude='other_buildings*json' ../docs data credo-vis.tar geoserver.tar docker-compose.yml .env load_and_run.sh