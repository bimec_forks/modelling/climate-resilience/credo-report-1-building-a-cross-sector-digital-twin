#!/bin/sh

# Wait for Blazegraph to come up; only needed when running with docker-compose/podman-compose
#sleep 10

dafni_outputs_dir="/data/outputs"

curl -X POST \
          $KG_ENDPOINT --data-urlencode \
          'query=PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX cmcl: <https://kg.cmclinnovations.com#> SELECT ?c1 ?c2 ?c1l ?c2l WHERE {{ ?c1 cmcl:is_related_to ?c2 . ?c1 cmcl:has_label ?c1l . ?c2 cmcl:has_label ?c2l }}' \
           --data-urlencode 'format=sparql-results+json' > $dafni_outputs_dir/results.json

cat $dafni_outputs_dir/results.json |jq -r '["Entity","Associated with"], ["---", "---"], (.results.bindings|.[] | [.c1l.value, .c2l.value]) | @csv' > $dafni_outputs_dir/results.csv