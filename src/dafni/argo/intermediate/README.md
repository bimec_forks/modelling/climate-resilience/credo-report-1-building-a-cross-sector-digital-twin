An intermediate version of the CReDO workflow that includes the following containers:
- The knowledge graph
  - blazegraph
  - ontop
  - postgis
- Data upload
  - kg-initialiser
  - pgsql-uploader
- Data output
  - kg-data-extractor