from distutils.command.upload import upload
from dafni_cli.duplicate_workflow import duplicate_workflow
from dafni_cli.login import login
from dafni_cli.upload_parameter_sets import upload_parameter_sets
from dafni_cli.clear_parameter_sets import clear_parameter_sets
from docopt import docopt, DocoptExit
import json
import os.path as path

doc = """dafni_workflow_interactor.

Usage:
    dafni_workflow_interactor.py upload_parameter_sets [--user USER --password PASSWORD --workflowID ID]
    dafni_workflow_interactor.py clear_parameter_sets [--user USER --password PASSWORD --workflowID ID]
    dafni_workflow_interactor.py login [--user USER --password PASSWORD]

    Options:
    --user           DAFNI username
    --password       DAFNI password
    --workflowID     DAFNI workflow ID
"""

def start():
    try:
        args = docopt(doc)
        jwt = login(username=args['USER'],password=args['PASSWORD'])
    except DocoptExit:
        raise DocoptExit('Error: dafni_workflow_interactor called with wrong arguments.')
    if (args['login']):
        print(jwt)
    elif (args['upload_parameter_sets']):
        # parameter_file = open(path.join("input","parameter_set_template.json"), 'r')
        hipims_file = open(path.join("input","hipims_runs.csv"), 'r')
        upload_parameter_sets(jwt, hipims_file, args['ID'])
        hipims_file.close()
    elif (args['clear_parameter_sets']):
        clear_parameter_sets(jwt, args['ID'])
    elif (args['duplicate_workflow']):
        duplicate_workflow(workflowID=args['ID'], jwt=jwt)

if __name__ == '__main__':
    start()