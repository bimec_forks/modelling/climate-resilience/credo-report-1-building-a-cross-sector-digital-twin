WORKFLOWS_URL = "https://dafni-nims-api.secure.dafni.rl.ac.uk/workflows/"
LOGIN_API_URL = "https://keycloak.secure.dafni.rl.ac.uk/auth/realms/Production/protocol/openid-connect/token"
PARAMETER_UPLOAD_URL = "https://dafni-nims-api.secure.dafni.rl.ac.uk/workflows/parameter-set/upload/"
PARAMETER_DELETE_URL = "https://dafni-nims-api.secure.dafni.rl.ac.uk/workflows/parameter-set/"