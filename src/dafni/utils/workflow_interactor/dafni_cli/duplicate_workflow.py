from dafni_cli.urls import WORKFLOWS_URL
import requests
import json

# not complete

# workflow_to_duplicate: DAFNI ID of the workflow to duplicate
# jwt: JSON web token from login
def duplicate_workflow(workflowID:str, jwt:str): 
    auth_header = {"Authorization": jwt}

    # get workflow
    workflowGet = requests.get(
        WORKFLOWS_URL + 'instances',
        headers=auth_header
    )

    print(workflowGet)
    workflow = json.loads(workflowGet.text)

    #change name
    workflow['metadata']['display_name'] = 'DUPLICATE ' + workflow['metadata']['display_name'] 
    workflow['metadata']['name'] = 'duplicate-' + workflow['metadata']['name']
    workflow['version_message'] = 'Initial workflow version'

