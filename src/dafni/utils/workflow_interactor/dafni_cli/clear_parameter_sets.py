from dafni_cli.urls import WORKFLOWS_URL,PARAMETER_DELETE_URL
import requests
import json

def clear_parameter_sets(jwt:str, workflow_ID):
    auth_header = {"Authorization": jwt}

    # get workflow
    workflowGet = requests.get(
        WORKFLOWS_URL + workflow_ID,
        headers=auth_header
    )

    workflow = json.loads(workflowGet.text)
    parameters_to_delete = []
    for parameterSet in workflow['parameter_sets']:
        if (parameterSet['metadata']['name'] != 'testing' and parameterSet['metadata']['name'] != 'testing-ben'):
            parameters_to_delete.append(parameterSet['id'])
    
    for parameter_to_delete in parameters_to_delete:
        del_response = requests.delete( 
            PARAMETER_DELETE_URL + parameter_to_delete,
            headers=auth_header
        )
        print(del_response)
