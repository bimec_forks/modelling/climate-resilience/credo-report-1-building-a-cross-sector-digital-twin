#! /bin/sh

# Enable job control
set -m

# Generate the setup.cli file
SETTINGS_DIR="${SETTINGS_DIR:-/opt/jboss/data-federation}"
SETTINGS_FILE="${SETTINGS_DIR}/setup.cli"

echo connect > "${SETTINGS_FILE}"

for file in "${SETTINGS_DIR}/pgsql/"*-vdb.xml; do

    echo "deploy ${file}" >> "${SETTINGS_FILE}"
done

"${JBOSS_HOME}/bin/standalone.sh" -c "${TEIID_CONFING_FILE}" -b 0.0.0.0 -bmanagement 0.0.0.0 &
sleep 20 && \
"$JBOSS_HOME/bin/jboss-cli.sh" --file="${SETTINGS_FILE}" && \
fg %1