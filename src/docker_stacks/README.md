# Run the workflow

1.  Go to the correct directory  
    `cd src/docker_stacks/`
2.  Store credentials for GitHub
    1.  Create and store username in `state-updater/docker/credentials/repo_username.txt`
    2.  Create and store password in `state-updater/docker/credentials/repo_password.txt`
    3.  Copy both files into `kg_data_extractor/docker/credentials/`, `kg-initialiser/docker/credentials/`
3. Prepare test data, 
    - By default, the synthetic data at `./test_data/synthetic` will be used, to use a different dataset, change the value of `SHARED_DATA_DIR_HOST` in `.env_template`.
    - Copy flood data into a directory named `flood_data` into the directory specified by `SHARED_DATA_DIR_HOST`, to copy an example dataset in the shared directory  
        `mkdir ./test_data/synthetic/flood_data`  
        `cp -r /mnt/credo/shared/hipims_case_100RP8h45u/* ./test_data/synthetic/flood_data`
	- If you have different flood scenarios in different folders, edit the value of DATASET_DIR in the `flood_data.conf` file within `./test_data/synthetic/config_files`
3. Initiate the environment variables  
    `./scripts/generate_env_file.sh`
4. Start the Blazegraph, PostgreSQL and Ontop servers  
    `./restart.sh servers`
5. Upload the t-boxes to the triplestore and the data to the RDB
    1.  `./restart.sh data_upload`
    2.  Wait for the psgsql-uploader container to finish running the upload_to_pgsql.sh script
6. Initialise the update agent
    1.  `./restart.sh update_agent`
    2.  `podman exec -it update-agent trigger_add_derivations`
    3.  Wait for the trigger_add_derivations executable to finish
7. Evaluate the timesteps, either
    - Run all of the timesteps in one go just using the flood data and the logic stored in the KG
      1.  Set the values of the simulation times  
        Change the value of the `SIMULATION_TIMES` environment variable in the `docker-compose.run_timesteps_internal_only.yml` file
      2.  Run the container  
        `./restart.sh run_timesteps_internal_only`
    - Run one or more external model at each timestep
      1.  Set the values of the simulation times  
        Change the value of the `SIMULATION_TIMES` environment variable in the `docker-compose.run_timesteps_internal_only.yml` file
      2.  If desired, disable the knowlagegraph's internal state propagation by setting the `UPDATE_DERIVATIONS` environment variable to `FALSE` in the following files
          - `docker-compose.init_timestep.yml`
          - `docker-compose.process_model_outputs.yml`
      3.  For each timestep, waiting for each step to finish before starting the next one, do
          1.  Initialise the timestep  
              `./restart.sh init_timestep`
          2.  For each external model do
              1.  `./restart.sh gen_model_inputs`
              2.  Run the external model
              3.  `./restart.sh process_model_outputs`
8. Generate outputs for visualization
    - Optional steps to generate river and builing data, this is independent of the servers, so it can be executed right at the beginning, the building query may take a while depending on the value of MAX_NUM_BUILDINGS. To query the entire building data set, set `MAX_NUM_BUILDINGS="none"` in docker-compose.river_and_building.yml:
        1. `./restart.sh river_and_building`, this will run both the river and building data getters
            - To selectively run the river-data-getter or building-data-getter:
                - `./restart.sh river_and_building building-data-getter`
                - `./restart.sh river_and_building river-data-getter`
		2. Make sure the process has completed before moving on to the next step, you can do this by running `podman logs river-data-getter` and `podman logs building-data-getter`.
    - Mandatory step, need to be executed after the river/building data getters if you wish to include them
        1.  `./restart.sh data_extractor`
		2. Wait for the process to complete before starting the visualisation, check with `podman logs kg-data-extractor`
9. Start visulisation  
    `./restart.sh vis`

# Commands to reset podman

1.  Stop all containers  
    `podman stop -a`
2.  Remove all containers  
    `podman rm -a`
3.  Remove all attached volume which used to store the data  
    `podman volume rm -a`
