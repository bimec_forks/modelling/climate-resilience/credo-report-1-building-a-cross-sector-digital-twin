import pyder_agent.power.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.flaskapp.update_routes.update_power_states import UPDATE_POWER_STATES_ROUTE
from pyder_agent.derivations.derivation_client import get_derivation_client
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)

def add_power_state_derivations()->None:

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    logger.info("---Adding power states derivations---")

    logger.info("Querying for assets power states dependencies")
    response = qtempl.get_power_state_dependecies()
    logger.info(f"Found {len(response)} entries to be processed")
    # dictionary that would hold the following info, e.g.:
    #   (belongsTo)
    #   child_power_state_iri =  [parent_power_state_iri1,     # isDerivedFrom_1
    #                             parent_power_state_iri2,     # isDerivedFrom_2
    #                             ..,                          # ...
    #                             child_flood_state_iri]       # isDerivedFrom_N
    power_state_dep = {}
    if response:
        logger.info(f"Processing found dependency entries")
        for response_dict in tqdm(response):
            child_power_state = response_dict['child_power_state']
            child_flood_state = response_dict.pop('child_flood_state',None)
            parent_power_state = response_dict.pop('parent_power_state',None)

            if child_flood_state is None and parent_power_state is None:
                continue

            if child_power_state not in power_state_dep:
                power_state_dep[child_power_state] = []

            temp_list = power_state_dep[child_power_state]

            if parent_power_state is not None:
                if parent_power_state not in temp_list:
                    temp_list.append(parent_power_state)

            if child_flood_state is not None:
                if child_flood_state not in temp_list:
                    temp_list.append(child_flood_state)

            power_state_dep[child_power_state] = temp_list

    logger.info(f"Found {len(power_state_dep)} assets power states dependent on other states")
    if power_state_dep:
        logger.info(f"Adding derivations to these assets")
        # init outer lists for bulk creation of derivations
        power_state_iris = []       # child power state iri list of lists
        agent_iris = []             # agent iris list
        power_state_dep_iris = []   # child power state dependency list (isDerivedFrom)
        agent_urls = []             # agent urls list
        # construct the outer lists
        for child_power_state, child_power_state_dep in power_state_dep.items():
            power_state_iris.append([child_power_state])
            agent_iris.append(child_power_state+'/agent')
            power_state_dep_iris.append(child_power_state_dep)
            agent_urls.append(endpoints.UPDATE_AGENT_HTTP+UPDATE_POWER_STATES_ROUTE)

        # bulk create power state derivations
        _ = derivation_client.bulkCreateDerivationsWithTimeSeries(
            belongs_to_iris=power_state_iris,
            agent_iris=agent_iris,
            derived_from_iris=power_state_dep_iris,
            update_agent_urls=agent_urls
    )