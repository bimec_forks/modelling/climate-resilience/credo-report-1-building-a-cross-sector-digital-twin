class StoreClientUpdateEndpointNotSetError(Exception):
    pass

class DerivationSetupError(Exception):
    pass

class TriggerOverrideInputsError(Exception):
    pass

class EmptyKGResponse(Exception):
    pass

class EmptyDerivedFromIris(Exception):
    pass

class EmptyBelongsToIris(Exception):
    pass

class MissingRequiredDerivedFromStates(Exception):
    pass

class NoActiveBelongsToStateDependencies(Exception):
    pass