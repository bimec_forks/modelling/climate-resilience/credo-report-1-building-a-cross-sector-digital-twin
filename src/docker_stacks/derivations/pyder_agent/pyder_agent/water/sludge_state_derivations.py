import pyder_agent.water.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.flaskapp.update_routes.update_sludge_states import UPDATE_SLUDGE_STATES_ROUTE
from pyder_agent.derivations.derivation_client import get_derivation_client
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)

def add_sludge_state_derivations()->None:

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    logger.info("Querying for assets sludge states dependencies")
    response = qtempl.get_sludge_state_dependecies()
    logger.info(f"Found {len(response)} entries to be processed")
    # dictionary that would hold the following info, e.g.:
    #   (belongsTo)
    #   child_sludge_state_iri =  [parent_sludge_state_iri1,     # isDerivedFrom_1
    #                             parent_sludge_state_iri2,      # isDerivedFrom_2
    #                             ..,                            # ...
    #                             child_power_state_iri]         # isDerivedFrom_N-1
    #                             child_flood_state_iri]         # isDerivedFrom_N
    sludge_state_dep = {}
    if response:
        logger.info(f"Processing found dependency entries")
        for response_dict in tqdm(response):
            child_sludge_state = response_dict['child_sludge_state']
            # child power and flood states dependency might be optional
            child_power_state = response_dict.pop('child_power_state', None)
            child_flood_state = response_dict.pop('child_flood_state',None)
            parent_sludge_state = response_dict.pop('parent_sludge_state', None)

            if child_power_state is None and child_flood_state is None and \
               parent_sludge_state is None:
                continue

            if child_sludge_state not in sludge_state_dep:
                sludge_state_dep[child_sludge_state] = []

            temp_list = sludge_state_dep[child_sludge_state]

            if parent_sludge_state is not None:
                if parent_sludge_state not in temp_list:
                    temp_list.append(parent_sludge_state)

            if child_power_state is not None:
                if child_power_state not in temp_list:
                    temp_list.append(child_power_state)

            if child_flood_state is not None:
                if child_flood_state not in temp_list:
                    temp_list.append(child_flood_state)
            sludge_state_dep[child_sludge_state] = temp_list

    logger.info(f"Found {len(sludge_state_dep)} assets sludge states dependent on other states")
    if sludge_state_dep:
        logger.info(f"Adding derivations to these assets")
        # init outer lists for bulk creation of derivations
        sludge_state_iris = []     # child sludge state iri list of lists
        agent_iris = []            # agent iris list
        sludge_state_dep_iris = [] # child sludge state dependency list (isDerivedFrom)
        agent_urls = []            # agent urls list
        for child_sludge_state, child_sludge_state_dep in sludge_state_dep.items():
            sludge_state_iris.append([child_sludge_state])
            agent_iris.append(child_sludge_state+'/agent')
            sludge_state_dep_iris.append(child_sludge_state_dep)
            agent_urls.append(endpoints.UPDATE_AGENT_HTTP+UPDATE_SLUDGE_STATES_ROUTE)

        # bulk create sludge state derivations
        _ = derivation_client.bulkCreateDerivationsWithTimeSeries(
            belongs_to_iris=sludge_state_iris,
            agent_iris=agent_iris,
            derived_from_iris=sludge_state_dep_iris,
            update_agent_urls=agent_urls
        )