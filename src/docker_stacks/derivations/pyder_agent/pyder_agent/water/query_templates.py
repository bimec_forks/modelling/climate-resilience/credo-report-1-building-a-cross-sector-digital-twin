from pyder_agent.kgoperations.store_client import get_store_client
import pyder_agent.common.endpoints as endpoints

def get_water_state_dependecies():
    query = """
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX aw: <http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#>
    PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
    SELECT ?child ?child_water_state ?child_water_type ?child_power_state ?child_flood_state ?parent ?parent_water_state
    WHERE {
        ?child aw:hasWaterState ?child_water_state .
        OPTIONAL {?parent aw:suppliesWaterTo ?child ;
                aw:hasWaterState ?parent_water_state . }
        OPTIONAL {?child ukpn:hasPowerState ?child_power_state . }
        OPTIONAL {?child credo:hasFloodState ?child_flood_state . }
    }
    """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_sewage_state_dependecies():
    query = """
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX aw: <http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#>
    PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
    SELECT ?child ?child_sewage_state ?child_sewage_type ?child_power_state ?child_flood_state ?parent ?parent_sewage_state
    WHERE {
        ?child aw:hasSewageState ?child_sewage_state .
        OPTIONAL {?child ukpn:hasPowerState ?child_power_state . }
        OPTIONAL {?parent aw:suppliesSewageTo ?child ;
                aw:hasSewageState ?parent_sewage_state . }
        OPTIONAL {?child credo:hasFloodState ?child_flood_state . }
    }
    """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_sludge_state_dependecies():
    query = """
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX aw: <http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#>
    PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
    SELECT ?child ?child_sludge_state ?child_sludge_type ?child_power_state ?child_flood_state ?parent ?parent_sludge_state
    WHERE {
        ?child aw:hasSludgeState ?child_sludge_state .
        OPTIONAL {?child ukpn:hasPowerState ?child_power_state . }
        OPTIONAL {?parent aw:suppliesSludgeTo ?child ;
                aw:hasSludgeState ?parent_sludge_state . }
        OPTIONAL {?child credo:hasFloodState ?child_flood_state . }
    }
    """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response