import pyder_agent.common.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.time_series.time_series_client import get_default_time_series_client
from pyder_agent.common.java_utils import JAVA_INTEGER_TYPE, JAVA_BOOLEAN_TYPE, JAVA_DOUBLE_TYPE
import pyder_agent.common.tbox_concepts as tbox
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)


def add_time_series_to_all_states()->None:
    time_series_client = get_default_time_series_client()

    logger.info("Querying for assets's states and has states relations")
    response = qtempl.get_assets_states()
    # this returns the following variables:
    # ?asset ?asset_state ?has_state_property
    logger.info(f"Found {len(response)} entries to be processed")

    # initialise helper dicts, and lists, and init values for all states
    # asset_states_dicts will hold all hasState subproperties and
    # iris they are pointing at attached to a given asset, e.g:
    # e.g. bt_site_iri1: {
    #          'hasFloodState':        ['flood_state_iri'],
    #          'hasFloodDepth':        ['flood_state_depth_iri'],
    #          'hasMaximumFloodDepth': ['flood_state_max_depth_iri'],
    #          'hasTelephoneState':    ['telephone_state_iri'],
    #          'hasPowerState':        ['power_state_iri']}
    asset_states_dicts = {}
    default_state_value = True        # default value for the power and phone states
    flood_state_depth_value = 0.0     # default flood depth state value [m]
    flood_state_max_depth_value = 0.6 # default flood max depth state value [m]
    # default value for the flood state (False mean not flooded)
    flood_state_value = flood_state_depth_value > flood_state_max_depth_value
    state_iris_outer= []              # state iris list of lists to be passed to the bulkInitTimeSeries
    data_iri_classes_outer = []       # data iris list of lists to be passed to the bulkInitTimeSeries
    init_time_stamps = [0]            # initial time stamp to be used in all time series tables
    iri_values_outer = []             # list of lists values associated with each data iri, used
                                      # to populate time series tables at init time stamp
    if response:
        logger.info(f"Processing found entries")
        for response_dict in tqdm(response):
            asset_iri = response_dict['asset']
            asset_state = response_dict['asset_state']
            asset_states_relation = response_dict['has_state_property']

            # start constructing the asset_states_dicts, if asset_iri not on
            # the dict already, add it
            if asset_iri not in asset_states_dicts:
                # this will e.g. produce: 'bt_site_iri1': {}
                asset_states_dicts[asset_iri] = {}

            # if a given hasState relation is not yet associated with a given
            # asset iri, add it and point to an empty list
            if asset_states_relation not in asset_states_dicts[asset_iri]:
                # this will e.g. produce: 'bt_site_iri1': {'hasTelephoneState:[]'}
                asset_states_dicts[asset_iri][asset_states_relation] = []

            # this extends a given assets relation list, adding new iris to it
            # e.g. 'bt_site_iri1': {'hasTelephoneState:[]'}, where the temp_list is []
            # will be appended to give:
            # e.g. 'bt_site_iri1': {'hasTelephoneState:['telephoneState_iri_1']'}
            # the temp_list is now ['telephoneState_iri_1']
            temp_list = asset_states_dicts[asset_iri][asset_states_relation]
            temp_list.append(asset_state)

            # finally write back the temp list to the dictionary
            asset_states_dicts[asset_iri][asset_states_relation] = temp_list

    # having found all assets hasState sub properties and associated states iris
    # process it to arrive at the final lists for the bulkInitTimeSeries method
    # and for creating the first value rows in all tables
    for _, state_dict in asset_states_dicts.items():
        state_iris_inner= []         # inner loop storing all state iris for a given asset
        data_iri_classes_inner = []  # inner loop storing all data iri classes for a given asset
        iri_values_inner = []        # inner loop storing all data iri values a given asset

        # this loop checks what is the type of state_iri by looking at its
        # hasState sub-property. This is needed to correctly distinguish
        # between FloodState and e.g. FloodDepthState as one is Boolean and the
        # other is Double. They will also hold different values.
        for state_relation, state_iri in state_dict.items():
            state_iris_inner.extend(state_iri)
            if state_relation == tbox.HAS_FLOOD_DEPTH_STATE:
                iri_values_inner.append([flood_state_depth_value])
                data_iri_classes_inner.append(JAVA_DOUBLE_TYPE)
            elif state_relation == tbox.HAS_FLOOD_MAX_DEPTH_STATE:
                iri_values_inner.append([flood_state_max_depth_value])
                data_iri_classes_inner.append(JAVA_DOUBLE_TYPE)
            elif state_relation == tbox.HAS_FLOOD_STATE:
                iri_values_inner.append([flood_state_value])
                data_iri_classes_inner.append(JAVA_BOOLEAN_TYPE)
            else:
                # power, phone states, water, sludge and sewage states
                iri_values_inner.append([default_state_value])
                data_iri_classes_inner.append(JAVA_BOOLEAN_TYPE)

        # assemble the final outer lists
        state_iris_outer.append(state_iris_inner)
        data_iri_classes_outer.append(data_iri_classes_inner)
        iri_values_outer.append(iri_values_inner)

    logger.info(f"Initializing {len(state_iris_outer)} time series tables")
    time_series_client.bulkInitTimeSeries(
        data_iris=state_iris_outer,
        data_iri_classes=data_iri_classes_outer
    )

    logger.info(f"Adding first entries to {len(state_iris_outer)} time series tables")
    with tqdm(total=len(state_iris_outer)) as pbar:
        for data_iri, iri_value in zip(state_iris_outer,iri_values_outer):

            timeseries_data = time_series_client.createTimeSeriesData(
                time_stamps= init_time_stamps,
                data_iris= data_iri,
                iri_values=iri_value)
            time_series_client.addTimeSeriesData(timeseries_data)
            pbar.update(1)