import pyder_agent.common.endpoints as endpoints
from pyder_agent.kgoperations.java_gateway import jpsBaseLibGW
from pyder_agent.kgoperations.store_client import get_store_client
from pyder_agent.common.java_utils import JAVA_INTEGER_TYPE
from typing import Union, Any, List
import logging
from tqdm import tqdm


logger = logging.getLogger(__name__)

jpsBaseLib_view = jpsBaseLibGW.createModuleView()
jpsBaseLibGW.importPackages(jpsBaseLib_view,"uk.ac.cam.cares.jps.base.timeseries.*")

class TimeSeriesClient:
    """Wrapper over Java's TimeSeries Client."""
    def __init__(
        self,
        query_endpoint:str,
        update_endpoint: Union[str,None],
        time_class: Any) -> None:

        store_client = get_store_client(
            query_endpoint=query_endpoint,
            update_endpoint=update_endpoint)

        self.query_endpoint = query_endpoint
        self.update_endpoint = update_endpoint
        self.store_client = store_client
        self.time_class = time_class

        self.time_series_client = jpsBaseLib_view.TimeSeriesClient(
            store_client.store_client,                    # kbClient
            time_class,                      # timeClass
            endpoints.POSTGRES_URL,          # rdbURL
            endpoints.POSTGRES_USER,        # user
            endpoints.POSTGRES_PASSWORD)    # password

    def add_time_series(
        self,
        data_iris: List[str],
        data_iri_classes: List[Any],
        time_stamps: List[Any],
        iri_values: List[List[Any]]) -> None:
        """Utility method that initializes time series tables,
           and creates and adds time series data."""

        self.initTimeSeries(
            data_iris=data_iris,
            data_iri_classes=data_iri_classes)

        timeseries_data = self.createTimeSeriesData(
            time_stamps= time_stamps,
            data_iris= data_iris,
            iri_values=iri_values)

        self.addTimeSeriesData(timeseries_data)

    def bulkInitTimeSeries(
        self,
        data_iris: List[List[str]],
        data_iri_classes: List[List[Any]]
        )->None:
        """Bulk initialises time series tables."""

        # Initialise time series in both KG and RDB using TimeSeriesClass
        self.time_series_client.bulkInitTimeSeries(
            data_iris,            # list of dataIRIs as Strings
            data_iri_classes,     # list of data classes for each dataIRI
            None)

    def bulk_add_time_series(
        self,
        data_iris: List[List[str]],
        data_iri_classes: List[List[Any]],
        time_stamps: List[List[Any]],
        iri_values: List[List[List[Any]]]) -> None:
        """Utility method that bulk initialises time series tables
           and adds data to them."""

        self.bulkInitTimeSeries(
            data_iris=data_iris,
            data_iri_classes=data_iri_classes)

        logger.info(f"Adding {len(time_stamps)} time series tables")
        with tqdm(total=len(time_stamps)) as pbar:
            for time_stamp, data_iri, iri_value in zip(time_stamps,data_iris,iri_values):
                timeseries_data = self.createTimeSeriesData(
                    time_stamps= time_stamp,
                    data_iris= data_iri,
                    iri_values=iri_value)
                self.addTimeSeriesData(timeseries_data)
                pbar.update(1)

    @staticmethod
    def createTimeSeriesData(
        time_stamps: List[Any],
        data_iris: List[str],
        iri_values: List[List[Any]])->Any:
        """Utility method that creates time series object."""

        timeseries_data = jpsBaseLib_view.TimeSeries(
            time_stamps,           # list of timestamps
            data_iris,             # list of data IRIs provided as string
            iri_values)            # list of list of values containing the data for each data IRI

        return timeseries_data

    def initTimeSeries(
        self,
        data_iris: List[str],
        data_iri_classes: List[Any])->None:
        """Initialises time series tables. See also bulkInitTimeSeries method."""

        # Initialise time series in both KG and RDB using TimeSeriesClass
        self.time_series_client.initTimeSeries(
            data_iris,            # list of dataIRIs as Strings
            data_iri_classes,     # list of data classes for each dataIRI
            None)

    def addTimeSeriesData(
        self,
        timeseries_data: Any)->None:
        """Adds time series data to the corresponding tables."""

        self.time_series_client.addTimeSeriesData(timeseries_data)

    def getMaxTime(
        self,
        iri: str)->Any:
        """Returns max time for a given table entry."""

        return self.time_series_client.getMaxTime(iri)

    def getTimeSeriesWithinBounds(
        self,
        iri_list: List[str],
        lower_time_bound: Any,
        upper_time_bound: Any)->Any:
        """Returns time series object given table entries iris and time bounds."""

        return self.time_series_client.getTimeSeriesWithinBounds(iri_list, lower_time_bound, upper_time_bound)

    @staticmethod
    def getTimeSeriesValues(
        iri: str,
        time_series: Any)->Any:
        """Returns values in a time series object for a particular iri."""

        return time_series.getValues(iri)

    def get_most_recent_state_value(
        self,
        state_iri: str)->Any:
        """Returns the most recent time series value for a given iri entry."""

        max_time = self.time_series_client.getMaxTime(state_iri)
        time_series = self.getTimeSeriesWithinBounds(
            iri_list = [state_iri],
            lower_time_bound = max_time,
            upper_time_bound = max_time)

        state_value = self.getTimeSeriesValues(iri=state_iri, time_series=time_series)
        return state_value[0]

    def getLatestData(
        self,
        state_iri: str
        )->Any:
        """Returns the most recent time series object for a given entry iri."""

        time_series = self.time_series_client.getLatestData(state_iri)
        return time_series

    def get_latest_values(
        self,
        state_iri: str
        )->Any:

        time_series = self.getLatestData(state_iri=state_iri)

        latest_values = self.getTimeSeriesValues(
            iri=state_iri,
            time_series=time_series)
        return latest_values

    def deleteAll(self)->None:
        """Deletes all time series data."""
        self.time_series_client.deleteAll()

def get_default_time_series_client():
    return TIME_SERIES_CLIENT

def __get_time_series_client(
    query_endpoint:str,
    update_endpoint:str,
    time_class: Any
    )->TimeSeriesClient:
    """Factory function for creating the time series clients."""


    time_series_client = TimeSeriesClient(
        query_endpoint=query_endpoint,
        update_endpoint=update_endpoint,
        time_class=time_class)

    return time_series_client

def drop_all_time_series()->None:
    """Utility function to delete all time series."""

    time_series_client = get_default_time_series_client()

    time_series_client.deleteAll()

TIME_SERIES_CLIENT = __get_time_series_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL,
        time_class=JAVA_INTEGER_TYPE)