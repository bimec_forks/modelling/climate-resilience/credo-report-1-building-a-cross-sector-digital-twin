import pyder_agent.common.endpoints as endpoints
from typing import List, Union, Any
from pyder_agent.kgoperations.store_client import get_store_client
from pyder_agent.kgoperations.java_gateway import jpsBaseLibGW

jpsBaseLib_view = jpsBaseLibGW.createModuleView()
jpsBaseLibGW.importPackages(jpsBaseLib_view,"uk.ac.cam.cares.jps.base.derivation.*")

#=====================================================================================
#    _________________________
#   |     Root State 1        |
#   |   (derived_from_iri)    |
#   | (top_derived_from_iri1) |
#   |_________________________|              ____________
#     ^                  isDerivedUsing    | AgentURL 1 |
#     | isDerivedFrom /------------------> |____________|
#     |              /
#    _______________/             _____________________     ________________________
#   |  Derivation 1 |<----------| Intermediate State  |   |      Leaf State  1     |
#   |_______________| belongsTo |  (belongs_to_iri)   |   |    (belongs_to_iri)    |
#      |                        |  (derived_from_iri) |   |                        |
#      |                        |_____________________|   |________________________|
#      |                                            ^        |
#      | isDerivedFrom               isDerivedFrom  |        | belongsTo
#      v                                            |        |
#    _________________________                     _|________v_____________
#   |      Root State 2       |                   |  Derivation 2          |
#   |   (derived_from_iri)    |                   | (leaf_derivation_iri1) |
#   | (top_derived_from_iri2) |                   |________________________|
#   |_________________________|                            \        ____________
#                                            isDerivedUsing \ ---> | AgentURL 2 |
#                                                                  |____________|
#
#=====================================================================================

class DerivationClient:
    """Wrapper over Java's Derivation Client."""
    def __init__(
        self,
        query_endpoint:str,
        update_endpoint:Union[str,None])->None:

        store_client = get_store_client(
            query_endpoint=query_endpoint,
            update_endpoint=update_endpoint)

        self.store_client = store_client

        self.derivation_client: Any = jpsBaseLib_view.DerivationClient(store_client.store_client)

    def createDerivationWithTimeSeries(
        self,
        belongs_to_iris: List[str],
        agent_iri: str,
        derived_from_iris: List[str],
        update_agent_url: str
        ) -> str:
        """Creates derivation triples with time series attached to them.
           States time series should exist prior calling this method.
           See also a more efficient bulkCreateDerivationsWithTimeSeries."""

        derivation = self.derivation_client.createDerivationWithTimeSeries(
            belongs_to_iris,           # list of entities
            agent_iri,                # agentIRI
            update_agent_url,         # agentURL (should be called update_state!)
            derived_from_iris)        # list inputsIRI
        return derivation


    def bulkCreateDerivationsWithTimeSeries(
        self,
        belongs_to_iris: List[List[str]],
        agent_iris: List[str],
        derived_from_iris: List[List[str]],
        update_agent_urls: List[str]
        )->List[str]:
        """Bulk creates derivation triples with time series attached to them.
           States time series should exist prior calling this method."""

        derivations = self.derivation_client.bulkCreateDerivationsWithTimeSeries(
            belongs_to_iris,          # List<List<String>> entitiesList
            agent_iris,               # List<String> agentIRIList
            update_agent_urls,        # List<String> agentURLList
            derived_from_iris)        # List<List<String>> inputsList
        return derivations

    def removeTimeInstance(
        self,
        iri:str)->None:
        """Removes a single time instance attached to iri."""

        self.derivation_client.removeTimeInstance(iri)

    def addTimeInstance(
        self,
        iris:List[str])->None:
        """Adds time instances to specified iris."""

        self.derivation_client.addTimeInstance(iris)

    def validateDerivations(
        self,
        )->bool:
        """Validates derivations nodes connectivity"""

        result = False
        result = self.derivation_client.validateDerivations()
        return result

    def updateTimestamp(
        self,
        iri:str)->None:
        """Updates time stamp for an indicated iri."""

        self.derivation_client.updateTimestamp(iri)

    def dropAllDerivationsAndTimestamps(self)->None:
        """Drops all derivations and time stamp triples."""
        self.derivation_client.dropAllDerivationsAndTimestamps()

def get_derivation_client(
    query_endpoint:str,
    update_endpoint:Union[str,None]=None)->DerivationClient:
    """Utility function for creating derivation clients."""

    derivation_client = DerivationClient(query_endpoint, update_endpoint)
    return derivation_client


def drop_all_derivations()->None:
    """Utility function for removing all derivation triples."""

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    derivation_client.dropAllDerivationsAndTimestamps()