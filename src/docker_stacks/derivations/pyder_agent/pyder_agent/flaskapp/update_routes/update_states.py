import pyder_agent.common.endpoints as endpoints
from pyder_agent.time_series.time_series_client import get_default_time_series_client
from pyder_agent.common.java_utils import JAVA_INTEGER_TYPE
import pyder_agent.common.query_templates as qtmpl
from pyder_agent.common.simulation_time import SIMULATION_TIME_IRI
import pyder_agent.app_exceptions.app_exceptions as appexc
import json
import logging
from typing import List, Dict, Any, Tuple, Optional, Callable

logger = logging.getLogger(__name__)

NOT_ACTIVE = "NOT_ACTIVE"

class BelongsTo_Dependency:
    """Helper class, wrapping data and logic for a given derivedFrom dependency"""
    def __init__(
        self,
        name: str,                       # name, can be anything
        included_states: List[str],      # derivedFrom states required by this dependency
        processing_function: Callable):  # how to process derivedFrom states values

        self.name = name
        self.included_states = {state:[] for state in included_states}
        self.processing_function = processing_function
        self.processed_result = None
        self.is_active = False

    def set_dependency_value(
        self,
        state,
        value):

        temp_list = self.included_states[state]
        temp_list.extend(value)
        self.included_states[state] = temp_list

    def get_processed_value(self):
        # create a single list holding included_states values
        # in case, a given state does not have any value, use None
        # for e.g.
        #    included_states = {"state1": [True,False], "state2": [False]}
        # the outcome should be:
        #    state_values = [True,False,False]
        # and for:
        #    included_states = {"state1": [True,False], "state2": [None]}
        #    state_values = [True,False,None]
        state_values = []
        empty_state_values = []
        for state, values in self.included_states.items():
            if not values:
                # additionally catch states that were empty
                empty_state_values.append(state)
                state_values.append(None)
            else:
                state_values.extend(values)

        # now process that the states_values using the processing logic function
        if not self.is_active:
            # dependency not found in provided derivedFrom iris
            # so it wont be used in calculating the final belongsTo states values
            # set its value using special NOT_ACTIVE tag
            return NOT_ACTIVE
        elif not empty_state_values:
            return self.processing_function(state_values)
        else:
            # some values are None for this dependency, cant apply the processing function
            raise appexc.MissingRequiredDerivedFromStates(f"Error: Empty derivedFrom values found in {self.name} dependency.")


class BelongsTo_Logic:
    """Helper class, wrapping main logic for setting belongsTo states values."""
    def __init__(self,
        enable_manual_override=True):

        self.dependencies = []           # list of all belongsTo state dependencies
        self.dependencies_values = {}    # dictionary storing the final value of each dependency
        self.enable_manual_override = enable_manual_override

    def add_dependency(
        self,
        name: str,
        included_states: List[str],
        processing_function: Callable)->Any:

        self.dependencies.append(
            BelongsTo_Dependency(
                name=name,
                included_states=included_states,
                processing_function=processing_function))
        return self

    def get_value_from_dependencies(
        self,
        derivedFrom_values,
        )->bool:

        # work out belongsTo state value based on its dependencies
        for dependency in self.dependencies:
            # the inner loop populates each specified dependency statelse:
            # using provided derivedFrom states
            for derivedFrom_state, derivedFrom_value in derivedFrom_values.items():
                for dependency_state in dependency.included_states.keys():
                    if derivedFrom_state == dependency_state:
                    # add dervied from state and its value to the current
                    # dependency, and set it as active
                        dependency.set_dependency_value(
                            state=dependency_state,
                            value=derivedFrom_value)
                        dependency.is_active = True

            # after all derivedFrom states been processed for this dependency,
            # calculate the final dependency value and store it in the main
            # belongs to logic class
            self.dependencies_values[dependency.name] = dependency.get_processed_value()

        # after processing all dependencies, filter them, removing all not active ones
        # (those without any derivedFrom states)
        active_state_values = [state_value
                               for state_value in self.dependencies_values.values()
                               if state_value != NOT_ACTIVE]

        # if all dependencies were not active, this is an error
        if not active_state_values:
            raise appexc.NoActiveBelongsToStateDependencies("Error: All specified belongsTo state dependencies are not found.")

        # Use AND operator on all active dependency values to calculate the final belongsTo states values
        return all(active_state_values)


def update_states(
    request: Any,
    belongsTo_state_logic: BelongsTo_Logic
    )->List[List[bool]]:
    """Main function handling all the update state requests."""

    logger.info(f"Processing query: {request.args['query']}")
    query_str = request.args["query"]
    query = json.loads(query_str)
    derivedFrom_iris = query["agent_input"]
    belongsTo_iris = query["belongsTo"]

    logger.info(f"Read derived_from_iris: {derivedFrom_iris}")
    logger.info(f"Read belongs_to_iris: {belongsTo_iris}")

    if not derivedFrom_iris:
        raise appexc.EmptyDerivedFromIris("Error: Passed derivedFrom iris list is empty.")
    if not belongsTo_iris:
        raise appexc.EmptyBelongsToIris("Error: Passed BelongsTo iris list is empty.")

    belongsTo_values: List[List[bool]] = []

    time_series_client = get_default_time_series_client()

    # query for derivedFrom state types (via their has_[state] predicates)
    # and for any manual overrite states attached to the belongsTo states
    derivation_states_info = qtmpl.get_derivation_states_info(
        derivedFrom_iris=derivedFrom_iris,
        belongsTo_iris=belongsTo_iris)

    if not derivation_states_info:
        raise appexc.EmptyKGResponse("KG query for the derivation states info returned nothing.")
    else:
       belongsTo_manual_overrides, derivedFrom_values = _process_states_info_response(
            derivation_states_info = derivation_states_info[0],
            belongsTo_iris = belongsTo_iris,
            derivedFrom_iris = derivedFrom_iris,
            time_series_client = time_series_client)

    # calculate belongsTo states value from tis derivedFrom states for a later use
    value_from_dependencies = belongsTo_state_logic.get_value_from_dependencies(
        derivedFrom_values=derivedFrom_values
    )

    # set all belongsTo state values
    for iri in belongsTo_iris:
        man_override = belongsTo_manual_overrides[iri]
        if man_override is not None and belongsTo_state_logic.enable_manual_override:
            belongsTo_values.append([man_override])
        else:
            belongsTo_values.append([value_from_dependencies])

    # write new belongsTo state values back to time series tables
    new_timestamps = [time_series_client.getMaxTime(SIMULATION_TIME_IRI)]*len(belongsTo_iris)

    timeseries_data = time_series_client.createTimeSeriesData(
        time_stamps=new_timestamps,
        data_iris=belongsTo_iris,
        iri_values=belongsTo_values)

    time_series_client.addTimeSeriesData(timeseries_data=timeseries_data)

    return belongsTo_values

def _process_states_info_response(
    derivation_states_info: Dict[str,str],
    belongsTo_iris: List[str],
    derivedFrom_iris: List[str],
    time_series_client: Any
    )-> Tuple[Dict[str,Optional[bool]],Dict[str,bool]]:
    """Helper function that processes the KG response object."""

    belongsTo_states_man_overrides={}
    derivedFrom_states_values={}
    for i, iri in enumerate(belongsTo_iris):
        man_override_iri = derivation_states_info.pop(f'belongsTo_man_override_{i}',None)
        man_override_value = None
        if man_override_iri is not None:
            man_override_value = time_series_client.get_latest_values(
                state_iri=man_override_iri)[0]
        belongsTo_states_man_overrides[iri] = man_override_value

    for i, iri in enumerate(derivedFrom_iris):
        derived_from_value = time_series_client.get_latest_values(
            state_iri=iri)[0]
        predicate = derivation_states_info[f'derivedFrom_predicate_{i}']
        if predicate not in derivedFrom_states_values:
            derivedFrom_states_values[predicate] = [derived_from_value]
        else:
            temp_list = derivedFrom_states_values[predicate]
            temp_list.append(derived_from_value)
            derivedFrom_states_values[predicate] = temp_list
    return (belongsTo_states_man_overrides, derivedFrom_states_values)