from flask import Blueprint, request, jsonify
import pyder_agent.common.tbox_concepts as tbox
from pyder_agent.flaskapp.update_routes.update_states import BelongsTo_Logic, update_states
import logging

logger = logging.getLogger(__name__)

UPDATE_WATER_STATES_ROUTE="/pyder_agent/update_water_states"

update_water_states_bp = Blueprint(
    'update_water_states_bp', __name__
)

# Define a route for API requests
# should be called update_state
@update_water_states_bp.route(UPDATE_WATER_STATES_ROUTE, methods=['GET'])
def api():
    belongsTo_state_logic = BelongsTo_Logic(
        enable_manual_override=True) \
        .add_dependency(
            name="is_not_flooded",
            included_states=[tbox.HAS_FLOOD_STATE],
            processing_function=lambda x: not x[0]) \
        .add_dependency(
            name="is_power_supplied",
            included_states=[tbox.HAS_POWER_STATE],
            processing_function=lambda x: any(x)) \
        .add_dependency(
            name="is_water_supplied",
            included_states=[tbox.HAS_WATER_STATE],
            processing_function=lambda x: any(x))

    try:
        belongsTo_values = update_states(
            request=request,
            belongsTo_state_logic=belongsTo_state_logic)
    except Exception as ex:
        logger.error("Error: Couldn't set the belongsTo state values.")
        logger.exception(ex)
        return jsonify(None), 500

    return jsonify(belongsTo_values), 200