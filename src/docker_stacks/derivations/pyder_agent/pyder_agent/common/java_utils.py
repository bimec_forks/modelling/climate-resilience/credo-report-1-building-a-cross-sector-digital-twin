from pyder_agent.kgoperations.java_gateway import jpsBaseLibGW

jpsBaseLib_view = jpsBaseLibGW.createModuleView()

JAVA_BOOLEAN_TYPE = jpsBaseLib_view.java.lang.Boolean.TYPE
JAVA_INTEGER_TYPE = jpsBaseLib_view.java.lang.Integer.TYPE
JAVA_DOUBLE_TYPE = jpsBaseLib_view.java.lang.Double.TYPE
#JAVA_INSTANT = jpsBaseLib_view.java.time.Instant
#JAVA_INSTANT_CLASS = JAVA_INSTANT.now().getClass()