from pyder_agent.kgoperations.store_client import get_store_client
import pyder_agent.common.endpoints as endpoints
from typing import List, Dict

def get_assets_states()->List[Dict[str,str]]:
    query="""
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT DISTINCT ?asset ?asset_state ?has_state_property
    WHERE {

    ?has_state_property rdfs:subPropertyOf* credo:hasState .

    SERVICE <#ontop_endpoint#> {
        ?asset ?has_state_property ?asset_state .
        }
    }ORDER BY ?asset""".replace("#ontop_endpoint#",endpoints.ONTOP_SPARQL)

    store_client = get_store_client(query_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    response = store_client.executeQuery(query)
    return response

def add_manual_state_override(
    state_iri: str)->str:

    manual_state_override_iri = f"{state_iri}/manualOverride"

    query = """
        PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
        PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
        INSERT {
            <#state_iri#> credo:hasManualOverride <#manual_override#> .
            ?derivation der:isDerivedFrom <#manual_override#> .
            <#manual_override#> a ?state_iri_type .
        }
        WHERE {
            <#state_iri#> der:belongsTo ?derivation .
            SERVICE <#ontop_iri#> {
                <#state_iri#> a ?state_iri_type_in .
            }
             BIND(?state_iri_type_in AS ?state_iri_type)
        }
    """.replace("#state_iri#", state_iri) \
       .replace("#manual_override#", manual_state_override_iri) \
       .replace("#ontop_iri#", endpoints.ONTOP_SPARQL)

    store_client = get_store_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    response = store_client.executeUpdate(query)
    return manual_state_override_iri

def drop_manual_state_overrides()->int:
    query = """
        PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
        DELETE {
            ?manual_override ?x1 ?y1 .
            ?x2 ?y2 ?manual_override .
        }
        WHERE {
            ?asset credo:hasManualOverride ?manual_override .
            ?manual_override ?x1 ?y1 .
            ?x2 ?y2 ?manual_override .
        }"""

    store_client = get_store_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    status_code = store_client.executeUpdate(query)
    return status_code

def get_derivation_states_info(
    derivedFrom_iris: List[str],
    belongsTo_iris: List[str])->List[Dict[str,str]]:

    derivedFrom_pred_select = ' '.join([f"?derivedFrom_predicate_{i}"
                                         for i in range(len(derivedFrom_iris))])
    derivedFrom_pred_query = '\n'.join([f"?asset_{i} ?derivedFrom_predicate_{i}  <{iri}> ."
                                         for i, iri in enumerate(derivedFrom_iris)])
    belongsTo_man_override_select = ' '.join([f"?belongsTo_man_override_{i}"
                                              for i in range(len(belongsTo_iris))])
    belongsTo_man_override_query = '\n'.join([f"OPTIONAL {{<{iri}> credo:hasManualOverride ?belongsTo_man_override_{i} .}}"
                                              for i, iri in enumerate(belongsTo_iris)])

    query = """PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
               PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
               SELECT DISTINCT #belongsTo_man_override_select# #derivedFrom_pred_select#
               WHERE {
                   #belongsTo_man_override_query#
                   SERVICE <#ontop_iri#> {
                        #derivedFrom_pred_query#
                   }
               }""".replace("#ontop_iri#", endpoints.ONTOP_SPARQL) \
                   .replace("#derivedFrom_pred_select#", derivedFrom_pred_select) \
                   .replace("#derivedFrom_pred_query#", derivedFrom_pred_query) \
                   .replace("#belongsTo_man_override_select#", belongsTo_man_override_select) \
                   .replace("#belongsTo_man_override_query#", belongsTo_man_override_query) \

    store_client = get_store_client(query_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_root_states():
    query = """
        PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
        SELECT DISTINCT ?root_state
        WHERE {
            ?derivation1 der:isDerivedFrom ?root_state .
            MINUS{?root_state der:belongsTo ?derivation2 .}
        }"""

    store_client = get_store_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    response = store_client.executeQuery(query)
    return response

def get_leaf_derivations():
    query = """
        PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
        SELECT DISTINCT ?leaf_derivation
        WHERE {
            ?leaf_state der:belongsTo ?leaf_derivation .
            MINUS{?derivation der:isDerivedFrom ?leaf_state .}
        }"""

    store_client = get_store_client(
        query_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    response = store_client.executeQuery(query)
    return response