from pyder_agent.kgoperations.store_client import get_store_client
import pyder_agent.common.endpoints as endpoints
from typing import List, Dict, Any

def get_asset_phone_states()->List[Dict[str,Any]]:

    query = """
        PREFIX bt: <http://theworldavatar.com/ontology/ontocredo/ontobt.owl#>
        SELECT DISTINCT ?phone_state
        WHERE {
            ?phone_state a bt:TelephoneState .
        }"""
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_phone_state_dependecies():
    # TODO: Add later power states
    query = """
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX bt: <http://theworldavatar.com/ontology/ontocredo/ontobt.owl#>
    PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
    SELECT ?child ?child_phone_state ?child_type ?child_phone_type ?child_power_state ?child_flood_state ?parent ?parent_phone_state
    WHERE {
        ?child bt:hasTelephoneState ?child_phone_state ;
               a ?child_type .

        OPTIONAL {?parent bt:suppliesPhoneTo ?child ;
                bt:hasTelephoneState ?parent_phone_state . }
        OPTIONAL {?child ukpn:hasPowerState ?child_power_state . }
        OPTIONAL {?child bt:hasConnectionType ?child_conn_type .
                  ?child_conn_type credo:hasValue ?child_phone_type .}
        OPTIONAL {?child credo:hasFloodState ?child_flood_state . }
    }
    """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_root_phone_states()->List[Dict[str,Any]]:
    query = """
        PREFIX bt: <http://theworldavatar.com/ontology/ontocredo/ontobt.owl#>
        SELECT DISTINCT ?root_phone_state
        WHERE {
            ?root_asset bt:suppliesPhoneTo ?child_asset ;
                        bt:hasTelephoneState ?root_phone_state .

            MINUS {?other_asset bt:suppliesPhoneTo ?root_asset .}
        }
        """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response


def get_leaf_phone_state_derivations()->List[Dict[str,Any]]:
    query = """
        PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
        PREFIX bt: <http://theworldavatar.com/ontology/ontocredo/ontobt.owl#>
        SELECT DISTINCT ?leaf_phone_state_derivation
        WHERE {
            { SELECT ?leaf_phone_state
                WHERE {

                BIND (?leaf_phone_state_in AS ?leaf_phone_state)
                SERVICE <#ontop_endpoint#> {
                    ?asset bt:suppliesPhoneTo ?leaf_asset .
                    ?leaf_asset bt:hasTelephoneState ?leaf_phone_state_in .
                    MINUS {?leaf_asset bt:suppliesPhoneTo ?other_asset .}
                }
                }
            }
            ?leaf_phone_state der:belongsTo ?leaf_phone_state_derivation .

        }""".replace('#ontop_endpoint#', endpoints.ONTOP_SPARQL)

    store_client = get_store_client(query_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    response = store_client.executeQuery(query)
    return response