# Description

`pyder_agent` is a flask-based python web agent responsible for the derivation framework operations on the CreDo digital twin.

# Requirements

- You need Python >=3.5. You can install Python by going to the official Python [download page](https://www.python.org/getit/)
- You also need to install a [Java Runtime Environment version 8](https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=hotspot)

# Installation #

To install the agent, simply run:

```sh
$ python -m pip install .
```

# How to use #

To be added..

# Uploading to DAFNI
There are two separate docker files and model definition files, to compile, navigate to "base-repo/src/docker_stacks" and run from WSL
```
./scripts/prep_dafni_model.sh update-agent -m derivations/pyder_agent/ -f Dockerfile-agent
./scripts/prep_dafni_model.sh information-cascade-model-initialiser -m derivations/pyder_agent/ -f Dockerfile-uploader
```

# Authors #
Daniel Nurkowski (danieln@cmclinnovations.com), 11 November 2021