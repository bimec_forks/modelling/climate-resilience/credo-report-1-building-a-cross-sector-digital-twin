from pyder_agent.flaskapp import create_app
from pyder_agent.flaskapp.update_routes.update_flood_states import UPDATE_FLOOD_STATES_ROUTE
from pyder_agent.flaskapp.update_routes.update_power_states import UPDATE_POWER_STATES_ROUTE
from pyder_agent.flaskapp.update_routes.update_phone_states import UPDATE_PHONE_STATES_ROUTE
from pyder_agent.flaskapp.update_routes.update_water_states import UPDATE_WATER_STATES_ROUTE
from pyder_agent.flaskapp.update_routes.update_sewage_states import UPDATE_SEWAGE_STATES_ROUTE
from pyder_agent.flaskapp.update_routes.update_sludge_states import UPDATE_SLUDGE_STATES_ROUTE
import pyder_agent.common.tbox_concepts as tbox
import pytest
import os
import json

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
TS_CLIENT_IMPORT_STR = 'pyder_agent.time_series.time_series_client.TimeSeriesClient'
UPDATE_STATES_IMPORT_STR='pyder_agent.flaskapp.update_routes.update_states.qtmpl'

FLOOD_ST_PREF = 'flood_state'
FLOOD_DEPTH_PREF = 'flood_depth_state'
FLOOD_MAX_DEPTH_PREF = 'flood_max_depth_state'
POWER_ST_PREF = 'power_state'
PHONE_ST_PREF = 'phone_state'
WATER_ST_PREF = 'water_state'
SEWAGE_ST_PREF = 'sewage_state'
SLUDGE_ST_PREF = 'sludge_state'
MAN_OVER_PREF = 'manual_override_state'

FLOOD_TEST = "Flood test"
POWER_TEST = "Power test"
PHONE_TEST = "Phone test"
WATER_TEST = "Water test"
SEWAGE_TEST = "Sewage test"
SLUDGE_TEST = "Sludge test"

STATE_PREFIXES = {FLOOD_ST_PREF: tbox.HAS_FLOOD_STATE,
                  FLOOD_DEPTH_PREF: tbox.HAS_FLOOD_DEPTH_STATE,
                  FLOOD_MAX_DEPTH_PREF: tbox.HAS_FLOOD_MAX_DEPTH_STATE,
                  POWER_ST_PREF: tbox.HAS_POWER_STATE,
                  PHONE_ST_PREF: tbox.HAS_TELEPHONE_STATE,
                  WATER_ST_PREF: tbox.HAS_WATER_STATE,
                  SEWAGE_ST_PREF: tbox.HAS_SEWAGE_STATE,
                  SLUDGE_ST_PREF: tbox.HAS_SLUDGE_STATE}

@pytest.fixture
def client():
    app = create_app({'TESTING': True})
    with app.test_client() as client:
        yield client

@pytest.mark.parametrize("flood_depths", [0.2, 0.8, None])
@pytest.mark.parametrize("flood_max_depths", [0.7, None])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_flood_updates_route(
    flood_depths,
    flood_max_depths,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())

    if flood_depths is not None:
        derived_from_data[FLOOD_DEPTH_PREF] = flood_depths
    if flood_max_depths is not None:
        derived_from_data[FLOOD_MAX_DEPTH_PREF] = flood_max_depths

    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=FLOOD_TEST)
    response = client.get(UPDATE_FLOOD_STATES_ROUTE, query_string=query)
    assert response.json == expected_result

@pytest.mark.parametrize("flood_state", [True, False, None])
@pytest.mark.parametrize("power_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_power_updates_route(
    flood_state,
    power_states,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())
    if flood_state is not None:
        derived_from_data[FLOOD_ST_PREF] = flood_state
    if power_states is not None:
        for i, power_state in enumerate(power_states):
            derived_from_data[f"{POWER_ST_PREF}_{i}"] = power_state

    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=POWER_TEST)
    response = client.get(UPDATE_POWER_STATES_ROUTE, query_string=query)
    assert response.json == expected_result


@pytest.mark.parametrize("flood_state", [True, False, None])
@pytest.mark.parametrize("power_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("phone_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_phone_updates_route(
    flood_state,
    power_states,
    phone_states,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())
    if flood_state is not None:
        derived_from_data[FLOOD_ST_PREF] = flood_state
    if power_states is not None:
        for i, power_state in enumerate(power_states):
            derived_from_data[f"{POWER_ST_PREF}_{i}"] = power_state
    if phone_states is not None:
        for i, phone_state in enumerate(phone_states):
            derived_from_data[f"{PHONE_ST_PREF}_{i}"] = phone_state


    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=PHONE_TEST)
    response = client.get(UPDATE_PHONE_STATES_ROUTE, query_string=query)
    assert response.json == expected_result


@pytest.mark.parametrize("flood_state", [True, False, None])
@pytest.mark.parametrize("power_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("water_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_water_updates_route(
    flood_state,
    power_states,
    water_states,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())
    if flood_state is not None:
        derived_from_data[FLOOD_ST_PREF] = flood_state
    if power_states is not None:
        for i, power_state in enumerate(power_states):
            derived_from_data[f"{POWER_ST_PREF}_{i}"] = power_state
    if water_states is not None:
        for i, water_state in enumerate(water_states):
            derived_from_data[f"{WATER_ST_PREF}_{i}"] = water_state

    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=WATER_TEST)
    response = client.get(UPDATE_WATER_STATES_ROUTE, query_string=query)
    assert response.json == expected_result

@pytest.mark.parametrize("flood_state", [True, False, None])
@pytest.mark.parametrize("power_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("sewage_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_sewage_updates_route(
    flood_state,
    power_states,
    sewage_states,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())
    if flood_state is not None:
        derived_from_data[FLOOD_ST_PREF] = flood_state
    if power_states is not None:
        for i, power_state in enumerate(power_states):
            derived_from_data[f"{POWER_ST_PREF}_{i}"] = power_state
    if sewage_states is not None:
        for i, sewage_state in enumerate(sewage_states):
            derived_from_data[f"{SEWAGE_ST_PREF}_{i}"] = sewage_state

    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=SEWAGE_TEST)
    response = client.get(UPDATE_SEWAGE_STATES_ROUTE, query_string=query)
    assert response.json == expected_result


@pytest.mark.parametrize("flood_state", [True, False, None])
@pytest.mark.parametrize("power_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("sludge_states", [[True], [False], None, [True, True], [True, False], [False, False]])
@pytest.mark.parametrize("belongsTo_iris_and_man_overrides",
                          [{"belongsTo_iri1":True},
                           {"belongsTo_iri1":False},
                           {"belongsTo_iri1":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":True},
                           {"belongsTo_iri1":True, "belongsTo_iri2":False},
                           {"belongsTo_iri1":False, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":False},
                           {"belongsTo_iri1":None, "belongsTo_iri2":True},
                           {"belongsTo_iri1":False, "belongsTo_iri2":None},
                           {"belongsTo_iri1":True, "belongsTo_iri2":None},
                           {"belongsTo_iri1":None, "belongsTo_iri2":None},
                          ]
                          )
def test_sludge_updates_route(
    flood_state,
    power_states,
    sludge_states,
    belongsTo_iris_and_man_overrides,
    client,
    mocker):

    derived_from_data = {}
    belongsTo_iris = list(belongsTo_iris_and_man_overrides.keys())
    man_overrides= list(belongsTo_iris_and_man_overrides.values())
    if flood_state is not None:
        derived_from_data[FLOOD_ST_PREF] = flood_state
    if power_states is not None:
        for i, power_state in enumerate(power_states):
            derived_from_data[f"{POWER_ST_PREF}_{i}"] = power_state
    if sludge_states is not None:
        for i, sludge_state in enumerate(sludge_states):
            derived_from_data[f"{SLUDGE_ST_PREF}_{i}"] = sludge_state

    # mock the relevant functions and methods
    mocker.patch(f"{UPDATE_STATES_IMPORT_STR}.get_derivation_states_info",
                 side_effect=_side_effect_get_derivation_states_info(man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.get_latest_values",
                 side_effect=_side_effect_get_latest_values(
                     derived_from_data,
                     belongsTo_iris_and_man_overrides))
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.createTimeSeriesData",
                 return_value=None)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.addTimeSeriesData",
                 side_effect=_side_effect_addTimeSeriesData)
    mocker.patch(f"{TS_CLIENT_IMPORT_STR}.getMaxTime",
                 return_value=[2]*len(belongsTo_iris))

    # put the query string together
    query = {"agent_input": list(derived_from_data.keys()),
                 "belongsTo": belongsTo_iris
            }
    query = f"query={json.dumps(query)}"

    expected_result = get_expected_result(
        derived_from_data,
        belongsTo_iris_and_man_overrides,
        test_type=SLUDGE_TEST)
    response = client.get(UPDATE_SLUDGE_STATES_ROUTE, query_string=query)
    assert response.json == expected_result

#---------------------------
# HELPER FUNCTIONS
#---------------------------
def get_expected_result(
    derived_from_data,
    belongsTo_iris_and_man_overrides,
    test_type):

    def _get_value_from_dependencies(derived_from_data, test_type):
        state_values = {
            FLOOD_ST_PREF: {},
            FLOOD_DEPTH_PREF: {},
            FLOOD_MAX_DEPTH_PREF: {},
            POWER_ST_PREF: {},
            PHONE_ST_PREF: {},
            WATER_ST_PREF: {},
            SEWAGE_ST_PREF: {},
            SLUDGE_ST_PREF: {}
        }

        state_keys = list(state_values.keys())

        for iri, iri_value in derived_from_data.items():
            for state_key in state_keys:
                if state_key in iri:
                    if "test_value" not in state_values[state_key]:
                        state_values[state_key]["test_value"] = []

                    temp_list = state_values[state_key]["test_value"]
                    temp_list.append(iri_value)
                    state_values[state_key]["test_value"]=temp_list

        current_depth = state_values[FLOOD_DEPTH_PREF].pop("test_value", None)
        max_depth = state_values[FLOOD_MAX_DEPTH_PREF].pop("test_value", None)
        flood_state = state_values[FLOOD_ST_PREF].pop("test_value", None)
        power_states = state_values[POWER_ST_PREF].pop("test_value",None)
        phone_states = state_values[PHONE_ST_PREF].pop("test_value", None)
        water_states = state_values[WATER_ST_PREF].pop("test_value", None)
        sewage_states = state_values[SEWAGE_ST_PREF].pop("test_value", None)
        sludge_states = state_values[SLUDGE_ST_PREF].pop("test_value", None)

        if test_type == FLOOD_TEST:
            if current_depth is not None and max_depth is not None:
                return [current_depth[0] > max_depth[0]]
            else:
                return None

        elif test_type == POWER_TEST:
            if flood_state is None and power_states is None:
                return None

            return [
                    (not flood_state[0] if flood_state is not None else True) and \
                    (any(power_states) if power_states is not None else True)
                   ]

        elif test_type == PHONE_TEST:
            if flood_state is None and power_states is None and phone_states is None:
                return None

            return [
                    (not flood_state[0] if flood_state is not None else True) and \
                    (any(power_states) if power_states is not None else True) and \
                    (any(phone_states) if phone_states is not None else True)
                   ]

        elif test_type == WATER_TEST:
            if flood_state is None and power_states is None and water_states is None:
                return None

            return [
                    (not flood_state[0] if flood_state is not None else True) and \
                    (any(power_states) if power_states is not None else True) and \
                    (any(water_states) if water_states is not None else True)
                  ]

        elif test_type == SEWAGE_TEST:
            if flood_state is None and power_states is None and sewage_states is None:
                return None

            return [
                    (not flood_state[0] if flood_state is not None else True) and \
                    (any(power_states) if power_states is not None else True) and \
                    (any(sewage_states) if sewage_states is not None else True)
                  ]

        elif test_type == SLUDGE_TEST:
            if flood_state is None and power_states is None and sludge_states is None:
                return None

            return [
                    (not flood_state[0] if flood_state is not None else True) and \
                    (any(power_states) if power_states is not None else True) and \
                    (any(sludge_states) if sludge_states is not None else True)
                  ]
        else:
            return None

    if not derived_from_data: return None

    value_from_dependencies = _get_value_from_dependencies(derived_from_data, test_type)
    if value_from_dependencies is None: return None

    # test types:
    belongsTo_state_values = []
    for _, man_override in belongsTo_iris_and_man_overrides.items():
        if man_override is not None:
            # MANUAL_OVERRIDE_TYPE
            belongsTo_state_values.append([man_override])
        else:
            belongsTo_state_values.append(value_from_dependencies)

    return belongsTo_state_values

def _side_effect_get_derivation_states_info(
    man_overrides):

    def _side_effect_internal(derivedFrom_iris,belongsTo_iris):
        response = []
        response_dict = {}
        for i, iri in enumerate(derivedFrom_iris):
            for state_prefix, state_predicate in STATE_PREFIXES.items():
                if state_prefix in iri:
                    response_dict[f"derivedFrom_predicate_{i}"]=state_predicate
        for i, (iri, man_override) in enumerate(zip(belongsTo_iris, man_overrides)):
            if man_override is not None:
                response_dict[f"belongsTo_man_override_{i}"]=f"{iri}/{MAN_OVER_PREF}"

        if response_dict:
            response.append(response_dict)
        return response

    return _side_effect_internal

def _side_effect_get_latest_values(
    derived_from_data,
    belongsTo_iris_and_man_overrides):

    def _side_effect_internal(state_iri):
        for iri, state_value in derived_from_data.items():
            if state_iri == iri: return [state_value]

        for iri, state_value in belongsTo_iris_and_man_overrides.items():
            if state_iri == f"{iri}/{MAN_OVER_PREF}": return [state_value]

    return _side_effect_internal

def _side_effect_addTimeSeriesData(*args, **kwargs):
    pass