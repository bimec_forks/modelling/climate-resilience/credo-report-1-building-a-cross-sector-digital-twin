from setuptools import setup, find_packages

setup(
    name='pyder_agent',
    version='1.0.0',
    author='Daniel Nurkowski',
    license='MIT',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=("tests")),
    url="",
    python_requires='>=3.5',
    include_package_data=True,
    install_requires= ['py4jps==1.0.17', 'flask', 'docopt', 'pytest', 'pytest-mock', 'tqdm'],
    entry_points={
        'console_scripts': [
            'trigger_add_derivations=pyder_agent.triggers.trigger_add_derivations:add_derivations'
        ],
    }
)