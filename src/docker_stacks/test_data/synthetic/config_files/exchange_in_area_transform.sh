#!/bin/sh
# Remove any rows that don't have a value in the "SITE_CODE" column
csvgrep -l -S -c "SITE_CODE" -r ".+" < /dev/stdin
# Add a entry for the "out of area" site
echo 0,out of area,out of area,,,,,,
