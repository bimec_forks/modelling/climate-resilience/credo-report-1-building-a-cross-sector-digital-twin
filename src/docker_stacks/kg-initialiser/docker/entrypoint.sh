#!/bin/sh

# Generate the obda file
/app/transform_obda.sh $OBDA_INPUT_DIR $OBDA_OUTPUT_DIR

# Unzip the TBox CSVs, if necessary
for d in $INPUT_DIRS; do
  zip_name="${d}.zip"
  if [ -f "$zip_name" ]; then
    echo "Unzipping TBox CSVs to $d"
    unzip "$zip_name" -d $(dirname $d)
  fi
done

# Run the Tbox uploader
java -jar /app/tbox_uploader-1.0.0-SNAPSHOT.jar

# If the KEEP_ALIVE variable was set, tail /dev/null to prevent the container exiting
if [ -n "$KEEP_ALIVE" ]; then
  tail -f /dev/null
fi