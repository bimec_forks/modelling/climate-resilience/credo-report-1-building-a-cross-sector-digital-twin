﻿
#target illustrator

var sizes = [64]

// Document currently open in Illustrator
var currentDocument = app.activeDocument;

// Log file location
var logFile = File(activeDocument.path + "/export.log");
logFile.open("e");
  
// Ask user where they want to export files to
var exportFolder = File(activeDocument.path);

/*****
    Main function (entry point).
 *****/
function main() {
    // Is variables are valid
    if(currentDocument == null && exportFolder == null) {
        alert( 'Invalid setup, exiting script' );
        return;
    }

    var overlays = currentDocument.layers.getByName('icons');
    var backgrounds = currentDocument.layers.getByName('backgrounds');

    if(overlays == null) {
		overlays = currentDocument.groups.getByName('icons');
		if(overlays == null) {
			alert( 'Could not locate icons layer, exiting script.' );
			return;
		}
    }
    overlays.hidden = false;

    if(backgrounds == null) {
		backgrounds = currentDocument.groups.getByName('backgrounds');
		if(backgrounds == null) {
			alert( 'Could not locate backgrounds layer, exiting script.' );
			return;
		}
    }
    backgrounds.hidden = false;

           
    // Loop through all overlay groups, hiding them
    for(var i = 0; i < overlays.groupItems.length; i++) {
        var group = overlays.groupItems[i];
        group.hidden = true;
    }

    // Loop through all background groups, hiding them
    for(var i = 0; i < backgrounds.groupItems.length; i++) {
        var background = backgrounds.groupItems[i];
        background.hidden = true;
    }

    // For each overlay group
    for(var i = 0; i < overlays.groupItems.length; i++) {
        var overlayGroup = overlays.groupItems[i];
        logFile.writeln("Processing overlay group: " + overlayGroup.name);
        
        // Un-hide
        overlayGroup.hidden = false;
        
        // For each background group
        for(var j = 0; j < backgrounds.groupItems.length; j++) {
            var backgroundGroup= backgrounds.groupItems[j];
           
            backgroundGroup.hidden = false;
            handle(overlayGroup, backgroundGroup);
            backgroundGroup.hidden = true;
        }
            
        // Hide before moving onto next group
        overlayGroup.hidden = true;
    }

    // For each overlay layer (as we may have accidently made sub-layers rather than groups)
    for(var i = 0; i < overlays.layers.length; i++) {
        var overlayGroup = overlays.layers[i];
        logFile.writeln("Processing icon layer: " + overlayGroup.name);
        
        // Un-hide
        overlayGroup.visible = true;
        
        // For each background group
        for(var j = 0; j < backgrounds.groupItems.length; j++) {
            var backgroundGroup = backgrounds.groupItems[j];
            logFile.writeln("Processing background layer: " + backgroundGroup.name);
            
            backgroundGroup.hidden = false;
            handle(overlayGroup, backgroundGroup);
            backgroundGroup.hidden = true;
        }
            
        // Hide before moving onto next group
        overlayGroup.visible = false;
    }
	
	   
	// Output regular empty background
	logFile.writeln("Outputting empty icon using regular background...");
	for(var j = 0; j < backgrounds.groupItems.length; j++) {
		var backgroundGroup = backgrounds.groupItems[j];
		
		if(backgroundGroup.name === "regular") {
			backgroundGroup.hidden = false;
			handle(overlayGroup, backgroundGroup, "empty");
			backgroundGroup.hidden = true;
		}
	}

	logFile.writeln('Script completed, visibilities may have changed.');
	logFile.close();
    alert( 'Script completed, visibilities may have changed.' );
}


/*****
    Loops through all permutations of this overlay and artworks in the input background group.
 *****/
function handle(overlayGroup, backgroundGroup, nameOverride) {
	if(nameOverride == null) nameOverride = false;
	
    // Get the name of the overlay group
    var overlayName = overlayGroup.name;
     
    // Cycle through all backgrounds in the backgroundGroup hiding them
    for(var i = 0; i < backgroundGroup.pathItems.length; i++) {
        var background = backgroundGroup.pathItems[i];
        background.hidden = true;        
    }   

    // Cycle through all backgrounds in the backgroundGroup, exporting for each
    for(var i = 0; i < backgroundGroup.pathItems.length; i++) {
        var background = backgroundGroup.pathItems[i];
        
        background.hidden = false;   
		
		if(background.name === "regular") {
			exportAll(overlayName, "", nameOverride); 
		} else {
			exportAll(overlayName, "-" + background.name, nameOverride);   
		}
		
        background.hidden = true;   
    }   
}


/*****
    Exports the current document in selected sizes/formats.
 *****/
function exportAll(iconName, backgroundName, nameOverride) {
	if(nameOverride == null) nameOverride = false;
	
    for(var i = 0; i < sizes.length; i++) {
        var size = sizes[i];
				
        var fileName = exportFolder + '/' + iconName + backgroundName + ".png";
		if(nameOverride) {
			fileName = exportFolder + '/' + currentDocument.name.split(".")[0] + "-empty.png";
		}
		logFile.writeln("Outputting file at: " + fileName);
       
	    var exportOptions = new ExportOptionsPNG24();
		var scaleFactor = (size / currentDocument.height) * 100;
		
		exportOptions.horizontalScale = scaleFactor;
		exportOptions.verticalScale = scaleFactor;
		exportOptions.transparency = true;
		exportOptions.artBoardClipping = true;
		
  
		currentDocument.exportFile(
			new File(fileName),
			ExportType.PNG24,
			exportOptions
		);
    }
}

// Launch the main function
main();
