/**
 * Start the authentication process.
 * 
 * @param {Function} callback callback token will be passed to.
 */
function authenticate(callback) {
    readKeycloakJSON().then(settings => {
        getToken(settings, callback);
    });
}

/**
 * Attempt to connect to the keycloak server.
 * 
 * @param {Object} settings keycloak settings.
 * @param {Function} callback callback token will be passed to.
 */
function getToken(settings, callback) {
    console.log("Starting the authentication process...");

    // Load the keycloak adapter from the server
    const keycloakAdapter = document.createElement("script");
    keycloakAdapter.src = settings.keycloakUrl + "js/keycloak.js";

    keycloakAdapter.onload = function () {
        // Create the Keycloak instance
        const keycloak = new Keycloak({
            url: settings.keycloakUrl,
            realm: settings.keycloakRealm,
            clientId: settings.keycloakClientId
        });

        // Keycloak initialisation options
        const initOptions = {
            checkLoginIframe: false,
            onLoad: "login-required",
            enableLogging: true
        };

        // Connect to the remote server (logging in if required)
        keycloak.init(initOptions).then(function (authenticated) {
            if (authenticated) {
                console.log("Authenticated with remote Keycloak server.");

                const logout = getParameterByName("logout");
                if (logout !== null) {
                    clearCookie(settings);
                    keycloak.logout({
                        redirectUri: ""
                    });
                } else {
                    // Create the token cookie
                    createCookie(keycloak.token, settings);

                    // Run the callback with the token string
                    if(callback != null) {
                        callback(keycloak.token);
                    }
                }
            } else {
                console.error("Could not authenticate with remote Keycloak server.");
            }
        });
    };

    // Add the adapter to the document to trigger it
    document.head.append(keycloakAdapter);
}

/**
 * Reads the keycloak JSON file.
 * 
 * @returns JSONObject containing keycloak settings.
 */
 async function readKeycloakJSON() {
    try {
        const response = await axios({
            url: "./dafni/keycloak.json",
            method: "get",
            timeout: 30_000,
            headers: {
                "Content-Type": "application/json",
            }
        });

        if (response.status === 200) {
            console.log("Keycloak JSON configuration read successfully.");
            return response.data;
        } 
    } catch (error) {
        console.error("ERROR: Can't access keycloak JSON configuration, will use hardcoded fallback...");
    }
    return null;
}

/**
 * Create the keycloack cookie containing the token.
 * 
 * @param {String} token keycloak token.
 * @param {Object} settings keycloak settings.
 */
 function createCookie(token, settings) {
    let d = new Date();
    let ms = Math.floor(settings.cookieExpirationHours * 60 * 60 * 1000);

    d.setTime(d.getTime() + ms);
    document.cookie = getCookieString(token, d.toUTCString(), settings);
    console.log("Keycloak cookie has been created.");
}

/**
 * Put keycloak cookie in the past and clear.
 * 
 * @param {Object} settings keycloak settings.
 */
function clearCookie(settings) {
    let d = new Date(3796416 * 10 ** 5);
    setCookie(getCookieString("", d.toUTCString(), settings));
    console.log("Keycloak cookie has been cleared.");
}

/**
 * Generate the string for the keycloak cookie.
 * 
 * @param {String} token keycloak token.
 * @param {String} expiration expiration string.
 * @param {Object} settings keycloak settings.
 * 
 * @returns cookie string
 */
function getCookieString(token, expiration, settings) {
    let cookie = settings.cookieName + "=" + encodeURIComponent(token);
    cookie += "; expires=" + expiration;
    cookie += "; domain=" + settings.cookieDomain;

    if (settings.cookieName.startsWith("__Secure-")) {
        cookie += "; secure";
    }
    return cookie + "; path=/";
}

/**
 * Pulls parameter with input name from current URL.
 * 
 * @param {String} name parameter name
 * @param {String} url current URL
 * 
 * @returns resulting value (or null)
 */
 function getParameterByName(name, url) {
    if (url === undefined) {
        url = window.location.href;
    }
    name = name.replace(/[[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}