/**
 * This class handles creating a new MapBox source and layer to sit beneath the
 * existing symbol layers and act as a highlight for the currently selected feature.
 * 
 * This class is part of the visualisation implementation and (at the time) of
 * writing, it not intended to be part of the generic visualisation framework due
 * to the issues listed below.
 * 
 * Current issues with this class:        
 *      - When used to highlight symbol layer features, there is no way to determine
 *        the size of the icon image (as this is relative to the original icon file
 *        size). Here I've assumed images are 64px by 64px and sized to that.
 * 
 *      - Highlight features are currently sized (through manual trial & error) to 
 *        work for the CReDo icons. No support is given for highlighting non-CReDo
 *        symbols, any circle/line/polygon/extrusion layers.
 */
class Highlighter {
    
    // Current MapBox instance.
    _map;

    // Names of highlight layers added to the map.
    _highlightLayers = [];

    // Name of original layers containing features
    _originalLayers = [];

    // Running list of feature IDs to highlight
    _featureIDs = {};

    /**
     * Initialise a new Highlighter instance.
     * 
     * @param {MapBox Map} map current map instance. 
     * @param {LayerHandler} layerHandler main layer handler instance.
     */
    constructor(map, layerHandler) {
        this._map = map;
        this._layerHandler = layerHandler;
    }

    /**
     * Highlight the input feature using the input layer.
     * 
     * @param {JSONObject} feature selected feature 
     * @param {String} color hex color (defaults to black)
     */
    highlightFeature(feature, layerOverride = null) {
        this.remove();
        if(feature == null || !feature["layer"]) return;
        
        let originalLayerName = (layerOverride != null ) ? layerOverride : feature["layer"]["id"];
        if(originalLayerName.endsWith("-focus")) {
            originalLayerName = originalLayerName.replace("-focus", "");
        }
        let newLayerName = originalLayerName + "-highlight";

        // Store original layer name
        if(!this._originalLayers.includes(originalLayerName)) {
            this._originalLayers.push(originalLayerName);
        }

        // Update list of feature IDs to highlight
        let featureID = feature["id"];
        if(this._featureIDs[newLayerName] == null) {
            this._featureIDs[newLayerName] = [];
        }
        if(!this._featureIDs[newLayerName].includes(featureID)) {
            this._featureIDs[newLayerName].push(featureID);
        }

        // Select a highlight layer based on the style
        let styleName = this._map.getStyle()["name"].toLowerCase();
        let color = "#000000";
        if(styleName.includes("dark") || styleName.includes("blueprint")) {
            color = "#FFFFFF";
        } 

        // Create a highlight layer
        if(!this._highlightLayers.includes(newLayerName)) {
            this.#createLayer(newLayerName, feature["source"], color);
            this._highlightLayers.push(newLayerName);
        } 
        this.#filterLayer(newLayerName);

        // Re-order highlight layers
        this.#orderLayers();
    }

    /*
     * Removes the highlight layer and source from the map.
     */
    remove() {
        this._highlightLayers.forEach(layerName => {
            this._map.removeLayer(layerName);
        });
        this._highlightLayers = [];
        this._originalLayers = [];
        this._featureIDs = {};
    }

    /**
     * Creates a special highlight layer.
     */
    #createLayer(layerName, sourceName, color) {
        let oldName = layerName.replace("-highlight", "");

        // Note that the circle-radius here has been manually set to work
        // with the 64px by 64px CReDo icons.
        let options = {
            id: layerName,
			source: sourceName,
            metadata: {
                provider: "cmcl"
            },
            type: 'circle',
			layout: {
				'visibility': 'visible'
			},
			paint: {
                'circle-radius': ['interpolate', ["linear"], ["zoom"], 10, 25, 15, 35], 
				'circle-color':  color,
                'circle-opacity': 0.85,
                'circle-blur': 0.5
			}
        }
        this._map.addLayer(options, oldName);
    }

    /**
     * Updates the filter on the input layer to only include features with IDs
     * within
     * @param {String} layerName name of layer to filter 
     */
    #filterLayer(layerName) {
        let allowedIDs = this._featureIDs[layerName];
        this._map.setFilter(
            layerName,
            ["in", ["id"], ["literal", allowedIDs]]
        );
    }
    
    /**
     * Orders layers so that the highlight layer is immediately under the first
     * occuring layer that contains a selected feature.
     */
    #orderLayers () {
        let firstLayer;
        for(var i = 0; i < this._map.getStyle().layers.length; i++) {
            let layer = this._map.getStyle().layers[i];
            if(this._originalLayers.includes(layer["id"])) {
                firstLayer = layer["id"];
                break;
            }
        }

        if(firstLayer != null) {
            this._highlightLayers.forEach(layerName => {
                this._map.moveLayer(layerName, firstLayer);
            });
        }
    }

}
// End of class.