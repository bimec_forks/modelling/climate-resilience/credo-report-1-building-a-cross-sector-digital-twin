/**
 * This class handles creating and configuring new, temporary layers that use filters
 * to provide a focussed "connection-view" mode. 
 * 
 * This class is part of the visualisation implementation and (at the time) of
 * writing, it not intended to be part of the visualisation framework due to
 * the issues listed below.
 * 
 * Current issues with this class:        
 *      - Assumes that all "line" type layers are connections.
 * 
 */
class FocusView {

    // MapBox map.
    _map;

    // Data registry
    _registry;

    // Handles highlighting
    _highlighter;

    // IDs of allowed connections, keyed by layer name.
    _connections = {};

    // IDs of allowed sites, keyed by layer name.
    _sites = {};

    // Max bounds for all in-focus components
    _minLong = 999;
    _maxLong = -999;
    _minLat = 999;
    _maxLat = -999;

    // Pre-focus mode filters for all site layers.
    _siteLayerFilters = {};

    /**
     * Initialise a new FocusView instance.
     * 
     * @param {MapBox Map} map current map instance
     * @param {DataRegistry} registry data registry instance
     */
    constructor(map, registry, highlighter) {
        this._map = map;
        this._registry = registry;
        this._highlighter = highlighter;
    }


    /**
     * Find and show connections to & from the input feature up
     * to the input connection depth.
     * 
     * @param {JSONObject} feature selected site
     * @param {Integer} depth connection depth
     */
    showConnectionsForFeature(feature, depth = 1) {
        // Reset state
        this._connections = {};
        this._sites = {};
        this._previousStates = {};
        this._siteLayerFilters = {};

        // Reset bounds
        this._minLong = 999;
        this._maxLong = -999;
        this._minLat = 999;
        this. _maxLat = -999;
           
        // Remove previously shown stuff
        this.#removeDuplications();

        // Recurse to find connections to show
        this.#recurseConnection(feature, 0, depth);
        if(Object.keys(this._sites).length === 0) {
            this._sites[feature["layer"]["id"]] = [feature["id"]];
        }

        // Create overlay and duplicate connection and site layers
        this.#createOverlay();
        this.#duplicateLayers(this._connections);
        this.#duplicateLayers(this._sites);

        // Update filters to show those connections
        this.#updateFilters(this._map);

        // Highlight the duplicate feature
        if((feature["geometry"]["type"] === "Point") && (feature["properties"]["icon-image"] != null)) {
            this._highlighter.highlightFeature(feature, feature["layer"]["id"] + "-focus");
        }

        // Set the global "focus mode" flag
        if(window.DT == null) window.DT = {};
        DT.focusMode = true;

        // Check if there are any hidden elements
        let hiddenElements = false;
        for (const [key, value] of Object.entries(this._connections)) {
            let visibility = this._map.getLayoutProperty(key, "visibility");
            if(visibility === "none") hiddenElements = true;
        }
        for (const [key, value] of Object.entries(this._sites)) {
            let visibility = this._map.getLayoutProperty(key, "visibility");
            if(visibility === "none") hiddenElements = true;
        }

        // Update warning label
        let hiddenElementContainer = document.getElementById("hiddenElementContainer");
        if(hiddenElements) {
            hiddenElementContainer.style.display = "block";
        } else {
            hiddenElementContainer.style.display = "none";
        }
    }

    /**
     * Ends the connection view mode and returns to default operation.
     */
    endFocusMode() {
        // Remove duplicated layers
        this.#removeDuplications();

        // Remove the highlight
        this._highlighter.remove();

        // Reset bounds
        this._minLong = 999;
        this._maxLong = -999;
        this._minLat = 999;
        this. _maxLat = -999;

        // Clear allowed IDs.
        this._connections = {};
        this._sites = {};
        this._previousStates = {};
        this._siteLayerFilters = {};

        // Update global flag.
        DT.focusMode = false;
    }

    /**
     * Fit the map to the generated bounds
     */
    flyToBounds() {
        let longRange = Math.abs(this._maxLong - this._minLong);
        let latRange = Math.abs(this._maxLat - this._minLat);

        let southWest = [
            (this._minLong - (longRange / 5)),
            (this._minLat - (latRange / 5))
        ];
        let northEast = [
            (this._maxLong + (longRange / 5)),
            (this._maxLat + (latRange / 5))
        ];

        this._map.fitBounds(
            [southWest, northEast],
            { linear: true, pitch: 0, bearing: 0}
        );
    }

    /**
     * Given an MapBox source name, clone it and add it to the map.
     * 
     * @param {String} originalSourceName name of source to clone.
     *  
     * @returns name of cloned source.
     */
    #cloneSource(originalSourceName) {
        let sourceData = this._registry.cachedGeoJSON[originalSourceName];
        let cloneName = originalSourceName + "-focus";

        if(this._map.getSource(cloneName) == null) {
            this._map.addSource(cloneName, {
                "type": "geojson",
                "data": sourceData,
                "generateId": false,
                "cluster": false
            });
            console.log("INFO: Special focus-view source '" + cloneName + "' has been added.");
        }
        return cloneName;
    }

    /**
     * Remove al duplicated layers and sources from the map.
     */
    #removeDuplications() {
        // Remove all focus layers
        let layers = this._map.getStyle().layers;
        for(var i = (layers.length - 1); i >= 0; i--) {
            let layer = layers[i];

            if(layer["id"].endsWith("-focus")) {
                this._map.removeLayer(layer["id"]);
            }
        }

        // Remove all focus sources
        let sources = this._map.getStyle().sources;
        for(var i = (sources.length - 1); i >= 0; i--) {
            let source = sources[i];

            if(source["id"].endsWith("-focus")) {
                this._map.removeSource(source["id"]);
            }
        }
    }

    /**
     * Recusively search for connections and sites linked to the 
     * original source and within the input depth.
     * 
     * @param {JSONObject} currentFeature current site
     * @param {Integer} currentDepth current depth
     * @param {Integer} depthLimit max connection depth
     */
    #recurseConnection(currentFeature, currentDepth, depthLimit) {
        // Gather details of connections from/to the current feature
        let connectionDetails = this.#getConnectionDetails(currentFeature);
        currentDepth = currentDepth + 1;

        // Add discovered connections to global pool
        for (const [connLayer, connIds] of Object.entries(connectionDetails)) {
            if(!this._connections[connLayer]) this._connections[connLayer] = [];

            for(var i = 0; i < connIds.length; i++) {
                if(!this._connections[connLayer].includes(connIds[i])) this._connections[connLayer].push(connIds[i]);

                // Get the GeoJSON feature for the connection
                let connectionFeature = this.#getConnection(connLayer, parseInt(connIds[i]));
                if(connectionFeature == null) continue;
                this.#checkBounds(connectionFeature);

                // Gather details of sites attached to this connection
                let siteDetails = this.#getSiteDetails(connectionFeature);

                // Add discovered sites to global pool
                for (const [siteLayer, siteIds] of Object.entries(siteDetails)) {
                    if(!this._sites[siteLayer]) this._sites[siteLayer] = [];

                    for(var j = 0; j < siteIds.length; j++) {
                        if(!this._sites[siteLayer].includes(siteIds[j])) this._sites[siteLayer].push(siteIds[j]);

                        let siteFeature = this.#getSite(siteLayer, siteIds[j]);
                        if(siteFeature == null) continue;
                        this.#checkBounds(siteFeature);

                        // Recurse if not at limit
                        if(currentDepth < depthLimit) {
                            this.#recurseConnection(siteFeature, currentDepth, depthLimit);
                        }
                    }
                }
            }
        }
    }

    /**
     * Check the location of the feature and adjust the bounds of the focus view as needed.
     * 
     * @param {JSONObject} feature GeoJSON feature. 
     */
    #checkBounds(feature) {
        if(feature == null) {
            return;
        }
        let coords = feature["geometry"]["coordinates"];
        let firstCoord = coords[0];

        if((typeof firstCoord) === "number") {
            if(firstCoord[0] < this._minLong) this._minLong = firstCoord[0];
            if(firstCoord[0] > this._maxLong) this._maxLong = firstCoord[0];
            if(firstCoord[1] < this._minLat) this._minLat = firstCoord[1];
            if(firstCoord[1] > this._maxLat) this._maxLat = firstCoord[1];

        } else {
            for(var i = 0; i < coords.length; i++) {
                let coord = coords[i];
                if(coord[0] < this._minLong) this._minLong = coord[0];
                if(coord[0] > this._maxLong) this._maxLong = coord[0];
                if(coord[1] < this._minLat) this._minLat = coord[1];
                if(coord[1] > this._maxLat) this._maxLat = coord[1];
            }
        }
    }

    /**
     * Update filters of MapBox layers to only show the allowed Connections and Sites.
     */
    #updateFilters() {
        for (const [connLayer, connIds] of Object.entries(this._connections)) {
            if(this._map.getLayer(connLayer + "-focus") != null) {
                let literal = ["literal", connIds];
                let filter = ["in", ["id"], literal];
                this._map.setFilter(connLayer + "-focus", filter);

                if(this._map.getLayer(connLayer + "_arrows-focus") != null) {
                    this._map.setFilter(connLayer + "_arrows-focus", filter);
                }
            }
        }

        for (const [siteLayer, siteIds] of Object.entries(this._sites)) {
            if(this._map.getLayer(siteLayer + "-focus") != null) {
                let literal = ["literal", siteIds];
                let filter = ["in", ["id"], literal];
                this._map.setFilter(siteLayer + "-focus", filter);
            }
        }
    }

    /**
     * Given a site feature, this method parses it and returns a
     * map of the connections to/from it as specified by the metadata.
     * 
     * @param {JSONObject} site site
     * 
     * @returns map of connection ids (keyed by layer name) connected to the site.
     */
    #getConnectionDetails(site) {
        let allowedConnections = {};

        // Get connections as listed in feature properties.
        // Note: a MapBox bug here means that what's set as a JSON array in the original GeoJSON
        // gets returned as a single string, hence the need to re-parse it as JSON.
        // https://github.com/mapbox/mapbox-gl-js/issues/2434
        let connections = site.properties["connections"];
        if(connections == null) return allowedConnections;

        if(typeof connections === "string") {
            connections = JSON.parse(connections);
        }

        for(var i = 0; i < connections.length; i++) {
            let connection = connections[i];
            let layerName = connection.split("/")[0];
            let id = connection.split("/")[1];

            if(!allowedConnections[layerName]) {
                allowedConnections[layerName] = [];
            }
            if(!allowedConnections[layerName].includes(parseInt(id))) {
                allowedConnections[layerName].push(parseInt(id));
            }
        }
        return allowedConnections;
    }

    
    /**
     * Given a connection feature, this method parses it and returns a
     * map of the site connected to it as specified by the metadata.
     * 
     * @param {JSONObject} connection connection
     * 
     * @returns map of connected site ids (keyed by layer name).
     */
    #getSiteDetails(connection) {
        let allowedSites = {};

        // Source of connection
        let sourceSite = connection["properties"]["from"];
        if(sourceSite != null) {
            let sourceSource = sourceSite.split("/")[0];
            let sourceID = parseInt(sourceSite.split("/")[1]);
            if(!allowedSites[sourceSource]) allowedSites[sourceSource] = [];
            if(!allowedSites[sourceSource].includes(sourceID)) allowedSites[sourceSource].push(parseInt(sourceID));
        }

        // Target of connection
        let targetSite = connection["properties"]["to"];
        if(targetSite != null) {
            let targetSource = targetSite.split("/")[0];
            let targetID = parseInt(targetSite.split("/")[1]);
            if(!allowedSites[targetSource]) allowedSites[targetSource] = [];
            if(!allowedSites[targetSource].includes(targetID)) allowedSites[targetSource].push(parseInt(targetID));
        }
        return allowedSites;
    }

    /**
     * Attempts to find the GeoJSON feature representing the input connection
     * ID within the input connection target.
     * 
     * @param {String} connectionSource name of connection source
     * @param {Integer} connectionID id of connection feature
     * 
     * @returns matching connection feature, or null
     */
    #getConnection(connectionSource, connectionID) {
        let sourceData = this._registry.cachedGeoJSON[connectionSource];
        return sourceData["features"].find(feature => {
            return feature["id"] === connectionID;
        });
    }

    /**
     * Attempts to find the GeoJSON feature representing the input site ID.
     * 
     * @param {String} siteSource name of site source
     * @param {Integer} siteID id of site feature
     * 
     * @returns matching site feature, or null
     */
    #getSite(siteSource, siteID) {
        let sourceData = this._registry.cachedGeoJSON[siteSource];
        return sourceData["features"].find(feature => {

            // Store the site's layer's filter
            let filter = this._map.getLayer(siteSource)["filter"];
            this._siteLayerFilters[siteSource] = filter;

            return feature["id"] === siteID;
        });
    }

    /**
     * Create and add the background overlay layer.
     * 
     * @param {String} color optional hex color (defaults to black) 
     */
    #createOverlay(color = "#FFFFFF") {
        if(this._map.getLayer("overlay-focus") == null) {
            this._map.addLayer({
                "id": "overlay-focus",
                "type": "background",
                "metadata": {
                    "provider": "cmcl"
                },
                "layout": {
                    "visibility": "visible"
                },
                "paint": {
                "background-color": color,
                "background-opacity": 0.80
                }
            });
        }
    }

    /**
     * Given a key/value collection of layer names and permitted feature IDs within them,
     * this method duplicates those layers and sets a filter on the new duplication that 
     * will only show features with the permitted IDs.
     * 
     * @param {{String, Integer[]}} allowedIDs layer names and the permitted feature IDs within them
     */
    #duplicateLayers(allowedIDs) {
        for (const [layerName, ids] of Object.entries(allowedIDs)) {
            // Get existing layer
            let existingLayer = this._map.getLayer(layerName);
            if(existingLayer == null) continue;

            let visibility = this._map.getLayoutProperty(layerName, "visibility");
            console.log("Visibility of " + layerName + " is " + visibility);
            if(visibility === "none") {
                continue;
            }

            // Clone that layer with a new ID
            let newID = existingLayer["id"] + "-focus";
            let sourceName = existingLayer["source"];

            if(this._map.getLayer(newID) == null) {

                if(existingLayer["type"] === "symbol") {
                    // Symbol layer, clone the source so that we can turn off clustering.
                    sourceName = this.#cloneSource(existingLayer["source"]);
                }
                let cloneLayer = this.#cloneLayer(newID, existingLayer, sourceName);
                this._map.addLayer(cloneLayer);
                console.log("ADDED " + newID);
            }

            // Check if there's an "_arrows" layer 
            if(this._map.getLayer(layerName + "_arrows") != null) {
                let cloneLayer = this.#cloneLayer(
                    layerName + "_arrows-focus", 
                    this._map.getLayer(layerName + "_arrows")
                );
                this._map.addLayer(cloneLayer);
            }
            // Filter so that only features with IDs within allowedIDs are shown
            let literal = ["literal", ids];
            let filter = ["in", ["id"], literal];
            this._map.setFilter(newID, filter);
        }
    }

    /**
     * Given an existing MapBox layer object, this creates a JSON object that
     * will can be used to create a new, nearly-identical layer.
     * 
     * @param {MapBox Layer} existingLayer 
     * @returns cloned layer JSON
     */
    #cloneLayer(newID, existingLayer, sourceName = null) {
        if(sourceName == null) {
            sourceName = existingLayer["source"];
        }

        // Clone the existing paint properties. This is dodgy as MapBox gives no way
        // to retrieve these other than accessing a private variable. In future, we 
        // should cache the settings used to create layers then we can clone those.
        let paint = {};
        for (var [key, value] of Object.entries(existingLayer.paint._values)) {
            try {
                let property = this._map.getPaintProperty(existingLayer.id, key);
                if(property != null) paint[key] = property
            } catch(error) {
                // Ignore and move on to next property
            }
        }

        // Clone the existing layout properties. This is dodgy as MapBox gives no way
        // to retrieve these other than accessing a private variable. In future, we 
        // should cache the settings used to create layers then we can clone those.
        let layout = {};
        for (var [key, value] of Object.entries(existingLayer.layout._values)) {
            try {
                let property = this._map.getLayoutProperty(existingLayer.id, key);
                if(property != null) layout[key] = property;
            } catch(error) {
                // Ignore and move on to next property
            }
        }

        // Required properties
        let newLayer = {
            "id": newID,
            "type": existingLayer["type"],
            "source": sourceName
        };
        if(Object.keys(layout).length != 0) newLayer["layout"] = layout;
        if(Object.keys(paint).length != 0) newLayer["paint"] = paint;

        // Properties that may have been set.
        if(existingLayer["filter"] != null) newLayer["filter"] = existingLayer["filter"];
        if(existingLayer["metadata"] != null) newLayer["metadata"] = existingLayer["metadata"];
        if(existingLayer["sourceLayer"] != null) newLayer["sourceLayer"] = existingLayer["sourceLayer"];
        if(existingLayer["visibility"] != null) newLayer["visibility"] = existingLayer["visibility"];
        return newLayer;
    }
    
}
// End of class.