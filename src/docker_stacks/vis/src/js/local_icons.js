
// CReDo specific map icons.
const EXAMPLE_ICONS = [
    "img/asset_icons/aw-clean-water.png",
	"img/asset_icons/aw-clean-water-error.png",
	"img/asset_icons/aw-sewage.png",
	"img/asset_icons/aw-sewage-error.png",
	"img/asset_icons/aw-sludge.png",
	"img/asset_icons/aw-sludge-error.png",
	"img/asset_icons/aw-mast.png",
	"img/asset_icons/aw-mast-error.png",
	"img/asset_icons/aw-generic.png",
	"img/asset_icons/aw-generic-error.png",
	"img/asset_icons/aw-empty.png",
	"img/asset_icons/aw-waste-empty.png",

	"img/asset_icons/bt-cabinet.png",
	"img/asset_icons/bt-cabinet-error.png",
	"img/asset_icons/bt-exchange.png",
	"img/asset_icons/bt-exchange-error.png",
	"img/asset_icons/bt-mast.png",
	"img/asset_icons/bt-mast-error.png",
	"img/asset_icons/bt-generic.png",
	"img/asset_icons/bt-generic-error.png",
	"img/asset_icons/bt-empty.png",

	"img/asset_icons/ukpn-primary.png",
	"img/asset_icons/ukpn-primary-error.png",
	"img/asset_icons/ukpn-secondary.png",
	"img/asset_icons/ukpn-secondary-error.png",
	"img/asset_icons/ukpn-generic.png",
	"img/asset_icons/ukpn-generic-error.png",
	"img/asset_icons/ukpn-empty.png",

	"img/asset_icons/ea-flow.png",
	"img/asset_icons/ea-flow-error.png",
	"img/asset_icons/ea-rainfall.png",
	"img/asset_icons/ea-rainfall-error.png",
	"img/asset_icons/ea-temperature.png",
	"img/asset_icons/ea-temperature-error.png",
	"img/asset_icons/ea-water-level.png",
	"img/asset_icons/ea-water-level-error.png",
	"img/asset_icons/ea-wind.png",
	"img/asset_icons/ea-wind-error.png",
	"img/asset_icons/ea-empty.png",

	"img/asset_icons/error.png",
	"img/asset_icons/error-empty.png",
	"img/asset_icons/question.png",
	"img/arrow-sdf.png"
];
