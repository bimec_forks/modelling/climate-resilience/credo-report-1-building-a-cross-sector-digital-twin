# CReDo Visualisation

This directory contains the source code and resources used to build the interactive CReDo visualisation.

## About

This visualisation is a collection of HTML, CSS, and JavaScript files used to display and add data to an interactive map provided by the [MapBox GL JS](https://www.mapbox.com/mapbox-gljs) library. These files are then made available in a standard web browser via the use of a simple [Apache web server](https://httpd.apache.org/).

A generic visualisation framework supplied by CMCL Innovations detects and loads local [JSON](https://www.json.org/json-en.html) files into memory before being adding them to the Map element as Sources. Visual Layers are then added to the map to display the underlying data and add user interaction. Scripts specific to the CReDo visualisation then add additional features such as site highlighting, failure indication, an a detailed connection view.

Please note that the information detailed in this README targets a local build/execution rather than integration with the DAFNI platform. For more information please read the internal CReDo report.


## Prerequisites 

Before the visualisation is built, the following conditions should be met:

* A valid internet connection is required.
    * Without this, the remote JavaScript libraries cannot be loaded and the visualisation will not function.
* A valid MapBox API key must be set within the `index.html` file.
    * CMCL Innovations has already set a MapBox API key specifically for the CReDo project. This key is valid for up to 50,000 visits per month; if this limit is reached, a new (premium) key must be generated and set in the aforementioned file.
* A valid installation of [Docker](https://www.docker.com/).
    * The visualisation has been constructed to run within an easily-buildable Docker container, as such a Docker installation (native or via other tools, such as [Podman](https://podman.io/)) is required.
* Execute the `generate_env_file.sh` script to generate the `.env` file.
    * Before any of the CMCL Innovations containers can be run, the `generate_env_file.sh` script must be executed. This produces a `.env` file that is then used by further scripts to determine which ports should be used.


## Build the visualisation 

The visualisation has been designed so that it can easily be built using `build.sh` script found within the `src/docker_stacks` directory. To build the visualisation container in isolation (based on the currently checked-out commit of this repository), please run the following command.

`./build.sh vis credo-vis`

Alternatively, the entire `vis` stack (which contains the CReDo visualisation) can be built with the following command.

`./build.sh vis`

For more information on this script, and how the Docker stacks are constructed, please see the internal CReDo report.


## Run the visualisation

Following the build process, the execution process has also been designed so that containers can be started using the `start.sh` script (or stopped using the `stop.sh` script). To run the visualisation container in isolation, please run the following command.

`./start.sh vis credo-vis`

Alternatively, the entire `vis` stack (which contains the CReDo visualisation) can be started with the following command.

`./start.sh vis`

For more information on this script, and how the Docker stacks are constructed, please see the internal CReDo report.


## Adding data

Before the visualisation can be used, data must be added to it. The generic framework at the core of the visualisation expects data to be in a standard format (details on this can be read [here](https://github.com/cambridge-cares/TheWorldAvatar/wiki/DTVF:-Data-Structure)). Within the CReDo project, a separate `kg-data-extractor` has been created to produce this data; see it's README for more details.

Once a directory of valid data has been created, it needs to be accessible to the visualisation. This requires the following steps:

* Copy the directory into the `credo-vis` container.
    * This can be done using the Docker (or Podman) `cp` command.
* Add the directory to the `rootDirectories` variable within the `index.html` file.
    * As the visualisation supports multiple directories of data, each needs to be added to the aforementioned variable using a human-readable name, and relative file location.

The `credo-vis` container has been created to use a [Docker volume](https://docs.docker.com/storage/volumes/) mounted at to the `/var/www/html/data` directory within the container. This means that files or directories added to that directory will still be present if the container itself is restarted or rebuilt.


## Viewing the visualisation

Once the visualisation is running the following steps should be followed to view it.

* Connect to the CReDO VPN.
* Create an SSH tunnel for the port the `credo-vis` container is running on.
* Create an SSH tunnel for the port the `geoserver` container is running on.
    * This container has also be started as part of the `vis` stack.
* Open a web browser to the `localhost:PORT` address, using the `credo-vis` tunnel port.


