#!/bin/sh
stack=$1
shift
podman-compose -f docker-compose.${stack}.yml down $@ && podman-compose --podman-build-args="--security-opt label=disable" -t identity -f docker-compose.${stack}.yml up -d $@
