package com.cmclinnovations.credo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import uk.ac.cam.cares.jps.base.exception.JPSRuntimeException;

public class FloodRDBClient {
    // URL and credentials for the relational database
    private String rdbURL = null;
    private String rdbUser = null;
    private String rdbPassword = null;
    // Constants
    private static final SQLDialect dialect = SQLDialect.POSTGRES;
    // RDB connection properties and jooq configuration
    private Connection conn = null;
    private DSLContext context;
    // Exception prefix
    private final String exceptionPrefix = this.getClass().getSimpleName() + ": ";

    public FloodRDBClient(String rdbURL, String rdbUser, String rdbPassword) {
        this.rdbURL = rdbURL;
        this.rdbUser = rdbUser;
        this.rdbPassword = rdbPassword;
        connect();
    }

    public DSLContext getContext() {
        return context;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public void setContext(DSLContext context) {
        this.context = context;
    }

    List<Integer> getTimesteps() {
        Field<String> fileColumn = DSL.field(DSL.name("filename"), String.class);
        DSL.table(DSL.name("flood_data"));

        List<String> filenames = context.select(fileColumn).from(DSL.table(DSL.name("flood_data"))).fetch(fileColumn);

        List<Integer> timesteps = new ArrayList<>();
        for (String filename : filenames) {
            String[] processedString = filename.split(".tif");
            timesteps.add(Integer.parseInt(processedString[0].substring(2)));
        }
        return timesteps;
    }

    Pair<List<Integer>, List<Double>> getFloodDepth(String easting, String northing) {

        String SQL_query = "SELECT ST_Value(rast, 1, po1) as FloodDepth, REPLACE(REPLACE(filename,'h_',''), '.tif','') as Time \n"
                + "FROM flood_data CROSS JOIN ST_SetSRID(ST_Point(" + easting + "," + northing + "),27700) as po1 \n"
                + "WHERE ST_Value(rast, 1, po1) IS NOT NULL";

        List<Integer> times = new ArrayList<>();
        List<Double> depths = new ArrayList<>();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(SQL_query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                times.add(rs.getInt("Time"));
                depths.add(rs.getDouble("FloodDepth"));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("No results.");
        }
        return Pair.of(times, depths);
    }

    /**
     * Initialise RDB table for particular time series and add respective entries to
     * central lookup table
     * <p>
     * For the list of supported classes, refer org.jooq.impl.SQLDataType
     * <p>
     * The timeseries IRI needs to be provided. A unique uuid for the corresponding
     * table will be generated.
     * 
     * @param dataIRI   list of IRIs for the data provided as string
     * @param dataClass list with the corresponding Java class (typical String,
     *                  double or int) for each data IRI
     * @param tsIRI     IRI of the timeseries provided as string
     */
    protected void initFloodTable(List<String> dataIRI, List<Class<?>> dataClass, String tsIRI) {
        // Generate UUID as unique RDB table name
        // String tsTableName = UUID.randomUUID().toString();
        // Initialise connection and set jOOQ DSL context
        connect();
        disconnect();
        System.out.println("Disconnecting successful");
    }

    /**
     * Establish connection to RDB and set DSL context
     */
    private void connect() {
        try {
            if (this.conn == null || this.conn.isClosed()) {
                // Load required driver
                Class.forName("org.postgresql.Driver");
                // Connect to DB (using static connection and context properties)
                this.conn = DriverManager.getConnection(this.rdbURL, this.rdbUser, this.rdbPassword);
                this.context = DSL.using(this.conn, dialect);
                System.out.println("Connecting successful: " + this.rdbURL);
            }
        } catch (Exception e) {
            System.out.println("Connecting failed: " + this.rdbURL);
            throw new JPSRuntimeException(exceptionPrefix + "Establishing database connection failed");
        }
    }

    /**
     * Close existing connection to RDB
     */
    void disconnect() {
        try {
            conn.close();
            System.out.println("Disconnecting successful");
        } catch (Exception e) {
            System.out.println("Disconnecting failed");
            throw new JPSRuntimeException(exceptionPrefix + "Closing database connection failed");
        }
    }
}
