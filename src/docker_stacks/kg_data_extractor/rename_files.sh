#!/bin/sh

mv "$1" "$(echo "$1" | sed -e 's/Anglian Water/Water Network/g' -e 's/BT Group/Communications Network/g' -e 's/UK Power Networks/Power Network/g')"