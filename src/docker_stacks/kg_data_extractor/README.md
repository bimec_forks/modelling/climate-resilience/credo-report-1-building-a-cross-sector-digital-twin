# KG data extractor
## Required environment variables
See the model definition (model-def.yml) for a description of the environment variables that must be set to use this code.

## Optional environment variables
- RIVER_DATA_DIR: absolute path to the directory containing river sensor data. (extracted using https://github.com/cambridge-cares/TheWorldAvatar/tree/develop/Agents/FloodAgent)
- BUILDINGS_DATA_DIR: absolute path to the directory containing buildings data. (extracted using https://github.com/cambridge-cares/TheWorldAvatar/tree/dev-dockerise-kingslynnagent/Agents/KingsLynnAgent)