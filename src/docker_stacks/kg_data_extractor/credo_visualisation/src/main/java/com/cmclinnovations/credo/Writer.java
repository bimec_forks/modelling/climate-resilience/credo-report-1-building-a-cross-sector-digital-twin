package com.cmclinnovations.credo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.cmclinnovations.credo.objects.Asset;
import com.cmclinnovations.credo.objects.AssetList;
import com.cmclinnovations.credo.objects.AssetList.RefStateComparator;
import com.cmclinnovations.credo.objects.Connection;
import com.cmclinnovations.credo.objects.Connection.ConnectionType;
import com.cmclinnovations.credo.objects.ConnectionList;
import com.opencsv.CSVReader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;

import uk.ac.cam.cares.jps.base.exception.JPSRuntimeException;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeries;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeriesClient;

public class Writer {
	private static final String README_MD = "AdditionalFilesReadme.md";
	private static final String MISSING_DATA_JSON = "missingData.json";
	private static final String FAILURE_SUMMARY_CSV = "failureSummary.csv";
	private static final String FAILURE_CSV_SUFFIX = "_failures.csv";

	private static final String IN_AREA_SUFFIX = "InArea";

	private static final Logger LOGGER = LogManager.getLogger(Writer.class);

	private Path outputDir;
	private List<Integer> vis_timesteps;
	private AssetList assetList;
	private ConnectionList connList;
	private JSONArray areaOfInterestPolys;
	private boolean onlySingleTimestep;
	private String riverDataFolder;
	private String buildingsDataFolder;
	private FloodDataProcessor floodDataProcessor;

	// properties in the overall meta file
	private double[] mapcentre = { 0.4, 52.676 };
	private String scenarioLabel;
	private String scenarioDir;

	private String hipims_code; // code for hipims simulation

	private static String display_order = "display_order"; // key for the display order field
	private static String collapse = "collapse"; // true = collapse, everything is expanded by default

	private static final String AOI_LAYER_NAME = "Area of interest";

	static Map<ConnectionType, String> credoColours = new HashMap<ConnectionType, String>() {
		{
			put(ConnectionType.suppliesPowerTo, "rgb(252,194,3)");
			put(ConnectionType.suppliesSewageTo, "#a56f3a");
			put(ConnectionType.suppliesPhoneTo, "#a55ce5");
			put(ConnectionType.suppliesWaterTo, "rgb(99,184,232)");
		}
	};

	static Map<String, String> standard_icons = new HashMap<String, String>() {
		{
			put("Water Site", "aw-clean-water");
			put("Sludge Site", "aw-sludge");
			put("Sewage Site", "aw-sewage");
			put("Exchange", "bt-exchange");
			put("Mobile mast", "bt-mast");
			put("Cabinet", "bt-cabinet");
			put("Primary sub-station", "ukpn-primary");
			put("Secondary sub-station", "ukpn-secondary");
		}
	};

	static Map<String, String> error_icons = new HashMap<String, String>() {
		{
			put("Water Site", "aw-clean-water-error");
			put("Sludge Site", "aw-sludge-error");
			put("Sewage Site", "aw-sewage-error");
			put("Exchange", "bt-exchange-error");
			put("Mobile mast", "bt-mast-error");
			put("Cabinet", "bt-cabinet-error");
			put("Primary sub-station", "ukpn-primary-error");
			put("Secondary sub-station", "ukpn-secondary-error");
		}
	};

	static Map<String, String> empty_icons = new HashMap<String, String>() {
		{
			put("Water Site", "aw-empty");
			put("Sludge Site", "aw-waste-empty");
			put("Sewage Site", "aw-waste-empty");
			put("Exchange", "bt-empty");
			put("Mobile mast", "bt-empty");
			put("Cabinet", "bt-empty");
			put("Primary sub-station", "ukpn-empty");
			put("Secondary sub-station", "ukpn-empty");
		}
	};

	static Map<String, String> textColors = new HashMap<String, String>() {
		{
			put("Water Site", "#63b8e8");
			put("Sludge Site", "#a56f3a");
			put("Sewage Site", "#a56f3a");
			put("Exchange", "#a55ce5");
			put("Mobile mast", "#a55ce5");
			put("Cabinet", "#a55ce5");
			put("Primary sub-station", "#f9d500");
			put("Secondary sub-station", "#f9d500");
		}
	};

	// colours to loop through
	List<String> pointcolours = Arrays.asList("rgb(204,41,41)", "rgb(41,150,204)", "rgb(95,204,41)", "rgb(217,186,33)",
			"rgb(204,41,204)", "rgb(102,20,20)", "rgb(20,75,102)", "rgb(40,102,20)", "rgb(128,109,19)",
			"rgb(102,20,102)");

	List<String> linecolours = Arrays.asList("rgb(102,20,102)", "rgb(128,109,19)", "rgb(40,102,20)", "rgb(20,75,102)",
			"rgb(102,20,20)", "rgb(204,41,204)", "rgb(217,186,33)", "rgb(95,204,41)", "rgb(41,150,204)",
			"rgb(204,41,41)");

	// HiPIMS simulation code to user facing texts
	private Map<String, String> floodCodeToLabelMap;
	private Map<String, String> floodCodeToDescriptionMap;
	
	public Writer(Path outputDir, List<Integer> vis_timesteps, AssetList assetList, ConnectionList connList,
			JSONArray areaOfInterestPolys, boolean onlySingleTimestep, String riverDataFolder,
			String buildingsDataFolder, FloodDataProcessor floodDataProcessor) {
		this.outputDir = outputDir;
		this.vis_timesteps = vis_timesteps;
		this.assetList = assetList;
		this.connList = connList;
		this.areaOfInterestPolys = areaOfInterestPolys;
		this.onlySingleTimestep = onlySingleTimestep;
		this.riverDataFolder = riverDataFolder;
		this.buildingsDataFolder = buildingsDataFolder;
		this.floodDataProcessor = floodDataProcessor;

		// this happens when SKIP_RIVER is set to true in the Argo workflow
		if (this.riverDataFolder != null) {
			Path riverDataPath = Paths.get(this.riverDataFolder);
			if (!(Files.exists(riverDataPath) && riverDataPath.toFile().listFiles().length > 0)) {
				this.riverDataFolder = null;
				LOGGER.info("Skipping river data because folder is either empty or does not exist");
			}
		}

		// this happens when SKIP_BUILDING is set to true in the Argo workflow
		if (this.buildingsDataFolder != null) {
			Path buildingsDataPath = Paths.get(this.buildingsDataFolder);
			if (!(Files.exists(buildingsDataPath) && buildingsDataPath.toFile().listFiles().length > 0)) {
				this.buildingsDataFolder = null;
				LOGGER.info("Skipping building data because folder is either empty or does not exist");
			}
		}

		// read file that defines texts for each flood scenario
		floodCodeToLabelMap = new HashMap<>();
		floodCodeToDescriptionMap = new HashMap<>();
		try {
			Reader reader = Files.newBufferedReader(Paths.get(Config.FLOOD_LABEL_FILE));
			CSVReader csvReader = new CSVReader(reader);
			List<String[]> readAll = csvReader.readAll();

			// skip first line (header)
			for (int i = 1; i < readAll.size(); i++) {
				floodCodeToLabelMap.put(readAll.get(i)[0], readAll.get(i)[1]);
				floodCodeToDescriptionMap.put(readAll.get(i)[0], readAll.get(i)[2]);
			}

			csvReader.close();
		} catch (IOException e1) {
			LOGGER.error("Error reading flood labels");
			LOGGER.error(e1.getMessage());
		}

		// set scenario folder name and label based on flood scenario
		// default values
		scenarioDir = "scenario-0";
		scenarioLabel = "Scenario #0";
		if (Files.exists(Config.GEOTIF_IN_DIR)) {
			// hard coded for CReDO workflow, not meant to be robust
			// if the directory name is flood_data, look 1 level below for the flood code
			Path currentDir = Config.GEOTIF_IN_DIR;
			hipims_code = "";
			while (hipims_code.isBlank()) {
				try {
					if (currentDir.getFileName().toString().contains("hipims")) {
						hipims_code = currentDir.getFileName().toString();
					} else {
						currentDir = Files.list(currentDir).filter(p -> p.toFile().isDirectory()).findAny().get();
					}
				} catch (IOException e) {
					LOGGER.error("Failed to list some directories in GEOTIF_DIR");
					LOGGER.warn(e.getMessage());
				}
			}

			if (!hipims_code.isBlank()) {
				scenarioDir = hipims_code + "-scenario";
				if (floodCodeToLabelMap.containsKey(hipims_code)) {
					scenarioLabel = floodCodeToLabelMap.get(hipims_code);
				}
			}
		} else {
			LOGGER.warn(Config.GEOTIF_IN_DIR + " does not exist");
		}
	}

	public void removeOldOutput() {
		try {
			if (Files.exists(outputDir)) {
				FileUtils.cleanDirectory(outputDir.toFile());
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed to delete old output directory '" + outputDir + "'.", e);
		}
	}

	void makeDirectories() {
		if (this.onlySingleTimestep) {
			outputDir.toFile().mkdirs();
		} else {
			Path scenarioDirPath = outputDir.resolve(scenarioDir);
			for (Integer i : this.vis_timesteps) {
				scenarioDirPath.resolve(i.toString()).toFile().mkdirs();
			}
		}
	}

	void writeOverallMeta() {

		Path file = outputDir.resolve("meta.json");

		JSONObject json = new JSONObject();

		JSONObject global = new JSONObject();
		global.put("defaultCenter", mapcentre);
		global.put("defaultZoom", 8.75);
		global.put("defaultBearing", 0.0);
		global.put("defaultPitch", 0.0);

		JSONObject local = new JSONObject();
		local.put("label", "Scenario");
		JSONArray groups = new JSONArray();
		JSONObject group1 = new JSONObject();
		group1.put("name", scenarioLabel);
		group1.put("directory", scenarioDir);
		groups.put(group1);
		local.put("groups", groups);

		json.put("global", global);
		json.put("local", local);

		// Additional files
		JSONArray linkedFiles = new JSONArray();

		// Readme
		try {
			Files.copy(getClass().getResourceAsStream("/" + README_MD), outputDir.resolve(README_MD));
			JSONObject readmeFileEntry = new JSONObject();
			readmeFileEntry.put("text", "Additional Files Readme");
			readmeFileEntry.put("url", README_MD);
			linkedFiles.put(readmeFileEntry);
		} catch (IOException ex) {
			throw new RuntimeException("Failed to extract file '" + README_MD + "'", ex);
		}

		// For failure summary
		JSONObject failureSummaryFileEntry = new JSONObject();
		failureSummaryFileEntry.put("text", scenarioLabel + ": Failure summary file");
		failureSummaryFileEntry.put("url", scenarioDir + "/" + FAILURE_SUMMARY_CSV);
		linkedFiles.put(failureSummaryFileEntry);

		for (String owner : assetList.getSiteOwners()) {
			JSONObject siteFailureSummaryFileEntry = new JSONObject();
			siteFailureSummaryFileEntry.put("text", scenarioLabel + ": " + owner + " site failures file");
			siteFailureSummaryFileEntry.put("url", scenarioDir + "/" + owner + FAILURE_CSV_SUFFIX);
			linkedFiles.put(siteFailureSummaryFileEntry);
		}

		// Missing data
		JSONObject missingDataFileEntry = new JSONObject();
		missingDataFileEntry.put("text", scenarioLabel + ": Missing data file");
		missingDataFileEntry.put("url", scenarioDir + "/" + MISSING_DATA_JSON);
		linkedFiles.put(missingDataFileEntry);

		global.put("linkedFiles", linkedFiles);

		// write to file
		writeToFile(file, json);
	}

	void writeLayerTree() {

		Path file = outputDir.resolve("tree.json");

		List<String> owners = this.assetList.getSiteOwners();

		JSONArray allLayer = new JSONArray();

		if (!areaOfInterestPolys.isEmpty()) {
			allLayer.put(createLayer(AOI_LAYER_NAME));
		}

		allLayer.put(createLayer(FloodDataProcessor.FLOOD_LAYER_DISPLAY_NAME));

		for (String owner : owners) {
			List<String> siteTypes = this.assetList.getSiteTypes(owner);
			JSONObject layers = createLayerGroup(owner, siteTypes);
			allLayer.put(layers);
		}

		// connection type layers
		List<String> connNames = this.connList.getConnectionLabels();
		if (!connNames.isEmpty()) {
			JSONObject connectionsJson = createLayerGroup("Connections", connNames);
			allLayer.put(connectionsJson);
		}

		// river sensor
		if (this.riverDataFolder != null) {
			File riverTreeFile = Paths.get(this.riverDataFolder, "tree.json").toFile();
			try {
				String riverTree = FileUtils.readFileToString(riverTreeFile, "UTF-8");
				JSONArray riverTreeJSON = new JSONArray(riverTree);
				allLayer.put(riverTreeJSON.getJSONObject(0));
			} catch (Exception e) {
				LOGGER.error("Error in reading tree file for river sensor data");
				LOGGER.error(e.getMessage());
			}
		}

		// buildings
		if (this.buildingsDataFolder != null) {
			File buildingsTreeFile = Paths.get(this.buildingsDataFolder, "tree.json").toFile();
			try {
				String buildingsTree = FileUtils.readFileToString(buildingsTreeFile, "UTF-8");
				JSONArray buildingsTreeJson = new JSONArray(buildingsTree);
				allLayer.put(buildingsTreeJson.getJSONObject(0));
			} catch (Exception e) {
				LOGGER.error("Error in reading tree file for buildings data");
				LOGGER.error(e.getMessage());
			}
		}

		// write to file
		writeToFile(file, allLayer);
	}

	private JSONObject createLayer(String layername) {
		JSONObject layer = new JSONObject();
		layer.put("layerName", layername);
		layer.put("defaultState", "visible");
		layer.put("layerIDs", new JSONArray().put(layername));
		return layer;
	}

	private JSONObject createLayerGroup(String groupname, List<String> layernames) {
		JSONObject group = new JSONObject();
		group.put("groupName", groupname);

		JSONArray layers = new JSONArray();

		for (String layername : layernames) {
			layers.put(createLayer(layername));
		}

		group.put("layers", layers);

		return group;
	}

	void writeOverallScenarioMeta() {

		Path file = outputDir.resolve(scenarioDir).resolve("meta.json");

		// overall json object
		JSONObject json = new JSONObject();
		json.put("label", "Timestep");

		JSONArray groups = new JSONArray();
		for (Integer i : this.vis_timesteps) {
			JSONObject group = new JSONObject();
			group.put("name", i.toString());
			group.put("directory", i.toString());
			groups.put(group);
		}

		json.put("groups", groups);

		writeToFile(file, json);
	}

	void writeIndividualScenarioMeta() {

		JSONObject json = new JSONObject();

		JSONArray dataSets = new JSONArray();

		if (!areaOfInterestPolys.isEmpty()) {
			JSONObject dataSet = new JSONObject();
			dataSet.put("name", AOI_LAYER_NAME);
			dataSet.put("dataLocation", "areaOfInterest.geojson");
			dataSet.put("locationType", "polygon");
			dataSet.put("dataType", "geojson");
			dataSet.put("clickable", false);
			dataSets.put(dataSet);
		}

		List<String> owners = this.assetList.getSiteOwners();
		for (String owner : owners) {
			List<String> siteTypes = this.assetList.getSiteTypes(owner);
			for (String siteType : siteTypes) {
				JSONObject dataSet = new JSONObject();
				dataSet.put("name", siteType);
				dataSet.put("dataLocation", siteType + "locations.geojson");
				dataSet.put("dataType", "geojson");

				// clustering properties
				dataSet.put("cluster", true);
				dataSet.put("clusterRadius", 30);

				// preparing cluster properties
				JSONObject clusterProperties = new JSONObject();

				JSONArray icon_image = new JSONArray();
				icon_image.put("string").put(empty_icons.get(siteType));

				JSONArray text_colour = new JSONArray();
				text_colour.put("string").put(textColors.get(siteType));

				JSONArray floodCount = new JSONArray();
				floodCount.put("+");
				floodCount.put(new JSONArray().put("get").put("floodCount").put(new JSONArray().put("properties")));

				clusterProperties.put("icon-image", icon_image);
				clusterProperties.put("text-color", text_colour);
				clusterProperties.put("floodCount", floodCount);

				dataSet.put("clusterProperties", clusterProperties);

				if (standard_icons.containsKey(siteType)) {
					dataSet.put("locationType", "symbol");
				} else {
					dataSet.put("locationType", "point");
				}

				dataSet.put("metaFiles", new JSONArray().put(siteType + "meta.json"));
				dataSet.put("timeseriesFiles", new JSONArray().put(siteType + "timeseries.json"));

				dataSets.put(dataSet);
			}
		}

		List<String> connectionNames = this.connList.getConnectionLabels();
		if (!connectionNames.isEmpty()) {
			for (String connectionName : connectionNames) {
				JSONObject dataSet = new JSONObject();
				dataSet.put("name", connectionName);
				dataSet.put("dataLocation", connectionName + ".geojson");
				dataSet.put("locationType", "line");
				dataSet.put("dataType", "geojson");
				dataSet.put("minzoom", 12);
				dataSet.put("metaFiles", new JSONArray().put(connectionName + "meta.json"));
				dataSet.put("arrows", true);

				dataSets.put(dataSet);
			}
		}

		// river and buildings data
		List<String> river_and_buildings_folder = new ArrayList<>();
		if (this.riverDataFolder != null) {
			river_and_buildings_folder.add(this.riverDataFolder);
		}
		if (this.buildingsDataFolder != null) {
			river_and_buildings_folder.add(this.buildingsDataFolder);
		}
		for (String folder : river_and_buildings_folder) {
			try {
				File overallMetaFile = Paths.get(folder, "meta.json").toFile();
				JSONObject overallMeta = new JSONObject(FileUtils.readFileToString(overallMetaFile, "UTF-8"));
				String directoryToRead = overallMeta.getJSONObject("local").getJSONArray("groups").getJSONObject(0)
						.getString("directory");

				File dataMetaFile = Paths.get(folder, directoryToRead, "meta.json").toFile();
				JSONObject dataMeta = new JSONObject(FileUtils.readFileToString(dataMetaFile, "UTF-8"));
				JSONArray sets = dataMeta.getJSONArray("dataSets");
				for (int i = 0; i < sets.length(); i++) {
					dataSets.put(sets.getJSONObject(i));
				}
			} catch (Exception e) {
				LOGGER.error("Error in copying data in " + folder);
				LOGGER.error(e.getMessage());
			}
		}

		json.put("dataSets", dataSets);

		// write to file
		Integer previousTime = 0;
		for (Integer time : this.vis_timesteps) {
			JSONObject scenarioMetaJson = new JSONObject(json.toString());
			JSONArray scenarioDataSets = scenarioMetaJson.getJSONArray("dataSets");

			for (String layerName : floodDataProcessor.getLayernames()) {
				scenarioDataSets.put(floodDataProcessor.getFloodLayerNode(layerName, previousTime, time));
			}

			Path file = outputDir.resolve(scenarioDir).resolve(time.toString()).resolve("meta.json");
			writeToFile(file, scenarioMetaJson);

			previousTime = time + 1;
		}
	}

	void writeAssetsMeta() {
		List<String> owners = this.assetList.getSiteOwners();

		for (String owner : owners) {
			for (String siteType : this.assetList.getSiteTypes(owner)) {
				if (this.onlySingleTimestep) {
					JSONArray metaDataCollection = new JSONArray();
					int i = this.vis_timesteps.get(this.vis_timesteps.size() - 1);
					Path file = outputDir.resolve(siteType + "meta.json");
					for (Asset site : assetList.getSites(owner, siteType)) {
						if (!site.getLocation().isEmpty()) {
							JSONObject metaData = new JSONObject();
							getAssetMetaData(site, metaData, i);
							metaData.put("id", site.getVisId());
							metaDataCollection.put(metaData);
						}
					}
					writeToFile(file, metaDataCollection);
				} else {
					for (Integer i : this.vis_timesteps) {
						JSONArray metaDataCollection = new JSONArray();
						Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
								.resolve(siteType + "meta.json");
						for (Asset site : assetList.getSites(owner, siteType)) {
							if (!site.getLocation().isEmpty()) {
								JSONObject metaData = new JSONObject();
								getAssetMetaData(site, metaData, i);
								metaData.put("id", site.getVisId());
								metaDataCollection.put(metaData);
							}
						}
						writeToFile(file, metaDataCollection);
					}
				}
			}
		}
	}

	void writeFailureSummaryFile(QueryClient queryClient) {

		Map<String, String> stateMap = queryClient.getLabelToStateIRIMap().entrySet().stream()
				.collect(Collectors.toMap(Entry::getValue, Entry::getKey, (entry1, entry2) -> entry1));

		List<String> stateLabelsInOrder = new ArrayList<>();
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#FloodState"));
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#PowerState"));
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontobt.owl#TelephoneState"));
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#WaterState"));
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#SewageState"));
		stateLabelsInOrder.add(stateMap.get("http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#SludgeState"));

		String totalColumnName = "Total number of assets in digital twin";
		String totalInAreaColumnName = "Total number of assets in area of interest";
		String failureFreeColumnName = "Failure free";
		String totalFailureColumnName = "Number of failed assets in digital twin";
		String totalFailureInAreaColumnName = "Number of failed assets in area of interest";

		String total = "total";
		String totalFailed = "totalFailed";
		String noFail = "noFail";

		List<String> sitesInAOE = queryClient.getSitesInAOE();
		if (null == sitesInAOE) {
			sitesInAOE = assetList.getAllSites().stream().map(Asset::getIri).collect(Collectors.toList());
		}

		String assetOwnerColumnName = "Asset owner";
		String siteTypeColumnName = "Type of asset";
		String siteNameColumnName = "Name";
		String siteIDColumnName = "ID";
		String siteCritColumnName = "Criticality";
		String siteEastingColumnName = "Easting";
		String siteNortingColumnName = "Northing";

		List<String> siteFailureColumnHeadings = new ArrayList<>(List.of(assetOwnerColumnName, siteTypeColumnName,
				siteNameColumnName, siteIDColumnName, siteCritColumnName, siteEastingColumnName,
				siteNortingColumnName));
		siteFailureColumnHeadings.addAll(stateLabelsInOrder);

		Map<String, Map<String, Map<String, Integer>>> summaryCount = new HashMap<>();
		Map<String, Map<String, Map<String, Double>>> summaryCriticalities = new HashMap<>();
		for (String owner : assetList.getSiteOwners()) {
			Map<String, Map<String, Integer>> ownerCount = new HashMap<>();
			Map<String, Map<String, Double>> ownerCriticalities = new HashMap<>();
			List<Map<String, String>> siteOwnerSummary = new ArrayList<>();
			for (String siteType : assetList.getSiteTypes(owner)) {
				Map<String, Integer> siteTypeCount = new HashMap<>();
				Map<String, Double> siteTypeCriticalities = new HashMap<>();
				for (Asset site : assetList.getSites(owner, siteType)) {
					if (!site.getLocation().isEmpty() && site.hasStates()) {
						TimeSeries<Integer> statesTs = site.getStateTimeSeries();
						List<String> dataIRIs = statesTs.getDataIRIs();
						Map<String, Long> failureCount = new HashMap<>(dataIRIs.size());
						int nRows = statesTs.getTimes().size();
						Map<String, Boolean> refStateValues = new HashMap<>();
						for (String dataIRI : dataIRIs) {
							String label = assetList.getStateLabel(dataIRI);
							failureCount.put(label, IntStream.range(0, nRows)
									.filter(rowIndex -> testForFailure(statesTs, dataIRI, rowIndex))
									.count());
							Boolean refStateValue = assetList.getRefStateValue(dataIRI);
							if (null != refStateValue) {
								refStateValues.put(label, refStateValue);
							}
						}

						Map<String, String> siteSummary = site.getPropertyKeys().stream()
								.collect(Collectors.toMap(propKey -> propKey, site::getProperty));
						failureCount.entrySet().stream()
								.collect(Collectors.toMap(e -> e.getKey(), e -> Boolean.toString(
										refStateValues.getOrDefault(e.getKey(), true) && e.getValue() == 0),
										(e1, e2) -> e2, () -> siteSummary));
						siteSummary.put(assetOwnerColumnName, owner);
						siteSummary.put(siteTypeColumnName, siteType);
						siteSummary.put(siteNameColumnName, site.getName());
						siteSummary.put(siteIDColumnName, site.getId());
						siteSummary.put(siteCritColumnName, Double.toString(site.getCriticality()));

						final Long maxCount = Collections.max(failureCount.values());

						incrementSummary(total, siteTypeCount, siteTypeCriticalities, sitesInAOE, site);

						if (maxCount == 0L) {
							incrementSummary(noFail, siteTypeCount, siteTypeCriticalities, sitesInAOE, site);
						} else {
							incrementSummary(totalFailed, siteTypeCount, siteTypeCriticalities, sitesInAOE, site);
							for (String state : stateLabelsInOrder) {
								if (maxCount == failureCount.getOrDefault(state, 0L)) {
									incrementSummary(state, siteTypeCount, siteTypeCriticalities, sitesInAOE, site);
									break;
								}
							}
						}
						siteOwnerSummary.add(siteSummary);
					}
				}
				ownerCount.put(siteType, siteTypeCount);
				ownerCriticalities.put(siteType, siteTypeCriticalities);
			}
			try (BufferedWriter out = Files
					.newBufferedWriter(outputDir.resolve(scenarioDir).resolve(owner + FAILURE_CSV_SUFFIX));
					CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
				printer.printRecord(siteFailureColumnHeadings);
				for (Map<String, String> siteSummary : siteOwnerSummary) {
					for (String columnName : siteFailureColumnHeadings) {
						printer.print(siteSummary.getOrDefault(columnName, ""));
					}
					printer.println();
				}
			} catch (IOException ex) {
				throw new RuntimeException("Failed to write out owner specific failure file", ex);
			}
			summaryCount.put(owner, ownerCount);
			summaryCriticalities.put(owner, ownerCriticalities);
		}

		List<String> summarycolumnHeadings = List.of(assetOwnerColumnName, siteTypeColumnName, "Cause of failure",
				totalFailureInAreaColumnName, totalInAreaColumnName, totalFailureColumnName, totalColumnName,
				"Criticality of failed assets in area of interest", "Criticality of all assets in area of interest",
				"Criticality of failed assets in digital twin", "Criticality of all assets in digital twin");

		try (
				BufferedWriter out = Files
						.newBufferedWriter(outputDir.resolve(scenarioDir).resolve(FAILURE_SUMMARY_CSV));
				CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
			printer.printRecord(summarycolumnHeadings);
			summaryCount.forEach((owner, assetTypes) -> assetTypes
					.forEach((assetType, failureTypes) -> failureTypes.forEach((failureType, count) -> {
						if (stateLabelsInOrder.contains(failureType)) {
							try {
								Map<String, Double> criticalities = summaryCriticalities.get(owner).get(assetType);
								printer.print(owner);
								printer.print(assetType);
								printer.print(failureType);
								printer.print(failureTypes.getOrDefault(failureType + IN_AREA_SUFFIX, 0));
								printer.print(failureTypes.getOrDefault(total + IN_AREA_SUFFIX, 0));
								printer.print(count);
								printer.print(failureTypes.getOrDefault(total, 0));
								printer.print(criticalities.getOrDefault(failureType + IN_AREA_SUFFIX, 0.0));
								printer.print(criticalities.getOrDefault(total + IN_AREA_SUFFIX, 0.0));
								printer.print(criticalities.getOrDefault(failureType, 0.0));
								printer.print(criticalities.getOrDefault(total, 0.0));
								printer.println();
							} catch (IOException e) {
								throw new RuntimeException("Failed to write out failure summary file", e);
							}
						}
					})));
		} catch (IOException e) {
			throw new RuntimeException("Failed to write out failure summary file", e);
		}
	}

	private void incrementSummary(String columnName, Map<String, Integer> siteTypeCount,
			Map<String, Double> siteTypeCriticalities, List<String> sitesInAOE, Asset site) {
		siteTypeCount.compute(columnName, (key, value) -> value == null ? 1 : ++value);
		siteTypeCriticalities.compute(columnName,
				(key, value) -> (value == null ? 0 : value) + site.getCriticality());
		if (sitesInAOE.contains(site.getIri())) {
			incrementSummary(columnName + IN_AREA_SUFFIX, siteTypeCount, siteTypeCriticalities, Collections.emptyList(),
					site);
		}
	}

	/**
	 * recursive function to create metadata for a given site
	 * 
	 * @param asset
	 */
	void getAssetMetaData(Asset asset, JSONObject metaData, int timestep) {
		// force display order
		JSONArray order = new JSONArray();
		metaData.put(display_order, order);

		// generic properties
		metaData.put("Properties", getProperties(asset));
		order.put("Properties");

		// states
		if (asset.hasStates()) {
			asset.setState(timestep, false);
			TimeSeries<Integer> statesTs = asset.getStateTimeSeries();
			JSONObject states_json = new JSONObject();
			List<Integer> timeColumn = statesTs.getTimes();
			for (String dataIRI : statesTs.getDataIRIs()) {
				int row_index;
				if (timeColumn.contains(timestep)) {
					row_index = timeColumn.indexOf(timestep);
				} else if (timestep > timeColumn.get(timeColumn.size() - 1)) {
					// pick the last value
					row_index = timeColumn.size() - 1;
				} else { // pick value right before vis time step
					row_index = timeColumn.indexOf(timeColumn.stream().filter(t -> (t > timestep)).findFirst().get())
							- 1;
					if (row_index < 0) {
						row_index = 0;
					}
				}
				String stateLabel = this.assetList.getStateLabel(dataIRI);
				JSONObject stateProperties = new JSONObject(); // value and IRI
				JSONArray statePropertiesOrder = new JSONArray();

				stateProperties.put("IRI", dataIRI); // not displayed in vis by not adding into statePropertiesOrder

				if (!asset.getStateUnit(dataIRI).isEmpty()) {
					stateProperties.put("unit", asset.getStateUnit(dataIRI));
				}
				stateProperties.put("value", getTimeSeriesValues(statesTs, dataIRI).get(row_index));
				statePropertiesOrder.put("value");

				Double stateProbability = this.assetList.getStateProbability(dataIRI);
				if (null != stateProbability) {
					stateProperties.put("probability", stateProbability);
					statePropertiesOrder.put("probability");
				}
				// stateProperties.put(display_order, statePropertiesOrder);
				states_json.put(stateLabel, stateProperties);

				// set state of this asset for icon selection later
				// default value from getState is false (asset is ok)
				// once it's failed, it is not necessary to check for another state
				if (!asset.getState(timestep) && this.assetList.hasRefState(dataIRI)) {
					if (testForFailure(statesTs, dataIRI, row_index)) {
						asset.setState(timestep, true);
					}
				}
			}
			metaData.put("States", states_json);
			order.put("States");
		}

		// connections
		JSONObject connections = new JSONObject();

		// supplies to
		Iterator<String> suppliesToConnTypes = asset.getSuppliesTo().keySet().iterator();
		while (suppliesToConnTypes.hasNext()) {
			String suppliesToConnType = suppliesToConnTypes.next();
			JSONObject assets = new JSONObject();

			for (Asset suppliesToAsset : asset.getSuppliesTo().get(suppliesToConnType)) {
				assets.put(suppliesToAsset.getId(), getProperties(suppliesToAsset));
			}
			assets.put(collapse, true);
			connections.put(suppliesToConnType, assets);
		}

		// receives from
		Iterator<String> receivesFromConnTypes = asset.getReceivesFrom().keySet().iterator();
		while (receivesFromConnTypes.hasNext()) {
			String receivesFromConnType = receivesFromConnTypes.next();
			JSONObject assets = new JSONObject();

			for (Asset receivesFromAsset : asset.getReceivesFrom().get(receivesFromConnType)) {
				assets.put(receivesFromAsset.getId(), getProperties(receivesFromAsset));
			}
			assets.put(collapse, true);
			connections.put(receivesFromConnType, assets);
		}
		if (!connections.isEmpty()) {
			metaData.put("Connections", connections);
			order.put("Connections");
		}

		String partsKey = "Part(s):";

		for (Asset part : asset.getParts()) {
			JSONObject partjson = new JSONObject();

			JSONObject part1json = new JSONObject();
			part1json.put(part.getId(), partjson);

			if (metaData.has(partsKey)) {
				metaData.getJSONObject(partsKey).put(part.getId(), partjson);
			} else {
				metaData.put(partsKey, part1json);
				metaData.getJSONObject(partsKey).put(collapse, true);
			}

			getAssetMetaData(part, partjson, timestep);
		}

		if (asset.getParts().size() > 0) {
			order.put(partsKey);
			metaData.getJSONObject(partsKey).put(collapse, true);
		}
	}

	private boolean testForFailure(TimeSeries<Integer> statesTs, String dataIRI, int row_index) {
		boolean failedState;
		RefStateComparator comparator = this.assetList.getComparator(dataIRI);
		if (null != comparator) {
			switch (comparator) {
				case equals:
					boolean refState = this.assetList.getRefStateValue(dataIRI);
					// fail this asset if different from refState
					failedState = getTimeSeriesBooleanValues(statesTs, dataIRI).get(row_index) != refState;
					break;
				case max:
					// ref values are in another column of this same time series (max flood depth)
					String refStateIRI = this.assetList.getRefState(dataIRI);
					// fail this asset if value is greater than the maximum imposed depth
					failedState = getTimeSeriesDoubleValues(statesTs, dataIRI)
							.get(row_index) > getTimeSeriesDoubleValues(statesTs, refStateIRI).get(row_index);
					break;
				case min:
					refStateIRI = this.assetList.getRefState(dataIRI);
					// fail this asset if value is less than the minimum imposed depth
					failedState = getTimeSeriesDoubleValues(statesTs, dataIRI)
							.get(row_index) < getTimeSeriesDoubleValues(statesTs, refStateIRI).get(row_index);
					break;
				default:
					failedState = false;
					break;
			}
		} else {
			failedState = false;
		}
		return failedState;
	}

	private JSONObject getProperties(Asset asset) {
		// generic properties
		List<String> propertyKeys = asset.getPropertyKeys();
		JSONObject properties = new JSONObject();
		for (String key : propertyKeys) {
			properties.put(key, asset.getProperty(key));
		}
		if (!asset.getName().equals("")) {
			properties.put("name", asset.getName());
			propertyKeys.add("name");
		}
		properties.put("id", asset.getId());
		propertyKeys.add("id");

		properties.put("Criticality", asset.getCriticality());
		propertyKeys.add("Criticality");

		properties.put("IRI", asset.getIri()); // not displayed by not adding into propertyKeys

		// preferred display order for properties
		List<String> preferred_order = Arrays.asList("name", "id", "Criticality", "Latitude", "Longitude", "Easting",
				"Northing");
		JSONArray properties_order = new JSONArray();
		// for keys present in the preferred_order list
		for (String key : preferred_order) {
			if (propertyKeys.contains(key)) {
				properties_order.put(key);
			}
		}
		// append the array with the remaining keys
		for (String key : propertyKeys) {
			if (!preferred_order.contains(key)) {
				properties_order.put(key);
			}
		}

		properties.put(display_order, properties_order);
		return properties;
	}

	/**
	 * writes one file per unique asset owner filename = [owner name].geojson
	 * 
	 * @param siteLocations
	 */
	void writeAssetLocationGeoJSON() {

		List<String> owners = this.assetList.getSiteOwners();
		Iterator<String> colour_iterator = pointcolours.iterator();

		for (Integer i : this.vis_timesteps) {
			// 1 layer per asset type
			for (String owner : owners) {
				for (String siteType : this.assetList.getSiteTypes(owner)) {
					Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
							.resolve(siteType + "locations.geojson");

					JSONObject featureCollection = new JSONObject();
					featureCollection.put("type", "FeatureCollection");
					JSONArray features = new JSONArray();

					String colourToUse;
					if (colour_iterator.hasNext()) {
						colourToUse = colour_iterator.next();
					} else {
						colour_iterator = pointcolours.iterator();
						colourToUse = colour_iterator.next();
					}

					List<Asset> sites = assetList.getSites(owner, siteType);

					for (Asset site : sites) {
						if (!site.getLocation().isEmpty()) {
							String displayName = owner + ": " + site.getLabel() + " (" + site.getName() + ")";

							// each station will be a feature within FeatureCollection
							JSONObject feature = new JSONObject();

							// type
							feature.put("type", "Feature");

							// id for mapbox
							feature.put("id", site.getVisId());

							// properties (display name and styling)
							JSONObject property = new JSONObject();
							property.put("displayName", displayName);
							property.put("description", site.getComment());

							// true means site error
							if (standard_icons.containsKey(siteType)) {
								if (site.getState(i)) {
									property.put("icon-image", error_icons.get(siteType));
								} else {
									property.put("icon-image", standard_icons.get(siteType));
								}
							} else {
								property.put("circle-color", colourToUse);
								property.put("circle-stroke-width", 1);
								property.put("circle-stroke-color", "#000000"); // black
								property.put("circle-opacity", 0.75);
							}

							// for clustering to display number of flooded sites
							if (site.getState(i)) {
								property.put("floodCount", 1);
							} else {
								property.put("floodCount", 0);
							}

							// connections
							JSONArray connections = new JSONArray();
							for (Connection conn : site.getConnections()) {
								if (conn.getSupplier().getLocation().isEmpty()
										|| conn.getReceiver().getLocation().isEmpty()) {
									continue;
								}
								connections.put(conn.getLabel() + "/" + conn.getId());
							}
							property.put("connections", connections);

							feature.put("properties", property);

							// geometry
							feature.put("geometry", site.getLocation());

							// add to main array
							features.put(feature);
						}
					}
					featureCollection.put("features", features);

					writeToFile(file, featureCollection);
				}
			}
		}
	}

	void writeAreaOfInterestGeoJSON() {

		if (!areaOfInterestPolys.isEmpty()) {
			JSONObject featureCollection = new JSONObject();
			featureCollection.put("type", "FeatureCollection");
			JSONArray features = new JSONArray();
			featureCollection.put("features", features);

			int id = 1;
			for (Object areaOfInterestPoly : areaOfInterestPolys) {
				// A single feature within a FeatureCollection
				JSONObject feature = new JSONObject();

				// type
				feature.put("type", "Feature");

				// id for mapbox
				feature.put("id", id++);

				// properties (display name and styling)
				JSONObject properties = new JSONObject();
				properties.put("displayName", "Area of interest");
				properties.put("description", "Sub-region of interest");

				properties.put("fill-color", "#FE2712");
				properties.put("fill-outline-color", "#FE2712");
				properties.put("fill-opacity", 0.33);

				feature.put("properties", properties);

				feature.put("geometry", areaOfInterestPoly);
				features.put(feature);
			}

			for (Integer i : this.vis_timesteps) {
				Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
						.resolve("areaOfInterest.geojson");
				writeToFile(file, featureCollection);
			}
		}
	}

	/**
	 * lists in input, index 0 - asset1 location, index 1 - asset2 location index 2
	 * - connection type
	 * 
	 * @param connections
	 */
	void writeConnectionsGeoJSON() {

		List<String> connectionLabels = this.connList.getConnectionLabels();
		Iterator<String> colour_iterator = linecolours.iterator();

		// 1 layer per connection type
		for (String connectionLabel : connectionLabels) {
			List<Connection> connections = this.connList.getConnections(connectionLabel);

			JSONObject featureCollection = new JSONObject();
			featureCollection.put("type", "FeatureCollection");
			JSONArray features = new JSONArray();

			String colourToUse;
			if (colour_iterator.hasNext()) {
				colourToUse = colour_iterator.next();
			} else {
				colour_iterator = linecolours.iterator();
				colourToUse = colour_iterator.next();
			}

			for (Connection conn : connections) {
				// skip this loop if either side of the connection does not have coordinates
				if (conn.getSupplier().getLocation().isEmpty() || conn.getReceiver().getLocation().isEmpty()) {
					continue;
				}

				JSONObject feature = new JSONObject();

				// type
				feature.put("type", "Feature");

				// id for mapbox
				feature.put("id", conn.getId());

				// properties (display name and styling)
				JSONObject property = new JSONObject();
				if (credoColours.containsKey(conn.getConnectionType())) {
					property.put("line-color", credoColours.get(conn.getConnectionType()));
				} else {
					property.put("line-color", colourToUse); // random colour
				}
				property.put("displayName", conn.getDisplayName());
				property.put("description", conn.getComment());
				property.put("from", conn.getSupplier().getLabel() + "/" + conn.getSupplier().getVisId());
				property.put("to", conn.getReceiver().getLabel() + "/" + conn.getReceiver().getVisId());

				feature.put("properties", property);

				// process line coordinates
				JSONArray point1 = conn.getReceiver().getLocation().getJSONArray("coordinates");
				JSONArray point2 = conn.getSupplier().getLocation().getJSONArray("coordinates");
				JSONArray line = new JSONArray().put(point2).put(point1);

				// geometry
				JSONObject geometry = new JSONObject();
				geometry.put("type", "LineString");
				geometry.put("coordinates", line);
				feature.put("geometry", geometry);

				// add to main array
				features.put(feature);
			}

			featureCollection.put("features", features);

			for (Integer i : this.vis_timesteps) {
				Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
						.resolve(connectionLabel + ".geojson");
				writeToFile(file, featureCollection);
			}
		}
	}

	void writeConnectionsMeta() {

		List<String> connectionLabels = this.connList.getConnectionLabels();
		for (String connectionLabel : connectionLabels) {
			JSONArray metaDataCollection = new JSONArray();
			List<Connection> connections = this.connList.getConnections(connectionLabel);

			for (Connection conn : connections) {
				if (conn.getSupplier().getLocation().isEmpty() || conn.getReceiver().getLocation().isEmpty()) {
					continue;
				}
				JSONObject metadata = new JSONObject();
				metadata.put("id", conn.getId());
				metadata.put("Connection type", conn.getLabel());
				metadata.put("Description", conn.getComment());

				JSONObject supplier = new JSONObject();
				JSONArray supplierOrder = new JSONArray();
				if (!conn.getSupplier().getName().equals("")) {
					supplier.put("name", conn.getSupplier().getName());
					supplierOrder.put("name");
				}
				supplier.put("id", conn.getSupplier().getId());
				supplierOrder.put("id");

				// hidden in vis by not adding into supplierOrder
				supplier.put("IRI", conn.getSupplier().getIri());
				supplier.put(display_order, supplierOrder);

				JSONObject receiver = new JSONObject();
				JSONArray receiverOrder = new JSONArray();
				if (!conn.getReceiver().getName().equals("")) {
					receiver.put("name", conn.getReceiver().getName());
					receiverOrder.put("name");
				}
				receiver.put("id", conn.getReceiver().getId());
				receiverOrder.put("id");

				// hide in vis
				receiver.put("IRI", conn.getReceiver().getIri());
				receiver.put(display_order, receiverOrder);

				metadata.put("Supplier", supplier);
				metadata.put("Receiver", receiver);
				metaDataCollection.put(metadata);
			}

			for (Integer i : this.vis_timesteps) {
				Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
						.resolve(connectionLabel + "meta.json");
				writeToFile(file, metaDataCollection);
			}
		}
	}

	void writeTimeSeries(TimeSeriesClient<Integer> tsClient) {

		final Integer lastSimTime = this.vis_timesteps.get(this.vis_timesteps.size() - 1);

		// 1 file per asset sub type
		for (String owner : this.assetList.getSiteOwners()) {
			for (String siteType : this.assetList.getSiteTypes(owner)) {
				List<TimeSeries<Integer>> tsList = new ArrayList<>();
				List<Asset> sites = this.assetList.getSites(owner, siteType);
				List<Integer> visIdList = new ArrayList<>();
				List<Map<String, String>> unitsList = new ArrayList<>();
				for (Asset site : sites) {
					if (site.getLocation().isEmpty()) {
						continue;
					}
					// time series queried directly
					TimeSeries<Integer> rawTimeSeries = site.getStateTimeSeries();
					if (rawTimeSeries == null) {
						continue;
					}
					// convert time series so that it has the visualisation time steps
					List<Integer> timeColumn = new ArrayList<>(rawTimeSeries.getTimes());
					boolean extendTimeSeries = lastSimTime > timeColumn.get(timeColumn.size() - 1);
					if (extendTimeSeries) {
						timeColumn.add(lastSimTime);
					}

					List<List<?>> newValueColumns = new ArrayList<>();
					Map<String, String> units = new HashMap<>();
					List<String> tableHeaders = new ArrayList<>();

					for (String dataIRI : rawTimeSeries.getDataIRIs()) {
						List<Object> newValues = getTimeSeriesValues(rawTimeSeries, dataIRI);
						if (extendTimeSeries) {
							newValues.add(newValues.get(newValues.size() - 1));
						}
						newValueColumns.add(newValues);
						units.put(assetList.getStateLabel(dataIRI), site.getStateUnit(dataIRI));
						tableHeaders.add(assetList.getStateLabel(dataIRI));
					}

					// in the processed time series, dataIRIs are replaced by the labels, which are
					// then used as the headers in the vis
					TimeSeries<Integer> processedTimeSeries = new TimeSeries<Integer>(
							timeColumn, tableHeaders, newValueColumns);
					tsList.add(processedTimeSeries);
					visIdList.add(site.getVisId());
					unitsList.add(units);
				}

				JSONArray ts_JSON = tsClient.convertToJSON(tsList, visIdList, unitsList, null);
				for (Integer i : this.vis_timesteps) {
					Path file = outputDir.resolve(scenarioDir).resolve(i.toString())
							.resolve(siteType + "timeseries.json");
					writeToFile(file, ts_JSON);
				}
			}
		}
	}

	void writeMissingDataFile() {

		Path file = outputDir.resolve(scenarioDir).resolve(MISSING_DATA_JSON);

		JSONObject overallJSON = new JSONObject();
		JSONArray missingNameAssets = new JSONArray();

		for (Asset asset : assetList.getAssetsWithoutName()) {
			JSONObject assetJson = new JSONObject();
			assetJson.put("id", asset.getId());
			assetJson.put("IRI", asset.getIri());
			missingNameAssets.put(assetJson);
		}
		overallJSON.put("Assets with missing name", missingNameAssets);

		JSONArray missingLocationAssets = new JSONArray();

		for (Asset asset : assetList.getAssetsWithoutLocation()) {
			JSONObject assetJson = new JSONObject();
			assetJson.put("id", asset.getId());
			assetJson.put("IRI", asset.getIri());
			missingLocationAssets.put(assetJson);
		}

		overallJSON.put("Sites with missing location", missingLocationAssets);

		// assets with parthood error
		JSONArray functionalErrorAssets = new JSONArray();

		for (Asset asset : assetList.getAssetsWithFunctionalRelationError()) {
			JSONObject assetJson = new JSONObject();
			assetJson.put("id", asset.getId());
			assetJson.put("IRI", asset.getIri());
			functionalErrorAssets.put(assetJson);
		}

		overallJSON.put("Assets with functional relation error", functionalErrorAssets);

		writeToFile(file, overallJSON);
	}

	private void writeToFile(Path file, JSONObject output) {
		try (BufferedWriter writer = Files.newBufferedWriter(file)) {
			output.write(writer, 4, 0);
			LOGGER.info("Created " + file.toAbsolutePath());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new JPSRuntimeException(e);
		}
	}

	private void writeToFile(Path file, JSONArray output) {
		try (BufferedWriter writer = Files.newBufferedWriter(file)) {
			output.write(writer, 4, 0);
			LOGGER.info("Created " + file.toAbsolutePath());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new JPSRuntimeException(e);
		}
	}

	private List<Object> getTimeSeriesValues(TimeSeries<Integer> statesTs, String dataIRI) {
		List<?> values = statesTs.getValues(dataIRI);
		List<Object> valuesOut = new ArrayList<>(values.size());
		Object previousNonNullValue = null;
		for (Object value : values) {
			if (null != value) {
				previousNonNullValue = value;
			}
			valuesOut.add(previousNonNullValue);
		}
		return valuesOut;
	}

	private List<Boolean> getTimeSeriesBooleanValues(TimeSeries<Integer> statesTs, String dataIRI) {
		List<?> values = statesTs.getValues(dataIRI);
		List<Boolean> valuesOut = new ArrayList<>(values.size());
		Boolean previousNonNullValue = null;
		for (Object value : values) {
			if (null != value) {
				previousNonNullValue = (Boolean) value;
			}
			valuesOut.add(previousNonNullValue);
		}
		return valuesOut;
	}

	private List<Double> getTimeSeriesDoubleValues(TimeSeries<Integer> statesTs, String dataIRI) {
		List<?> values = statesTs.getValues(dataIRI);
		List<Double> valuesOut = new ArrayList<>(values.size());
		Double previousNonNullValue = null;
		for (Object value : values) {
			if (null != value) {
				previousNonNullValue = ((Number) value).doubleValue();
			}
			valuesOut.add(previousNonNullValue);
		}
		return valuesOut;
	}

	// copy and integrate buildings/river data
	void copyBuildingsAndRiverData() {
		List<String> dataFolders = new ArrayList<>();
		if (this.riverDataFolder != null) {
			dataFolders.add(this.riverDataFolder);
		}
		if (this.buildingsDataFolder != null) {
			dataFolders.add(this.buildingsDataFolder);
		}

		for (String folder : dataFolders) {
			try {
				File overallMetaFile = Paths.get(folder, "meta.json").toFile();
				JSONObject overallMeta = new JSONObject(FileUtils.readFileToString(overallMetaFile, "UTF-8"));
				String directoryToRead = overallMeta.getJSONObject("local").getJSONArray("groups").getJSONObject(0)
						.getString("directory");

				File directoryToCopy = Paths.get(folder, directoryToRead).toFile();
				for (File file : directoryToCopy.listFiles()) {
					if (file.getName().equals("meta.json")) {
						continue;
					}
					for (Integer i : this.vis_timesteps) {
						File destinationDir = outputDir.resolve(scenarioDir).resolve(i.toString()).toFile();
						FileUtils.copyFileToDirectory(file, destinationDir);
					}
				}
				LOGGER.info("Copied files from " + folder);
			} catch (Exception e) {
				LOGGER.error("Error in copying data in " + folder);
				LOGGER.error(e.getMessage());
			}
		}
	}

	void writeDAFNIPublisherMetadata() {
		try {
			InputStream inputstream = new ClassPathResource("dafni_metadata.json").getInputStream();
			JSONObject dafni_metadata = new JSONObject(new String(inputstream.readAllBytes(), StandardCharsets.UTF_8));
			dafni_metadata.put("dct:title", "CReDo digital twin output (" + hipims_code +")");
			dafni_metadata.put("dct:description", "Outputs from " + Config.WORKFLOW_NAME + " to analyse the effects of a HiPIMS simulation of a " 
				+ floodCodeToDescriptionMap.get(hipims_code) + ". Use the CMCL Vis App to view the outputs. " + 
				"This can be accessed in the data catalogue page by clicking the tickbox next to this dataset and clicking the Visualise box at the top of the page.");
			dafni_metadata.put("dct:created", LocalDate.now());
			dafni_metadata.put("dcat:keyword", new JSONArray().put(hipims_code).put("digital twin").put("CReDo"));

			writeToFile(Paths.get(Config.DAFNI_METADATA_FILE), dafni_metadata);
		} catch (Exception e) {
			LOGGER.error("Failed to write metadata for DAFNI publisher");
			LOGGER.error(e.getMessage());
		}
	}
}
