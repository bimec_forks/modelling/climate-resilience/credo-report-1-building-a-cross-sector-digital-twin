package com.cmclinnovations.credo;

import java.net.URISyntaxException;
import java.nio.file.Path;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.apache.http.client.utils.URIBuilder;

public class Config {
	private static final Logger LOGGER = LogManager.getLogger(Config.class);
	/// URL of triple-store that stores the TBox
	public static String KG_URL;
	
	// parameters used to construct KG_URL
	private static String KG_HOST = System.getenv("KG_HOST");
	private static String KG_PATH = System.getenv("KG_PATH");
	private static String KG_PROTOCOL = System.getenv("KG_PROTOCOL");
	private static String KG_PORT = System.getenv("KG_PORT");
	
	/// JBDC URL of the Timeseries database
	public static String TIMESERIES_RDB_URL;

	// parameters used to construct TIMESERIES_RDB_URL
	private static String POSTGRES_HOST = System.getenv("POSTGRES_HOST");
	private static String POSTGRES_PORT = System.getenv("POSTGRES_PORT");
	private static String TIMESERIES_DB = System.getenv("TIMESERIES_DB");

	/// Username of the relational database server
	public static final String POSTGRES_USER = System.getenv("POSTGRES_USER");
	/// Password of the relational database server
	public static final String POSTGRES_PASSWORD = System.getenv("POSTGRES_PASSWORD");

	/// URL of ontop database, this is the OBDA database storing the asset data
	public static String ONTOP_URL;

	// parameters used to construct ONTOP_URL
	private static String ONTOP_HOST = System.getenv("ONTOP_HOST");
	private static String ONTOP_PATH = System.getenv("ONTOP_PATH");
	private static String ONTOP_PORT = System.getenv("ONTOP_PORT");
	private static String ONTOP_PROTOCOL = System.getenv("ONTOP_PROTOCOL");

	/// Directory to write the output files
	public static final Path OUTPUT_DIR = Path.of(System.getenv("OUTPUT_DIR"));
	/// Directory to read GeoTiffs from
	public static final Path GEOTIF_IN_DIR = Path.of(System.getenv("GEOTIF_DIR"));
	/// Directory to read GeoTiffs from
	public static final Path GEOTIF_OUT_DIR = OUTPUT_DIR.resolve(GEOTIF_IN_DIR.getFileName());

	// ## Optional environment variables

	/// If "true", writes out the trimmed down version of the
	/// output files for the other models to read. Otherwise it writes out
	/// everything in the form required by the visualisation
	public static final boolean ONLY_SINGLE_TIMESTEP = Boolean.parseBoolean(System.getenv("ONLY_SINGLE_TIMESTEP"));
	/// Absolute path to the directory containing river sensor data (extracted using
	/// https://github.com/cambridge-cares/TheWorldAvatar/tree/develop/Agents/FloodAgent)
	public static final String RIVER_DATA_DIR = System.getenv("RIVER_DATA_DIR");
	/// Absolute path to the directory containing buildings data (extracted using
	/// https://github.com/cambridge-cares/TheWorldAvatar/tree/dev-dockerise-kingslynnagent/Agents/KingsLynnAgent)
	public static final String BUILDINGS_DATA_DIR = System.getenv("BUILDINGS_DATA_DIR");

	/// Username of triple-store
	public static final String KG_USER = System.getenv("KG_USER");
	/// Password of triple-store
	public static final String KG_PASSWORD = System.getenv("KG_PASSWORD");

	// mainly for DAFNI
	// csv files that contains user facing texts for each flood event
	public static final String FLOOD_LABEL_FILE = System.getenv("FLOOD_LABEL_FILE");
	// directory to write metadata for DAFNI publisher, this should be /data/outputs/ when running as a DAFNI workflow
	public static final String DAFNI_METADATA_FILE = System.getenv("DAFNI_METADATA_FILE");
	// name of the calling workflow, used in metadata
	public static final String WORKFLOW_NAME = System.getenv("WORKFLOW_NAME");
	
	/**
	 * construct URLs for KG_URL, ONTOP_URL, TIMESERIES_RDB_URL
	 */
	public static void initURLs() {
		// KG URL
		if (KG_HOST != null && KG_PATH != null && KG_PROTOCOL != null) {
			URIBuilder kgurl_builder = new URIBuilder();
			kgurl_builder.setHost(KG_HOST);
			kgurl_builder.setPath(KG_PATH);
			kgurl_builder.setScheme(KG_PROTOCOL);
			if (KG_PORT != null) {
				kgurl_builder.setPort(Integer.parseInt(KG_PORT));
			}
			try {
				KG_URL = kgurl_builder.build().toString();
				LOGGER.info("Constructed KG_URL = " + KG_URL);
			} catch (URISyntaxException e) {
				LOGGER.error("Error building KG_URL");
				LOGGER.error(e.getMessage());
				throw new RuntimeException(e);
			}
		} else {
			String errmsg = "KG_HOST, KG_PATH and KG_PROTOCOL must be present as environment variables";
			LOGGER.error(errmsg);
			throw new RuntimeException(errmsg);
		}

		// ONTOP URL
		if (ONTOP_HOST != null && ONTOP_PATH != null && ONTOP_PROTOCOL != null) {
			URIBuilder ontop_builder = new URIBuilder();
			ontop_builder.setHost(ONTOP_HOST);
			ontop_builder.setPath(ONTOP_PATH);
			ontop_builder.setScheme(ONTOP_PROTOCOL);
			if (ONTOP_PORT != null) {
				ontop_builder.setPort(Integer.parseInt(ONTOP_PORT));
			}
			try {
				ONTOP_URL = ontop_builder.build().toString();
				LOGGER.info("Constructed ONTOP_URL = " + ONTOP_URL);
			} catch (URISyntaxException e) {
				LOGGER.error("Error building ONTOP_URL");
				LOGGER.error(e.getMessage());
				throw new RuntimeException(e);
			}
		} else {
			String errmsg = "ONTOP_HOST, ONTOP_PATH, and ONTOP_PROTOCOL must be present as environment variables";
			LOGGER.error(errmsg);
			throw new RuntimeException(errmsg);
		}

		// TIMESERIES_RDB_URL
		if (TIMESERIES_DB != null) {
			if (POSTGRES_USER == null || POSTGRES_PASSWORD == null) {
				throw new RuntimeException("The environment variable POSTGRES_USER/POSTGRES_PASSWORD is not set");
			}
			if (POSTGRES_HOST != null) {
				if (POSTGRES_PORT != null) {
					TIMESERIES_RDB_URL = "jdbc:postgresql://" + POSTGRES_HOST + ":" + POSTGRES_PORT + "/" + TIMESERIES_DB;
				} else {
					TIMESERIES_RDB_URL = "jdbc:postgresql://" + POSTGRES_HOST + "/" + TIMESERIES_DB;
				}
			} else {
				TIMESERIES_RDB_URL = "jdbc:postgresql:" + TIMESERIES_DB;
			}

			LOGGER.info("Time series will be queried from " + TIMESERIES_RDB_URL);
		} else {
			String errmsg = "The environment variable TIMESERIES_DB is not set";
			LOGGER.error(errmsg);
			throw new RuntimeException(errmsg);
		}
	}
}