package com.cmclinnovations.credo.sparqlbuilder;

import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;

public class AskQuery {
    GraphPattern gp;
    public AskQuery (GraphPattern gp) {
        this.gp = gp;
    }

    public String getQueryString() {
        return "ask {" + gp.getQueryString() + "}";
    }
}
