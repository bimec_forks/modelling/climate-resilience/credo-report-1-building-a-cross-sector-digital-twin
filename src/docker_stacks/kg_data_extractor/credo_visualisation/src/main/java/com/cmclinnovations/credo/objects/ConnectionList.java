package com.cmclinnovations.credo.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * connections grouped by the label, 1 layer per label in the visualisation
 */
public class ConnectionList {
    private Map<String, List<Connection>> connections; // for visualisation, type - connection

    public ConnectionList() {
        this.connections = new HashMap<>();
    }

    public void addConnection(Connection conn) {
        // for type-connection map
        if (this.connections.containsKey(conn.getLabel())) {
            this.connections.get(conn.getLabel()).add(conn);
        } else {
            List<Connection> newList = new ArrayList<>();
            newList.add(conn);
            this.connections.put(conn.getLabel(), newList);
        }
    }

    public List<String> getConnectionLabels() {
        return new ArrayList<>(connections.keySet());
    }

    public List<Connection> getConnections(String label) {
        return connections.get(label);
    }
}
