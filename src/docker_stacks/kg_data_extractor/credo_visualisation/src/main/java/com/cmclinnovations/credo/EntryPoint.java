package com.cmclinnovations.credo;

import java.util.Arrays;
import java.util.List;

import com.cmclinnovations.credo.objects.AssetList;
import com.cmclinnovations.credo.objects.ConnectionList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import uk.ac.cam.cares.jps.base.query.RemoteStoreClient;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeriesClient;

public class EntryPoint {
	private static final Logger LOGGER = LogManager.getLogger(EntryPoint.class);

	public static void main(String[] args) {
		if (Config.ONLY_SINGLE_TIMESTEP) {
			LOGGER.info("Detected environment variable ONLY_SINGLE_TIMESTEP = " + Config.ONLY_SINGLE_TIMESTEP);
		}

		// integrate river sensor and buildings data
		if (Config.RIVER_DATA_DIR != null) {
			LOGGER.info("Detected environment variable for RIVER_DATA_DIR = " + Config.RIVER_DATA_DIR);
		}
		if (Config.BUILDINGS_DATA_DIR != null) {
			LOGGER.info("Detected environment variable for BUILDINGS_DATA_DIR = " + Config.BUILDINGS_DATA_DIR);
		}
		Config.initURLs();
		RemoteStoreClient storeClient = new RemoteStoreClient(Config.KG_URL, Config.KG_URL,
				Config.KG_USER, Config.KG_PASSWORD);

		TimeSeriesClient<Integer> tsClient = new TimeSeriesClient<Integer>(storeClient, Integer.class,
				Config.TIMESERIES_RDB_URL, Config.POSTGRES_USER, Config.POSTGRES_PASSWORD);

		// Integer is the class of the time column, change this line manually to change
		// the time class
		QueryClient queryClient = new QueryClient(storeClient, tsClient);

		JSONArray areaOfInterestPolys = queryClient.getAreaOfInterestPolys();

		AssetList assetList = queryClient.initialiseAssets();

		// get list of sites for each unique asset owner
		queryClient.organiseSites(assetList);

		// connection
		ConnectionList connList = queryClient.getConnections(assetList);

		assetList.calcSiteCriticality();
		// parthood
		queryClient.addParthood(assetList);

		// add properties to all assets
		queryClient.addPropertiesToAssets(assetList);

		// time step for visualisation
		// temporarily using simulation time
		String visTimeIRI = "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#simulation_time";
		List<Integer> vis_timesteps;
		try {
			vis_timesteps = tsClient.getTimeSeries(Arrays.asList(visTimeIRI)).getValuesAsInteger(visTimeIRI);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			vis_timesteps = Arrays.asList(0);
		}

		// query state time series
		queryClient.addStatesTimeSeries(assetList, vis_timesteps);
		queryClient.getReferenceStates(assetList);

		FloodDataProcessor floodDataProcessor = new FloodDataProcessor();

		Writer writer = new Writer(Config.OUTPUT_DIR, vis_timesteps, assetList, connList, areaOfInterestPolys,
				Config.ONLY_SINGLE_TIMESTEP, Config.RIVER_DATA_DIR, Config.BUILDINGS_DATA_DIR, floodDataProcessor);

		// Actions that are required when Config.ONLY_SINGLE_TIMESTEP == true
		writer.removeOldOutput();
		writer.makeDirectories();
		writer.writeAssetsMeta(); // must run this before 'writeAssetLocationGeoJSON' to choose the right icon
		if (!Config.ONLY_SINGLE_TIMESTEP) {
			// Actions that should only be run when Config.ONLY_SINGLE_TIMESTEP == false
			writer.writeDAFNIPublisherMetadata();
			floodDataProcessor.generateGeoserverFiles();
			writer.writeFailureSummaryFile(queryClient);
			writer.writeOverallMeta();
			writer.writeLayerTree();
			writer.writeOverallScenarioMeta();
			writer.writeIndividualScenarioMeta();
			writer.writeAssetLocationGeoJSON();
			writer.writeConnectionsGeoJSON();
			writer.writeConnectionsMeta();
			writer.writeTimeSeries(tsClient); // only using convertToJSON method from tsClient
			writer.writeAreaOfInterestGeoJSON();
			writer.writeMissingDataFile();
			writer.copyBuildingsAndRiverData();
		}
	}
}
