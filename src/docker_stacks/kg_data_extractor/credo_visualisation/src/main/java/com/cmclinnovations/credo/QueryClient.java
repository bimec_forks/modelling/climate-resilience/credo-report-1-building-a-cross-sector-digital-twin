package com.cmclinnovations.credo;

import static org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf.iri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.cmclinnovations.credo.objects.Asset;
import com.cmclinnovations.credo.objects.AssetList;
import com.cmclinnovations.credo.objects.AssetList.RefStateComparator;
import com.cmclinnovations.credo.objects.Connection;
import com.cmclinnovations.credo.objects.ConnectionList;
import com.cmclinnovations.credo.sparqlbuilder.AskQuery;
import com.cmclinnovations.credo.sparqlbuilder.GeoSPARQL;
import com.cmclinnovations.credo.sparqlbuilder.ServiceEndpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.PropertyPaths;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.core.query.SelectQuery;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.TriplePattern;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;
import org.json.JSONArray;
import org.json.JSONObject;

import uk.ac.cam.cares.jps.base.interfaces.StoreClientInterface;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeries;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeriesClient;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeriesSparql;

public class QueryClient {
	private static final Logger LOGGER = LogManager.getLogger(QueryClient.class);
	private static String credo_namespace = "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#";
	StoreClientInterface storeClient;
	TimeSeriesClient<Integer> tsClient;

	// prefix
	private static Prefix p_credo = SparqlBuilder.prefix("credo",
			iri(credo_namespace));
	private static Prefix p_geof = SparqlBuilder.prefix("geof",
			iri("http://www.opengis.net/def/function/geosparql/"));

	// class
	private static Iri Site = p_credo.iri("Site");
	private static Iri Asset = p_credo.iri("Asset");

	// iri to name map
	private Map<String, String> ownerIriNameMap = null;

	// object property
	private static Iri hasLocation = p_credo.iri("hasLocation");
	private static Iri hasJSON = p_credo.iri("hasJSON");
	private static Iri hasWKB = p_credo.iri("hasWKB");
	private static Iri hasOwner = p_credo.iri("hasOwner");
	private static Iri hasName = p_credo.iri("hasName");
	private static Iri hasPart = iri(
			"http://www.theworldavatar.com/ontology/meta_model/mereology/mereology.owl#hasPart");
	private static Iri supplies = p_credo.iri("supplies");
	private static Iri hasProperty = p_credo.iri("hasProperty");
	private static Iri hasValue = p_credo.iri("hasValue");
	private static Iri hasState = p_credo.iri("hasState");
	private static Iri hasProbability = p_credo.iri("hasProbability");
	private static Iri hasId = p_credo.iri("hasId");
	private static Iri areaOfInterest = p_credo.iri("areaOfInterest");
	private static Iri hasUnit = p_credo.iri("hasUnit");

	// Service endpoints
	ServiceEndpoint ontop = new ServiceEndpoint(Config.ONTOP_URL);

	public QueryClient(StoreClientInterface storeClient, TimeSeriesClient<Integer> tsClient) {
		this.storeClient = storeClient;
		this.tsClient = tsClient;
	}

	AssetList initialiseAssets() {
		SelectQuery query = Queries.SELECT();
		Variable assetSubclass = query.var();
		Variable asset = query.var();
		Variable assetLabel = query.var();
		Variable assetComment = query.var();
		Variable assetName = query.var();
		Variable assetId = query.var();

		GraphPattern subClassPattern = assetSubclass.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBCLASSOF)), Asset);
		GraphPattern labelPattern = assetSubclass.has(RDFS.LABEL, assetLabel).optional();
		GraphPattern commentPattern = assetSubclass.has(RDFS.COMMENT, assetComment).optional();
		GraphPattern assetType = asset.isA(assetSubclass);
		GraphPattern assetNameTp = asset.has(hasName, assetName).optional();
		GraphPattern assetIdTp = asset.has(hasId, assetId).optional();

		query.select(assetSubclass, asset, assetLabel, assetComment, assetName, assetId)
				.where(ontop.service(assetType, assetNameTp, assetIdTp), subClassPattern, labelPattern, commentPattern)
				.prefix(p_credo);

		AssetList assetList = new AssetList();
		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		for (int i = 0; i < queryResult.length(); i++) {
			String rdfType = queryResult.getJSONObject(i).getString(assetSubclass.getQueryString().substring(1));
			String assetIRI = queryResult.getJSONObject(i).getString(asset.getQueryString().substring(1));

			String name;
			boolean missingName = false;
			if (queryResult.getJSONObject(i).has(assetName.getQueryString().substring(1))) {
				name = queryResult.getJSONObject(i).getString(assetName.getQueryString().substring(1));
			} else {
				name = "";
				missingName = true;
				// LOGGER.warn(assetIRI + " does not have a name");
			}

			String id = queryResult.getJSONObject(i).getString(assetId.getQueryString().substring(1));

			String label;
			if (queryResult.getJSONObject(i).has(assetLabel.getQueryString().substring(1))) {
				label = queryResult.getJSONObject(i).getString(assetLabel.getQueryString().substring(1));
			} else {
				label = "";
			}

			String comment;
			if (queryResult.getJSONObject(i).has(assetComment.getQueryString().substring(1))) {
				comment = queryResult.getJSONObject(i).getString(assetComment.getQueryString().substring(1));
			} else {
				comment = "";
			}

			// some assets have more than 1 rdf type
			if (!assetList.hasAsset(assetIRI)) {
				Asset newAsset = new Asset(assetIRI);
				newAsset.setType(rdfType);
				newAsset.setName(name);
				newAsset.setLabel(label);
				newAsset.setComment(comment);
				newAsset.setId(id);
				assetList.addAsset(newAsset);
				if (missingName) {
					assetList.addAssetWithoutName(assetIRI);
				}
			} else if (isSubProperty(rdfType, assetList.getAsset(assetIRI).getType())) {
				LOGGER.warn(assetIRI + " has more than 1 rdf:type attached, replacing with the lower level class");
				assetList.getAsset(assetIRI).setType(rdfType);
				assetList.getAsset(assetIRI).setName(name);
				assetList.getAsset(assetIRI).setLabel(label);
				assetList.getAsset(assetIRI).setComment(comment);
			}
		}
		return assetList;
	}

	boolean isSubProperty(String subclass1, String subclass2) {
		GraphPattern queryPattern = iri(subclass1).has(PropertyPaths.zeroOrMore(iri(RDFS.SUBCLASSOF)), iri(subclass2));
		AskQuery query = new AskQuery(queryPattern);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());
		return queryResult.getJSONObject(0).getBoolean("ASK");
	}

	/**
	 * organise sites according to owner
	 */
	void organiseSites(AssetList assetList) {
		SelectQuery query = Queries.SELECT();
		Variable site = query.var();
		Variable siteSubType = query.var();
		Variable geojson = query.var();
		Variable areaOfInterestPoly = query.var();
		Variable point = query.var();
		Variable owner = query.var();

		GraphPattern siteDataPattern;

		Boolean onlyShowSitesInAOI = Boolean.parseBoolean(System.getenv("ONLY_SHOW_SITES_IN_AOI"));
		if (onlyShowSitesInAOI) {
			GraphPattern areaOfInterestPattern = GraphPatterns.and(areaOfInterest.has(PropertyPaths.path(
					hasLocation, hasWKB), areaOfInterestPoly)).optional();

			GraphPattern locationPattern = GraphPatterns.and(
					site.has(PropertyPaths.path(hasLocation, hasJSON), geojson),
					site.has(PropertyPaths.path(hasLocation, hasWKB), point))
					.filter(Expressions.or(
							Expressions.not(Expressions.bound(areaOfInterestPoly)),
							GeoSPARQL.sfContains(areaOfInterestPoly, point)))
					.optional();

			siteDataPattern = GraphPatterns.and(areaOfInterestPattern,
					site.isA(siteSubType).andHas(hasOwner, owner), locationPattern);
		} else {
			GraphPattern locationPattern = site.has(PropertyPaths.path(hasLocation, hasJSON), geojson).optional();

			siteDataPattern = GraphPatterns.and(
					site.isA(siteSubType).andHas(hasOwner, owner), locationPattern);
		}

		GraphPattern subClassPattern = GraphPatterns.and(
				siteSubType.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBCLASSOF)), Site));

		// site data are in ontop, TBox are in KG
		query.select(site, geojson, owner).prefix(p_credo, p_geof)
				.where(ontop.service(siteDataPattern), subClassPattern);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		for (int i = 0; i < queryResult.length(); i++) {
			// compulsory variables
			String ownerIRI = queryResult.getJSONObject(i).getString(owner.getQueryString().substring(1));
			String siteIRI = queryResult.getJSONObject(i).getString(site.getQueryString().substring(1));

			// add to list
			// assetList.addSite(getOwnerName(ownerIRI), labelString, newSite);
			// add ID based on the number of sites of that owner
			Asset siteAsset;
			if (assetList.hasAsset(siteIRI)) {
				siteAsset = assetList.getAsset(siteIRI);
			} else {
				LOGGER.warn(siteIRI + " is not a sub class of Asset.");
				siteAsset = new Asset(siteIRI);
				assetList.addAsset(siteAsset);
			}

			JSONObject geometry;
			if (queryResult.getJSONObject(i).has(geojson.getQueryString().substring(1))) {
				geometry = new JSONObject(
						queryResult.getJSONObject(i).getString(geojson.getQueryString().substring(1)));
			} else {
				// LOGGER.warn(siteIRI + " does not have a location.");
				geometry = new JSONObject();
				assetList.addAssetWithoutLocation(siteIRI);
			}

			siteAsset.setLocation(geometry);
			assetList.organiseSiteWithOwner(getOwnerName(ownerIRI), siteAsset);
			siteAsset.setVisId(assetList.getSites(getOwnerName(ownerIRI), siteAsset.getLabel()).size());
		}
	}

	String getOwnerName(String owner) {
		if (ownerIriNameMap == null) {
			ownerIriNameMap = new HashMap<String, String>();
		}
		if (!ownerIriNameMap.containsKey(owner)) {
			SelectQuery query = Queries.SELECT();
			Variable ownerName = query.var();
			query.select(ownerName).where(ontop.service(iri(owner).has(hasName, ownerName))).prefix(p_credo);

			JSONArray queryResult = storeClient.executeQuery(query.getQueryString());
			String ownerNameString;
			try {
				ownerNameString = queryResult.getJSONObject(0).getString(ownerName.getQueryString().substring(1));
			} catch (Exception e) {
				String errmsg = owner + " does not have an associated name";
				LOGGER.error(errmsg);
				ownerNameString = "";
			}
			ownerIriNameMap.put(owner, ownerNameString);
		}
		return ownerIriNameMap.get(owner);
	}

	void addParthood(AssetList assetList) {
		SelectQuery query = Queries.SELECT();

		Variable asset = query.var();
		Variable part = query.var();

		GraphPattern sitePattern = asset.has(hasPart, part);

		query.prefix(p_credo).where(ontop.service(sitePattern)).select(part, asset);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		for (int i = 0; i < queryResult.length(); i++) {
			String assetIRI = queryResult.getJSONObject(i).getString(asset.getQueryString().substring(1));
			String partIRI = queryResult.getJSONObject(i).getString(part.getQueryString().substring(1));

			Asset a1;
			if (assetList.hasAsset(assetIRI)) {
				a1 = assetList.getAsset(assetIRI);
			} else {
				LOGGER.warn(assetIRI + " was not initialised in the asset query");
				a1 = new Asset(assetIRI);
				a1.setName("");
				assetList.addAsset(a1);
				assetList.addAssetWithFunctionalRelationError(assetIRI);
			}

			Asset a2;
			if (assetList.hasAsset(partIRI)) {
				a2 = assetList.getAsset(partIRI);
			} else {
				LOGGER.warn(partIRI + " was not initialised in the asset query");
				a2 = new Asset(partIRI);
				a2.setName("");
				assetList.addAsset(a2);
				assetList.addAssetWithFunctionalRelationError(partIRI);
			}

			// finally add the part
			a1.addPart(a2);
		}
	}

	/**
	 * returns a list of connections
	 * queries ?x ?subclassOfSupply ?y
	 * 
	 * @return
	 */
	ConnectionList getConnections(AssetList assetList) {
		SelectQuery query = Queries.SELECT();
		Variable asset1 = query.var();
		Variable asset2 = query.var();
		Variable suppliesSubClass = query.var();
		Variable suppliesLabel = query.var();
		Variable suppliesComment = query.var();
		Variable receivesLabel = query.var(); // inverse of supplies

		GraphPattern subclassPattern = GraphPatterns.and(
				suppliesSubClass.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBPROPERTYOF)), supplies),
				suppliesSubClass.has(iri(RDFS.LABEL), suppliesLabel).optional(),
				suppliesSubClass.has(iri(RDFS.COMMENT), suppliesComment).optional(),
				suppliesSubClass.has(PropertyPaths.path(iri(OWL.INVERSEOF), iri(RDFS.LABEL)), receivesLabel)
						.optional());

		GraphPattern supplyPattern = asset1.has(suppliesSubClass, asset2);

		query.prefix(p_credo).select(asset1, asset2, suppliesLabel, suppliesSubClass, receivesLabel, suppliesComment)
				.where(subclassPattern, ontop.service(supplyPattern));

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		ConnectionList connList = new ConnectionList();
		for (int i = 0; i < queryResult.length(); i++) {
			String asset1_iri = queryResult.getJSONObject(i).getString(asset1.getQueryString().substring(1));
			String asset2_iri = queryResult.getJSONObject(i).getString(asset2.getQueryString().substring(1));
			String suppliesSubClassString = queryResult.getJSONObject(i)
					.getString(suppliesSubClass.getQueryString().substring(1));

			String suppliesLabelString;
			if (queryResult.getJSONObject(i).has(suppliesLabel.getQueryString().substring(1))) {
				suppliesLabelString = queryResult.getJSONObject(i)
						.getString(suppliesLabel.getQueryString().substring(1));
			} else {
				LOGGER.warn(suppliesSubClassString + " does not have a label");
				suppliesLabelString = "";
			}

			String suppliesCommentString;
			if (queryResult.getJSONObject(i).has(suppliesComment.getQueryString().substring(1))) {
				suppliesCommentString = queryResult.getJSONObject(i)
						.getString(suppliesComment.getQueryString().substring(1));
			} else {
				suppliesCommentString = "";
			}

			String receivesLabelString;
			if (queryResult.getJSONObject(i).has(receivesLabel.getQueryString().substring(1))) {
				receivesLabelString = queryResult.getJSONObject(i)
						.getString(receivesLabel.getQueryString().substring(1));
			} else {
				receivesLabelString = "";
			}

			Asset a1;
			Asset a2;

			if (assetList.hasAsset(asset1_iri)) {
				a1 = assetList.getAsset(asset1_iri);
			} else {
				LOGGER.info("Initialising " + asset1_iri + " because it it not in the asset query");
				a1 = new Asset(asset1_iri);
				assetList.addAsset(a1);
			}

			if (assetList.hasAsset(asset2_iri)) {
				a2 = assetList.getAsset(asset2_iri);
			} else {
				LOGGER.info("Initialising " + asset2_iri + " because it it not in the asset query");
				a2 = new Asset(asset2_iri);
				assetList.addAsset(a2);
			}

			Connection newConn = new Connection(a1, a2, suppliesLabelString, receivesLabelString,
					suppliesSubClassString);
			newConn.setComment(suppliesCommentString);
			connList.addConnection(newConn);
			newConn.setId(connList.getConnections(suppliesLabelString).size());
		}
		return connList;
	}

	/**
	 * query all asset properties, then populate the given site list with the
	 * queried values, also adds properties for all the parts within the site
	 * 
	 * @param asset
	 */
	void addPropertiesToAssets(AssetList assetList) {
		SelectQuery query = Queries.SELECT();
		Variable assetvar = SparqlBuilder.var("asset");
		Variable hasValueSubProperty = SparqlBuilder.var("hasValue");
		Variable hasPropertySubProperty = SparqlBuilder.var("hasProperty");
		Variable property = SparqlBuilder.var("property");
		Variable propertyLabel = SparqlBuilder.var("propertyLabel");
		Variable propertyValue = SparqlBuilder.var("propertyValue");

		GraphPattern subPropertyPattern = GraphPatterns.and(
				hasValueSubProperty.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBPROPERTYOF)), hasValue),
				hasPropertySubProperty.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBPROPERTYOF)), hasProperty));
		GraphPattern propertyValuePattern = GraphPatterns.and(assetvar.has(hasPropertySubProperty, property),
				property.has(hasValueSubProperty, propertyValue));
		GraphPattern propertyLabelPattern = hasValueSubProperty.has(RDFS.LABEL, propertyLabel);

		query.prefix(p_credo).where(subPropertyPattern, ontop.service(propertyValuePattern), propertyLabelPattern)
				.select(assetvar, propertyLabel, propertyValue);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		for (int i = 0; i < queryResult.length(); i++) {
			String assetIRI = queryResult.getJSONObject(i).getString(assetvar.getQueryString().substring(1));
			String propertylabel = queryResult.getJSONObject(i).getString(propertyLabel.getQueryString().substring(1));
			String propertyvalue = queryResult.getJSONObject(i).getString(propertyValue.getQueryString().substring(1));

			Asset matchingAsset;
			if (assetList.hasAsset(assetIRI)) {
				matchingAsset = assetList.getAsset(assetIRI);
			} else {
				LOGGER.warn("Adding properties, " + assetIRI + " is not present in the initial queries");
				LOGGER.info("Initialising new asset");
				matchingAsset = new Asset(assetIRI);
				assetList.addAsset(matchingAsset);
			}
			matchingAsset.addProperty(propertylabel, propertyvalue);
		}
	}

	/**
	 * queries all states with time series attached
	 * ?asset ?hasState ?state
	 * ?state hasTimeSeries ?ts
	 * where ?hasState subpropertyof credo:hasState
	 * 
	 * @param assetList
	 * @throws Exception
	 */
	void addStatesTimeSeries(AssetList assetList, List<Integer> vis_timesteps) {
		// first query
		// ?x hasTimeSeries ?y
		SelectQuery query = Queries.SELECT();
		Variable state = query.var();
		Variable ts = query.var();

		Iri hasTimeSeries = iri(TimeSeriesSparql.ns_ontology + "hasTimeSeries");

		query.select(state, ts).where(state.has(hasTimeSeries, ts));

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		Map<String, String> state_timeseries_map = new HashMap<>();
		Map<String, List<String>> timeseries_states_map = new HashMap<>();
		for (int i = 0; i < queryResult.length(); i++) {
			String timeseriesIRI = queryResult.getJSONObject(i).getString(ts.getQueryString().substring(1));
			String stateIRI = queryResult.getJSONObject(i).getString(state.getQueryString().substring(1));

			// collect data
			state_timeseries_map.put(stateIRI, timeseriesIRI);
			if (timeseries_states_map.containsKey(timeseriesIRI)) {
				timeseries_states_map.get(timeseriesIRI).add(stateIRI);
			} else {
				List<String> newList = new ArrayList<>();
				newList.add(stateIRI);
				timeseries_states_map.put(timeseriesIRI, newList);
			}
		}

		// second query
		// ?asset ?hasState ?state, ?state hasUnit ?unit
		SelectQuery query2 = Queries.SELECT();
		Variable state2 = query2.var();
		Variable asset = query2.var();
		Variable statelabel = query2.var();
		Variable stateProbability = query2.var();
		Variable hasStateSubProperty = query2.var();
		Variable unit = query2.var();

		GraphPattern stateProbabilityPattern = state2.has(hasProbability, stateProbability);
		GraphPattern unitPattern = state2.has(hasUnit, unit);
		GraphPattern ontopPattern = ontop.service(asset.has(hasStateSubProperty, state2),
				stateProbabilityPattern.optional(), unitPattern.optional());

		GraphPattern stateLabelPattern = hasStateSubProperty.has(iri(RDFS.LABEL), statelabel);
		GraphPattern hasStateSubPropertyPattern = hasStateSubProperty
				.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBPROPERTYOF)), hasState);

		query2.select(asset, state2, statelabel, stateProbability, unit).prefix(p_credo).where(ontopPattern,
				stateLabelPattern.optional(), hasStateSubPropertyPattern);
		JSONArray queryResult2 = storeClient.executeQuery(query2.getQueryString());

		// all states within an asset belongs to the same time series table, use this to
		// cache results
		Map<String, String> state_stateLabel_map = new HashMap<>();
		Map<String, Double> state_stateProbability_map = new HashMap<>();
		Map<String, String> state_asset_map = new HashMap<>();

		// populate state_stateLabel_map and state_asset_map
		for (int i = 0; i < queryResult2.length(); i++) {
			String assetIRI = queryResult2.getJSONObject(i).getString(asset.getQueryString().substring(1));
			String stateIRI = queryResult2.getJSONObject(i).getString(state2.getQueryString().substring(1));

			// each asset should contain only 1 time series
			if (!state_asset_map.containsKey(stateIRI)) {
				state_asset_map.put(stateIRI, assetIRI);
			}

			String stateLabelString;
			if (queryResult2.getJSONObject(i).has(statelabel.getQueryString().substring(1))) {
				stateLabelString = queryResult2.getJSONObject(i).getString(statelabel.getQueryString().substring(1));
			} else {
				LOGGER.warn(stateIRI + " does not have an associated label");
				stateLabelString = "";
			}
			if (!state_stateLabel_map.containsKey(stateIRI)) {
				state_stateLabel_map.put(stateIRI, stateLabelString);
			}

			if (queryResult2.getJSONObject(i).has(stateProbability.getQueryString().substring(1))) {
				Double stateProbabilityValue = queryResult2.getJSONObject(i).getDouble(
						stateProbability.getQueryString().substring(1));
				if (!state_stateProbability_map.containsKey(stateIRI)) {
					state_stateProbability_map.put(stateIRI, stateProbabilityValue);
				}
			}

			if (queryResult2.getJSONObject(i).has(unit.getQueryString().substring(1))) {
				String unitString = queryResult2.getJSONObject(i).getString(unit.getQueryString().substring(1));
				assetList.getAsset(assetIRI).setStateUnit(stateIRI, unitString);
			} else {
				assetList.getAsset(assetIRI).setStateUnit(stateIRI, "");
			}
		}
		assetList.setStateLabelMap(state_stateLabel_map);
		assetList.setStateProbabilityMap(state_stateProbability_map);

		// timeseries
		Iterator<String> timeIter = timeseries_states_map.keySet().iterator();

		while (timeIter.hasNext()) {
			String tsIRI = timeIter.next();

			List<String> states = timeseries_states_map.get(tsIRI);

			// check each state belongs to the same asset
			String assetToAdd;
			try {
				assetToAdd = state_asset_map.get(states.get(0));
				for (String s : states) {
					if (!state_asset_map.get(s).equals(assetToAdd)) {
						LOGGER.error(states + " do not belong to the same asset");
						continue;
					}
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				LOGGER.info("This is probably the time series for simulation time and is harmless");
				LOGGER.info(tsIRI + " " + states);
				continue;
			}

			try {
				TimeSeries<Integer> rawTimeseries = tsClient.getTimeSeriesWithinBounds(states, vis_timesteps.get(0),
						vis_timesteps.get(vis_timesteps.size() - 1));

				// don't add if it's an empty table
				if ((assetList.hasAsset(assetToAdd)) && (rawTimeseries.getTimes().size() > 0)) {
					assetList.getAsset(assetToAdd).setStatesTimeSeries(rawTimeseries);
				} else {
					LOGGER.error(assetToAdd + " was not initialised");
				}
			} catch (Exception e) {
				// catch time series client exceptions
				LOGGER.error(e.getMessage());
			}

		}
	}

	void getReferenceStates(AssetList assetList) {
		Map<String, String> state_refState_map = new HashMap<>(); // for max flood depth
		Map<String, RefStateComparator> state_comparator_map = new HashMap<>();
		Map<String, Boolean> state_refStateValue_map = new HashMap<>();

		// ?state ?hasRefState ?refState
		// optional {?refState hasValue ?value}
		// if a refState does not have a value, assume it is a time series
		SelectQuery query = Queries.SELECT();

		Variable state = query.var();
		Variable hasRefState = query.var();
		Variable refState = query.var();
		Variable value = query.var();

		GraphPattern hasRef_gp = hasRefState.has(RDFS.SUBPROPERTYOF, p_credo.iri("hasReferenceState"));
		GraphPattern state_gp = ontop.service(state.has(hasRefState, refState),
				refState.has(hasValue, value).optional());

		query.select(state, hasRefState, refState, value).where(hasRef_gp, state_gp).prefix(p_credo);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());

		for (int i = 0; i < queryResult.length(); i++) {
			String stateIRI = queryResult.getJSONObject(i).getString(state.getQueryString().substring(1));
			String hasRefStateIRI = queryResult.getJSONObject(i).getString(hasRefState.getQueryString().substring(1));
			String refStateIRI = queryResult.getJSONObject(i).getString(refState.getQueryString().substring(1));

			// pick the comparator
			switch (hasRefStateIRI) {
				case "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#hasEqualReferenceState":
					state_comparator_map.put(stateIRI, RefStateComparator.equals);
					state_refState_map.put(stateIRI, refStateIRI);
					state_refStateValue_map.put(stateIRI,
							queryResult.getJSONObject(i).getBoolean(value.getQueryString().substring(1)));
					break;
				case "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#hasMaxReferenceState":
					state_comparator_map.put(stateIRI, RefStateComparator.max);
					state_refState_map.put(stateIRI, refStateIRI);
					break;
				case "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#hasMinReferenceState":
					state_comparator_map.put(stateIRI, RefStateComparator.min);
					state_refState_map.put(stateIRI, refStateIRI);
					break;
				default:
					LOGGER.error("Queried unknown comparator " + hasRefStateIRI);
			}
		}

		assetList.setRefStateMap(state_refState_map);
		assetList.setComparatorMap(state_comparator_map);
		assetList.setRefStateValueMap(state_refStateValue_map);
	}

	JSONArray getAreaOfInterestPolys() {
		SelectQuery query = Queries.SELECT();

		Variable areaOfInterestPoly = query.var();

		GraphPattern areaOfInterestPattern = GraphPatterns.and(areaOfInterest.has(PropertyPaths.path(
				hasLocation, hasJSON), areaOfInterestPoly)).optional();

		query.select(areaOfInterestPoly).prefix(p_credo)
				.where(ontop.service(areaOfInterestPattern));

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());
		String key = areaOfInterestPoly.getQueryString().substring(1);
		JSONArray areaOfInterestPolys = new JSONArray();
		for (int i = 0; i < queryResult.length(); ++i) {
			JSONObject result = queryResult.getJSONObject(i);
			if (result.has(key)) {
				areaOfInterestPolys.put(new JSONObject(result.getString(key)));
			}
		}
		return areaOfInterestPolys;
	}

	Map<String, String> getLabelToStateIRIMap() {
		SelectQuery query = Queries.SELECT();
		String stateLabelVarName = "stateLabel";
		Variable stateLabel = SparqlBuilder.var(stateLabelVarName);
		String stateIRIVarName = "stateIRI";
		Variable stateIRI = SparqlBuilder.var(stateIRIVarName);
		Variable hasStateSubProperty = query.var();

		GraphPattern stateLabelPattern = hasStateSubProperty.has(iri(RDFS.LABEL), stateLabel);
		GraphPattern statePattern = hasStateSubProperty.has(iri(RDFS.RANGE), stateIRI);
		GraphPattern hasStateSubPropertyPattern = hasStateSubProperty
				.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBPROPERTYOF)), hasState);

		query.select(stateLabel, stateIRI).prefix(p_credo).where(
				stateLabelPattern, statePattern, hasStateSubPropertyPattern);
		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());
		return StreamSupport.stream(queryResult.spliterator(), false)
				.map(entry -> (JSONObject) entry)
				.collect(Collectors.toUnmodifiableMap(
						entry -> entry.getString(stateLabelVarName),
						entry -> entry.getString(stateIRIVarName)));
	}

	public List<String> getSitesInAOE() {
		if (this.getAreaOfInterestPolys().isEmpty()) {
			return null;
		}

		SelectQuery query = Queries.SELECT();
		Variable site = SparqlBuilder.var("site");
		Variable siteSubType = query.var();
		Variable areaOfInterestPoly = query.var();
		Variable point = query.var();

		GraphPattern siteDataPattern;

		TriplePattern areaOfInterestTriple = areaOfInterest.has(PropertyPaths.path(
				hasLocation, hasWKB), areaOfInterestPoly);

		TriplePattern locationPattern = site.has(PropertyPaths.path(hasLocation, hasWKB), point);

		siteDataPattern = GraphPatterns.and(areaOfInterestTriple,
				site.isA(siteSubType),
				locationPattern,
				areaOfInterestTriple).filter(
						Expressions.and(GeoSPARQL.sfContains(areaOfInterestPoly, point)));

		GraphPattern subClassPattern = GraphPatterns.and(
				siteSubType.has(PropertyPaths.zeroOrMore(iri(RDFS.SUBCLASSOF)), Site));

		// site data are in ontop, TBox are in KG
		query.select(site).prefix(p_credo, p_geof)
				.where(ontop.service(siteDataPattern), subClassPattern);

		JSONArray queryResult = storeClient.executeQuery(query.getQueryString());
		return StreamSupport.stream(queryResult.spliterator(), false)
				.map(entry -> ((JSONObject) entry).getString("site"))
				.collect(Collectors.toList());
	}

}