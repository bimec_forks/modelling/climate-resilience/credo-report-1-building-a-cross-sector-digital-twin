package com.cmclinnovations.credo.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.stream.Collectors;

import com.cmclinnovations.credo.objects.Connection.ConnectionType;

/**
 * list of assets with owner name as the key
 */
public class AssetList {
    private static final Logger LOGGER = LogManager.getLogger(AssetList.class);
    Map<String, Map<String, List<Asset>>> owner_type_site; // owner - type - site
    List<Asset> allSites; // all sites without grouping
    List<Asset> nonSites; // assets that are not sites
    List<Asset> allAssets; // everything

    // missing data information
    List<Asset> assetsWithoutLocation;
    List<Asset> assetsWithoutName;
    List<Asset> assetsWithFunctionalRelationError;

    Map<String, String> state_stateLabel;
    Map<String, Double> state_stateProbability;
    Map<String, Asset> assetMap;

    // reference state to compare
    Map<String, String> state_refState_map;
    Map<String, RefStateComparator> state_comparator_map;
    Map<String, Boolean> state_refStateValue_map;

    // comparator to use to compare reference values
    // e.g. equals means that state must be the same as the reference value
    public enum RefStateComparator {
        equals,
        max,
        min
    }

    public AssetList() {
        this.allSites = new ArrayList<>();
        this.nonSites = new ArrayList<>();
        this.allAssets = new ArrayList<>();
        this.owner_type_site = new HashMap<>();
        this.assetMap = new HashMap<>();
        this.assetsWithoutLocation = new ArrayList<>();
        this.assetsWithoutName = new ArrayList<>();
        this.assetsWithFunctionalRelationError = new ArrayList<>();
    }

    public void addAsset(Asset asset) {
        if (!this.assetMap.containsKey(asset.getIri())) {
            this.allAssets.add(asset);
            this.assetMap.put(asset.getIri(), asset);
            this.nonSites.add(asset); // everything is not a site by default, will be removed in add site
        } else {
            LOGGER.error("Trying to add duplicate asset " + asset.getIri());
        }
    }

    public Asset getAsset(String iri) {
        return this.assetMap.get(iri);
    }

    public boolean hasAsset(String iri) {
        return this.assetMap.containsKey(iri);
    }

    public void addAssetWithoutLocation(String iri) {
        this.assetsWithoutLocation.add(getAsset(iri));
    }

    public List<Asset> getAssetsWithoutLocation() {
        return this.assetsWithoutLocation;
    }

    public void addAssetWithoutName(String iri) {
        this.assetsWithoutName.add(getAsset(iri));
    }

    public List<Asset> getAssetsWithoutName() {
        return this.assetsWithoutName;
    }

    public void addAssetWithFunctionalRelationError(String iri) {
        this.assetsWithFunctionalRelationError.add(getAsset(iri));
    }

    public List<Asset> getAssetsWithFunctionalRelationError() {
        return this.assetsWithFunctionalRelationError;
    }

    public void organiseSiteWithOwner(String owner, Asset site) {
        if (this.assetMap.containsKey(site.getIri())) {
            if (this.owner_type_site.containsKey(owner)) {
                if (this.owner_type_site.get(owner).containsKey(site.getLabel())) {
                    this.owner_type_site.get(owner).get(site.getLabel()).add(site);
                } else {
                    // owner is not new, but new subtype discovered
                    List<Asset> newSiteList = new ArrayList<>();
                    newSiteList.add(site);

                    this.owner_type_site.get(owner).put(site.getLabel(), newSiteList);
                }
            } else {
                // new owner discovered, type is automatically new as well
                Map<String, List<Asset>> type_site = new HashMap<>();
                List<Asset> newSiteList = new ArrayList<>();
                newSiteList.add(site);
                type_site.put(site.getLabel(), newSiteList);

                this.owner_type_site.put(owner, type_site);
            }
            this.allSites.add(site);
            this.nonSites.remove(site);
        } else {
            LOGGER.error(site.getIri() + " asset is not initialised");
        }
    }

    public List<String> getSiteOwners() {
        return new ArrayList<>(this.owner_type_site.keySet());
    }

    public List<String> getSiteTypes(String owner) {
        return new ArrayList<>(this.owner_type_site.get(owner).keySet());
    }

    public List<Asset> getSites(String owner, String type) {
        return this.owner_type_site.get(owner).get(type);
    }

    public List<Asset> getAllSites() {
        return this.allSites;
    }

    public List<Asset> getAllAssets() {
        return this.allAssets;
    }

    public void setStateLabelMap(Map<String, String> stateLabelMap) {
        this.state_stateLabel = stateLabelMap;
    }

    public String getStateLabel(String state) {
        return this.state_stateLabel.get(state);
    }

    public void setRefStateMap(Map<String, String> state_refState_map) {
        this.state_refState_map = state_refState_map;
    }

    public void setComparatorMap(Map<String, RefStateComparator> state_comparator_map) {
        this.state_comparator_map = state_comparator_map;
    }

    public void setRefStateValueMap(Map<String, Boolean> state_refStateValue_map) {
        this.state_refStateValue_map = state_refStateValue_map;
    }

    public Boolean getRefStateValue(String state) {
        return this.state_refStateValue_map.get(state);
    }

    public boolean hasRefState(String state) {
        return this.state_refState_map.containsKey(state);
    }

    /**
     * returns IRI of the reference state linked to this state
     * 
     * @param state
     * @return
     */
    public String getRefState(String state) {
        return this.state_refState_map.get(state);
    }

    public RefStateComparator getComparator(String state) {
        return this.state_comparator_map.get(state);
    }

    public void setStateProbabilityMap(Map<String, Double> stateProbabilityMap) {
        this.state_stateProbability = stateProbabilityMap;
    }

    public Double getStateProbability(String state) {
        return this.state_stateProbability.get(state);
    }

    public void calcSiteCriticality() {
        // sewage connection is inverse of the other connection to calculate criticality
        // a site that only receives sewage is technically the top node
        // get top nodes that are only supplying and does not receive anything
        List<Asset> topNodes = new ArrayList<>();
        for (Asset site : this.allSites) {
            List<Connection> connections = site.getConnections();
            List<Connection> receivesFromList = connections.stream().filter(c -> c.getReceiver() == site)
                    .collect(Collectors.toList());

            if (receivesFromList.size() == 0 || receivesFromList.stream()
                    .allMatch(c -> c.getConnectionType() == ConnectionType.suppliesSewageTo)) {
                topNodes.add(site);
            }
        }

        List<Asset> nodesToCalculate = topNodes;
        while (nodesToCalculate.size() > 0) {
            List<Asset> leftOutSites = new ArrayList<>();
            // keep track of calculation to avoid duplications
            DirectedAcyclicGraph<Asset, DefaultEdge> graph = new DirectedAcyclicGraph<>(DefaultEdge.class);
            for (Asset node : nodesToCalculate) {
                try {
                    node.calcCriticality(null, leftOutSites, graph);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                    LOGGER.error("Cannot calculate criticality for the graph of " + node.getIri());
                }
            }
            nodesToCalculate = leftOutSites;
        }
    }
}
