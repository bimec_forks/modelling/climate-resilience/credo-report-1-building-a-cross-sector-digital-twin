package com.cmclinnovations.credo.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * specifies a connection between two assets
 */
public class Connection {
    private Asset supplier;
    private Asset receiver;
    private int id;
    private String label;
    private String comment;
    private String inverseLabel;
    private ConnectionType type;

    public enum ConnectionType {
        suppliesPowerTo,
        suppliesSewageTo,
        suppliesWaterTo,
        suppliesPhoneTo,
        connTypeDefault;
    }

    private static Map<String, ConnectionType> connIri_type_map = new HashMap<String, ConnectionType>() {
		{
			put("http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#suppliesWaterTo", ConnectionType.suppliesWaterTo);
			put("http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#suppliesPowerTo", ConnectionType.suppliesPowerTo);
            put("http://theworldavatar.com/ontology/ontocredo/ontoaw.owl#suppliesSewageTo", ConnectionType.suppliesSewageTo);
            put("http://theworldavatar.com/ontology/ontocredo/ontobt.owl#suppliesPhoneTo", ConnectionType.suppliesPhoneTo);
		}
	};

    public Connection(Asset supplier, Asset receiver, String suppliesLabel, String receivesLabel, String connTypeIri) {
        this.supplier = supplier;
        this.receiver = receiver;
        this.label = suppliesLabel;
        this.inverseLabel = receivesLabel;
        supplier.addSuppliesTo(receiver,suppliesLabel,this);
        receiver.addReceivesFrom(supplier,receivesLabel,this);

        // type is mainly used for criticality calculation
        if (connIri_type_map.containsKey(connTypeIri)) {
            this.type = connIri_type_map.get(connTypeIri);
        } else {
            this.type = ConnectionType.connTypeDefault;
        }
    }

    public String getLabel() {
        return this.label;
    }

    public String getInverseLabel() {
        return this.inverseLabel;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return this.comment;
    }

    public Asset getSupplier() {
        return this.supplier;
    }

    public Asset getReceiver() {
        return this.receiver;
    }

    public String getDisplayName() {
        String s1;
        if (supplier.getName().equals("")) {
            s1 = this.supplier.getId();
        } else {
            s1 = this.supplier.getName();
        }

        String s2;
        if (receiver.getName().equals("")) {
            s2 = this.receiver.getId();
        } else {
            s2 = this.receiver.getName();
        }

        return s1 + " " + this.label.toLowerCase() + " " + s2;
    }

    public ConnectionType getConnectionType() {
        return this.type;
    }
}
