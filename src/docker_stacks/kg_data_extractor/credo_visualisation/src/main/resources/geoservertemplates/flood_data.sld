<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
	<NamedLayer>
		<Name>flood_data</Name>
		<UserStyle>
			<Name>flood_data</Name>
			<Title>Flood map raster</Title>
			<Abstract>A sample style for rasters</Abstract>
			<FeatureTypeStyle>
				<FeatureTypeName>Feature</FeatureTypeName>
				<Rule>
					<RasterSymbolizer>
						<ColorMap>
							<ColorMapEntry color="#000000" opacity="0.0" quantity="0.0" />
							<ColorMapEntry color="#1269eb" opacity="0.1" quantity="0.01" />
							<ColorMapEntry color="#08306B" opacity="0.9" quantity="2.0" />
						</ColorMap>
					</RasterSymbolizer>
				</Rule>
			</FeatureTypeStyle>
		</UserStyle>
	</NamedLayer>
</StyledLayerDescriptor>