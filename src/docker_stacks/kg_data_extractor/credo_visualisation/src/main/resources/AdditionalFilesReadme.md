# Additional Files
This document explains the content of the additional files attached to this webpage.
The links to the files are prefixed by the name of the scenario to which they belong.

## Failure summary file
This file summarises the number of failures for a given scenario.

Each row contains information about assets based on the asset owner, the type of asset, and the primary cause of failure.
The columns summarise the number of failed assets and total number of assets, both within the area of interest and within the whole digital twin.
The remaining columns summarise the total criticality scores of the failed assets and total assets, both within the area of interest and within the whole digital twin. (The criticality of each asset is calculated by counting the number of direct and indirect connections to each asset, so is broadly proportional to the number of things that would have a problem if an asset failed).

The difference between the totals for the area of interest and whole digital twin may be significant when the area of interest (for example, the area covered by a flood simulation) is small compared to the area covered by the whole digital twin.
Both totals are provided (as opposed to reporting the proportion of failed assets) to make it easier to identify situations where the cascade of failures causes many assets and/or critical assets outside the area of interest to fail, even when these assets may only represent a small proportion of all the total assets in the digital twin. 

## Site failures files
Each site failures file contains more granular information about the assets from each Asset Owner, including whether each asset has flooded, and whether there was an interruption in any of the services supplied to each asset.

## Missing data file
This file reports potential issues with the data in the digital twin.
The issues that can be detected are described as follows:

### Assets with missing name
Not all assets have a specified name.
In such cases, the IDs of an asset is used in lieu of its name.
	 
### Site with missing location
Not all sites have a specified geographic location in the source data, so cannot be displayed on the map. 
This typically happened with assets that were outside the region covered by the initial phase of the CReDo project.

### Asset with functional relation error
Assets are added to this list if their functional parent is not present in the digital twin. 
When this was observed, it was typically due to discrepancies in how the sub-parts of assets were defined in the source data.
