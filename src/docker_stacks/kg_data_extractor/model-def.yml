kind: Model
api_version: v1beta1
metadata:
  display_name: KG Data Extractor
  name: kg-data-extractor
  publisher: CMCL Innovations
  summary: This is used to post process data from the CReDo digital twin ready for further processing/visualisation.
  description: >
    Pulls data from knowledge graph containers and generates json and geo-json files that can be used as inputs for other models, or for the visualisation
spec:
  command: ["/app/entrypoint.sh"]
  inputs:
    dataslots:
      - name: Flood labels
        description: >
          File that maps HiPIMS simulation codes to meaningful description texts.
        default:
          - d80bca7a-cca9-4e4e-9a63-50b3e0eb208f
        path: inputs/
        required: false
    parameters:
      - name: BUILDINGS_DATA_DIR
        title: BUILDINGS_DATA_DIR
        default: "/data/inputs/building-data"
        description: "Input directory in which to search for building data to be added to the output."
        type: string
        required: false
      - name: GEOSERVER_PORT
        title: GEOSERVER_PORT
        default: 8080
        description: "Port to write to the metadata for all geotif layers. Ignored if ONLY_SINGLE_TIMESTEP is true."
        type: integer
        required: false
      - name: GEOTIF_DIR
        title: GEOTIF_DIR
        default: "/data/inputs/geotif-data"
        description: "Input directory in which to search for geotifs to add to the output. Ignored if ONLY_SINGLE_TIMESTEP is true."
        type: string
        required: false
      - name: KG_PASSWORD
        title: KG_PASSWORD
        default: ""
        description: "Password to use when contacting the blazegraph server."
        type: string
        required: false
      - name: KG_PROTOCOL
        title: KG_PROTOCOL
        default: "http"
        description: "Protocol for knowledge graph endpoint"
        type: string
        required: true
      - name: KG_HOST
        title: KG_HOST
        default: "blazegraph"
        description: "Host of knowledge graph endpoint"
        type: link
        required: true
      - name: KG_PORT
        title: KG_PORT
        default: 8080
        description: "Port of knowledge graph endpoint"
        type: integer
        required: true
      - name: KG_PATH
        title: KG_PATH
        description: "Path of KG url"
        default: "blazegraph/namespace/kb/sparql"
        type: string
        required: true
      - name: KG_USER
        title: KG_USER
        default: ""
        description: "Username to use when contacting the blazegraph server."
        type: string
        required: false
      - name: ONLY_SINGLE_TIMESTEP
        title: ONLY_SINGLE_TIMESTEP
        default: "FALSE"
        description: "Set to 'TRUE' (case insensitive) to output data only for the last timestep in each timeseries."
        type: string
        required: false
      - name: ONTOP_PROTOCOL
        title: ONTOP_PROTOCOL
        default: "http"
        description: "Protocol of Ontop endpoint"
        type: string
        required: true
      - name: ONTOP_HOST
        title: ONTOP_HOST
        description: "Host of Ontop endpoint"
        default: "ontop"
        type: link
        required: true
      - name: ONTOP_PORT
        title: ONTOP_PORT
        description: "Port of Ontop endpoint"
        default: 8080
        type: integer
        required: true
      - name: ONTOP_PATH
        title: ONTOP_PATH
        description: "Path of Ontop endpoint"
        default: "sparql"
        type: string
        required: true
      - name: OUTPUT_DIR
        title: OUTPUT_DIR
        default: "/data/outputs/data"
        description: "Directory in which to produce output files. Note that changing this from the default will mean no files are written to the output dataslot."
        type: string
        required: false
      - name: POSTGRES_PASSWORD
        title: POSTGRES_PASSWORD
        default: "postpass"
        description: "Password to use when contacting the postgres server."
        type: string
        required: false
      - name: POSTGRES_HOST
        title: POSTGRES_HOST
        default: "postgis"
        description: "Host of postgres server"
        type: link
        required: true
      - name: POSTGRES_PORT
        title: POSTGRES_PORT
        description: "Port of postgres server"
        type: integer
        default: 5432
        required: true
      - name: POSTGRES_USER
        title: POSTGRES_USER
        default: "postgres"
        description: "Username to use when contacting the postgres server."
        type: string
        required: false
      - name: RIVER_DATA_DIR
        title: RIVER_DATA_DIR
        default: "/data/inputs/river-data"
        description: "Input directory in which to search for river data to be added to the output."
        type: string
        required: false
      - name: TIMESERIES_DB
        title: TIMESERIES_DB
        default: "credo_timeseries"
        description: "Name of the postgres database to query."
        type: string
        required: false
      - name: DAFNI_METADATA_FILE
        title: DAFNI_METADATA_FILE
        default: "/data/outputs/dafni_metadata.json"
        description: Location to write the publisher metadata. DAFNI will use this file to assign meaningful names to the datasets from the CReDo workflow.
        type: string
        required: false
      - name: FLOOD_LABEL_FILE
        title: FLOOD_LABEL_FILE
        default: "/data/inputs/flood_labels.csv"
        description: This file maps a HiPIMS simulation ID to meaningful texts for the visualisation as well as to give meaningful descriptions to the outputs from the CReDo workflow.
        type: string
        required: false
      - name: WORKFLOW_NAME
        title: WORKFLOW_NAME
        default: ""
        description: Name of the workflow. This is used in the metadata for the outputs.
        type: string
        required: false
  outputs:
    datasets:
      - name: outputs/data
        type: json
        description: json and geo-json files for processing/visualisation
