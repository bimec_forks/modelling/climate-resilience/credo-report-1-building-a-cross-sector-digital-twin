#!/bin/sh

# Make sure the output directory exists
mkdir -p "$OUTPUT_DIR/data"

echo "Running processing code in new process..."
java -jar /app/visualisation-1.0.0-SNAPSHOT.jar

if [ "TRUE" = "$SANITISE_DATA" ]; then
  ./sanitise_data.sh
fi

# If the KEEP_ALIVE variable was set, tail /dev/null to prevent the container exiting
if [ -n "$KEEP_ALIVE" ]; then
  tail -f /dev/null
fi